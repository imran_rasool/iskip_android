package com.trianglz.iskipuserandroid.model

data class ToolBarModel(
    var backBtn: Boolean = false,
    var toolBarShow: Boolean = false,
    var toolBarTtlShow: Boolean = false,
    var bottomBar: Boolean = false,
    var toolBarTtl: String = ""
)

data class GenericModel1(
    var success: Boolean,
    var message: String,
    var menu_items: ArrayList<ItemModel>,
    var store: StoreListModel,
    var customer: CustomerM
)

data class GenericModel(
    var success: Boolean,
    var setting_options: FilterModel,
    var stores: ArrayList<StoreListModel>,
    var zones: ArrayList<ZonesModel>,
    var support_tickets: ArrayList<SupportTicketsModel>,
    var total_items: Int,
    var page: Int,
    var items: Int,
    var token: String,
    var message: String,
    var checkout_id: String,
    var terms_and_conditions_url: String,
    var reservation: ResereModel,
    var customer: UserData,
    var links: ArrayList<String>,
    var reservations: ArrayList<ReservationItemM>,
    var saved_lists: ArrayList<SavedListsItemM>,
    var saved_list: ArrayList<SavedListsItemM>

)

data class SavedListsItemM(
    var stores: ArrayList<StoresItemM>,
    var id: Int,
    var title: String
)


data class StoresItemM(
    var name: String,
    var logo: Logo,
    var distance_from_user: String,
    var id: Int
)

data class Logo(
    var url: String
)

data class ReservationItemM(
    var reservation_code: String,
    var reservation_date: String,
    var created_at: String,
    var reservation_time: String,
    var store: String,
    var cancellation_reason: String = "",
    var updated_at: String,
    var zone: String,
    var service: String,
    var deposit: Int,
    var qr_code: String,
    var payment: PaymentM,
    var id: Int,
    var seating_area: String,
    var store_location: String,
    var status: String,
    var number_of_guests: Int,
    var store_image: String
)

data class PaymentM(
    var payment_details: PaymentDetailsM,
    var payment_option: String,
    var payment_status: String,
    var bank_name: String,
    var payment_method: String
)

data class PaymentDetailsM(
    var wallet: Int,
    var vat_charges: Int,
    var online: Int,
    var promocode: String,
    var promocode_value: Int,
    var cash: Int,
    var promocode_type: String
)

data class FilterModel(
    var options: ArrayList<String>
)

data class LatLngModel(
    var lat: Double,
    var long: Double
)

data class StoreModel(
    var lat: Double? = null,
    var long: Double? = null,
    var locale: String? = null,
    var zoneId: Int? = 0,
    var category_name: String = "",
    var name: String = "",
    var items: Int = 10,
    var page: Int? = null,
    var public_api: Boolean
)

data class ZoneStoreModel(
    var lat: Double? = null,
    var long: Double? = null,
    var locale: String? = null,
    var zoneId: Int? = 0,
    var category_name: String = "",
    var name: String = "",
    var items: Int = 10,
    var page: Int? = null
)

data class ReservationListModel(
    var locale: String = "",
    var items: Int = 10,
    var page: Int = 0,
    var type: String = ""
)

data class StoreListModel(
    var id: Int,
    var name: String,
    var is_store_saved: Boolean,
    var name_en: String,
    var name_ar: String,
    var description_en: String,
    var description_ar: String,
    var services: ArrayList<String>,
    var facebook_url: String,
    var twitter_url: String,
    var instagram_url: String,
    var website_url: String,
    var location: String,
    var status: String,
    var latitude: Double,
    var longitude: Double,
    var publish_date: String,
    var booking_time: ArrayList<String>,
    var booking_times: ArrayList<BookingModel>,
    var cancellation_policy: String,
    var currency: String,
    var category_name: String,
    var store_type: String,
    var created_at: String,
    var updated_at: String,
    var zone: String,
    var logo: String,
    var cover_photo: String,
    var opening_hours: ArrayList<OpeningHrsModel>,
    var items: ArrayList<ItemModel>,
    var store_seating_areas: ArrayList<SeatingAreaModel>,
    var min_charge: Double,
    var public_api: Boolean

)

data class BookingModel(var id: Int, var day: String, var time_slots: ArrayList<String>)

data class ItemModel(
    var id: Int, var name: String,
    var category_name: String,
    var description: String,
    var price: String,
    var image: String,
    var url: String,
    var calories: Int,
    var in_stock: Boolean
)

data class OpeningHrsModel(
    var id: Int, var day: String,
    var from_time: String,
    var to_time: String
)

data class SeatingAreaModel(
    var id: Int, var name: String,
    var normal_available_tables: String,
    var vip_available_tables: String
)

data class ResereModel(
    var id: Int,
    var address: String,
    var title: String,
    var payment: PaymentModel
)

data class PaymentModel(
    var payment_option: String
)

data class SupportTicketsModel(
    var id: Int,
    var description: String,
    var title: String,
    var ticket_number: String,
    var reply: String,
    var created_at: String,
    var status: String
)

data class ZonesModel(
    var id: Int,
    var address: String,
    var title: String,
    var logo: UrlModel
)

data class UrlModel(var url: String)

data class UserData(
    var auth_token: String,
    var email: String,
    var name: String,
    var phone_verified: Boolean,
    var image: String
)

data class ParamModel(var id: String = "", var isSelect: Boolean = false)

data class ForgotPasswordModel(
    var phone_number: String, var no_sms: Boolean
)

data class ResetPasswordModel(
    var token: String, var password: String
)

data class OTPResetPasswordModel(
    var token: String, var otp: String
)

data class ProfileUpdateModel(
    var fcm_token: String,
    var device_id: String,
    var locale: String,
    var image: String,
    var name: String,
    var email: String,
    var date_of_birth: String,
    var gender: String
)

data class TimeModel(
    var time: String, var select: Boolean
)

data class ResendModel(
    var no_sms: Boolean
)

data class OtpModel(
    var phone_verification_code: String

)


data class LogoutModel(
    var device_id: String
)

data class ReservationModel(
    var store_id: Int,
    var type: String,
    var reservation_date: String,
    var reservation_time: String,
    var seating_area: String,
    var number_of_guests: String,
    var payment_method: String,
    var payment_option: String,
    var final_total_price: Int = 0,
    var original_price: Int = 0,
    var vat_charges: Int = 0,
    var wallet: Int = 0,
    var promocode_id: Int = 0,
    var cash: Int = 0,
    var online: Int = 0,
    var promocode: String = "",
    var promocode_type: String = "",
    var promocode_value: Int = 0,
)

data class UpsertModel(
    var title: String
)

//store_action==>remove or add
data class UpsertUpdateModel(
    var id: Int,
    var title: String,
    var store_id: Int,
    var store_action: String
)

data class LoginModel(
    var device_id: String,
    var fcm_token: String,
    var phone_number: String,
    var locale: String,
    var password: String
)

data class SignupModel(
    var device_id: String,
    var fcm_token: String,
    var phone_number: String,
    var email: String,
    var locale: String,
    var gender: String,
    var name: String,
    var no_sms: Boolean,
    var password: String
)

data class CustomerM(
    var social_provider_type: String,
    var image: String,
    var email_verified: Boolean,
    var gender: String,
    var lng: String,
    var city: String,
    var date_of_birth: String,
    var phone_verification_code: String,
    var locale: String,
    var must_change_password: Boolean,
    var country_code: String,
    var social_provider_id: String,
    var phone_verified: Boolean,
    var name: String,
    var orders_to_be_confirmed: List<OrdersToBeConfirmedItem>,
    var phone_number: String,
    var id: Int,
    var email: String,
    var lat: String
)

data class QrCodeImage(
    var url: String
)

data class OrdersToBeConfirmedItem(
    var store_id: Int,
    var book_now: Boolean,
    var reservation_code: String,
    var reservation_date: String,
    var qr_code_image: QrCodeImage,
    var valet_scanned_count: Int,
    var non_iskip_user_id: String,
    var created_at: String,
    var reservation_time: String,
    var cancellation_reason: String,
    var charges: String,
    var updated_at: String,
    var branch_slot_reservation_id: String,
    var reservation_at: String,
    var deposit: Int,
    var receptionist_scanned_count: Int,
    var id: Int,
    var customer_user_id: Int,
    var order_type: String,
    var seating_area: String,
    var status: String,
    var number_of_guests: Int
)

