package com.trianglz.iskipuserandroid.model

import java.io.Serializable

class PayloadResponse : Serializable {
    var msg:String?=null
    var orders_id:String?=null
    var type:String?=null

}


object NotificationModel {
    var id = "id"
    var msg = "msg"
    var orders_id = "orders_id"
    var title = "title"
    var type = "type"
    var senderId = "senderId"
    var notification_type = "notification_type"
    var notificationBundle="notificationBundle"

    enum class NotiTye{
        liked_post,comment_post,message,event_invite_rejected,event_invite_accept,new_invite,start_event,end_event,friend_Request,request_accept
    }

}