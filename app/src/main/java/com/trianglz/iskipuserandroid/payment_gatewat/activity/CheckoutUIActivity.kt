package com.trianglz.iskipuserandroid.payment_gatewat.activity

import android.content.ComponentName
import android.os.Bundle
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.ActivityCheckoutUiBinding
import com.trianglz.iskipuserandroid.payment_gatewat.common.Constants
import com.trianglz.iskipuserandroid.payment_gatewat.receiver.CheckoutBroadcastReceiver
import kotlinx.coroutines.ExperimentalCoroutinesApi

/**
 * Represents an activity for making payments via {@link CheckoutActivity}.
 */
class CheckoutUIActivity : BasePaymentActivity() {

    private var viewDataBinding: ActivityCheckoutUiBinding?=null

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = ActivityCheckoutUiBinding.inflate(layoutInflater)
        setContentView(viewDataBinding?.root)

        val amount = Constants.Config.AMOUNT + " " + Constants.Config.CURRENCY
        viewDataBinding?.apply {
            amountTextView.text = amount

            progressBar = progressBarCheckoutUi

            buttonProceedToCheckout.setOnClickListener {
                requestCheckoutId()
            }
        }
    }

    override fun onCheckoutIdReceived(checkoutId: String?) {
        super.onCheckoutIdReceived(checkoutId)
        if (checkoutId != null) {
            openCheckoutUI(checkoutId)
        }
    }

    private fun openCheckoutUI(checkoutId: String) {
        val checkoutSettings = createCheckoutSettings(checkoutId, getString(R.string.checkout_ui_callback_scheme))

        /* Set componentName if you want to receive callbacks from the checkout */
        val componentName = ComponentName(packageName, CheckoutBroadcastReceiver::class.java.name)

        /* Set up the Intent and start the checkout activity */
        val intent = checkoutSettings.createCheckoutActivityIntent(this, componentName)

        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT)
    }
}