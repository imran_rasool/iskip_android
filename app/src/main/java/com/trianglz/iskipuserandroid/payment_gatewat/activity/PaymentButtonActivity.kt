package com.trianglz.iskipuserandroid.payment_gatewat.activity

import android.content.ComponentName
import android.graphics.Color
import android.os.Bundle
import com.oppwa.mobile.connect.checkout.dialog.PaymentButtonFragment
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.payment_gatewat.common.Constants
import com.oppwa.mobile.connect.exception.PaymentException
import com.trianglz.iskipuserandroid.databinding.ActivityMainBinding
import com.trianglz.iskipuserandroid.databinding.ActivityPaymentButtonBinding
import com.trianglz.iskipuserandroid.payment_gatewat.receiver.CheckoutBroadcastReceiver
import kotlinx.coroutines.ExperimentalCoroutinesApi

/**
 * Represents an activity for making payments via {@link PaymentButtonSupportFragment}.
 */
class PaymentButtonActivity : BasePaymentActivity() {

    private var viewDataBinding: ActivityPaymentButtonBinding?=null
    private lateinit var paymentButtonFragment: PaymentButtonFragment

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = ActivityPaymentButtonBinding.inflate(layoutInflater)
        setContentView(viewDataBinding?.root)
     //   setContentView(R.layout.activity_payment_button)

        val amount = Constants.Config.AMOUNT + " " + Constants.Config.CURRENCY
        viewDataBinding?.apply {
            amountTextView.text = amount
            progressBar = progressBarPaymentButton
        }


        initPaymentButton()
    }

    override fun onCheckoutIdReceived(checkoutId: String?) {
        super.onCheckoutIdReceived(checkoutId)

        checkoutId?.let { pay(checkoutId) }
    }

    @ExperimentalCoroutinesApi
    private fun initPaymentButton() {
        paymentButtonFragment = viewDataBinding?.progressBarPaymentButton as PaymentButtonFragment

        paymentButtonFragment.paymentBrand = Constants.Config.PAYMENT_BUTTON_BRAND
        paymentButtonFragment.paymentButton.apply {
            setOnClickListener {
                requestCheckoutId()
            }

            /* Customize the payment button (except Google Pay button) */
         //   setBackgroundResource(R.drawable.button_base_background)
            setColorFilter(Color.rgb(255, 255, 255))
        }
    }

    private fun pay(checkoutId: String) {
        val checkoutSettings = createCheckoutSettings(checkoutId, getString(R.string.payment_button_callback_scheme))

        /* Set componentName if you want to receive callbacks from the checkout */
        val componentName = ComponentName(packageName, CheckoutBroadcastReceiver::class.java.name)

        try {
            paymentButtonFragment.submitTransaction(checkoutSettings, componentName)
        } catch (e: PaymentException) {
            showAlertDialog(R.string.error_message)
        }
    }
}