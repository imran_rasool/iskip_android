package com.trianglz.iskipuserandroid.payment_gatewat.activity

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.payment_gatewat.common.Constants
import com.oppwa.mobile.connect.exception.PaymentError
import com.oppwa.mobile.connect.exception.PaymentException
import com.oppwa.mobile.connect.payment.CheckoutInfo
import com.oppwa.mobile.connect.payment.card.CardPaymentParams
import com.oppwa.mobile.connect.provider.Connect
import com.oppwa.mobile.connect.provider.ITransactionListener
import com.oppwa.mobile.connect.provider.Transaction
import com.oppwa.mobile.connect.provider.TransactionType
import com.oppwa.mobile.connect.service.ConnectService
import com.oppwa.mobile.connect.service.IProviderBinder
import com.trianglz.iskipuserandroid.databinding.ActivityCheckoutUiBinding
import com.trianglz.iskipuserandroid.databinding.ActivityCustomUiBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi


class CustomUIActivity : BasePaymentActivity(), ITransactionListener {

    private var viewDataBinding: ActivityCustomUiBinding?=null
    private lateinit var checkoutId: String
    private var providerBinder: IProviderBinder? = null

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            /* we have a connection to the service */
            providerBinder = service as IProviderBinder
            providerBinder!!.addTransactionListener(this@CustomUIActivity)

            try {
                providerBinder!!.initializeProvider(Connect.ProviderMode.TEST)
            } catch (ee: PaymentException) {
                showErrorDialog(ee.message!!)
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            providerBinder = null
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = ActivityCustomUiBinding.inflate(layoutInflater)
        setContentView(viewDataBinding?.root)

        initViews()
    }

    override fun onStart() {
        super.onStart()

        val intent = Intent(this, ConnectService::class.java)

        startService(intent)
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()

        unbindService(serviceConnection)
        stopService(Intent(this, ConnectService::class.java))
    }

    override fun onCheckoutIdReceived(checkoutId: String?) {
        super.onCheckoutIdReceived(checkoutId)

        checkoutId?.let {
            this.checkoutId =checkoutId
            requestCheckoutInfo(checkoutId)
        }
    }

    //region ITransactionListener methods
    override fun paymentConfigRequestSucceeded(checkoutInfo: CheckoutInfo) {
        /* Get the resource path from checkout info to request the payment status later */
        resourcePath = checkoutInfo.resourcePath

        runOnUiThread {
            showConfirmationDialog(checkoutInfo.amount.toString(),
                    checkoutInfo.currencyCode!!
            )
        }
    }

    override fun paymentConfigRequestFailed(paymentError: PaymentError) {
        hideProgressBar()
//        Log.d("Dddddaaaaaa11111",paymentError.errorMessage.toString())
        showErrorDialog(paymentError)
    }

    @ExperimentalCoroutinesApi
    override fun transactionCompleted(transaction: Transaction) {
        if (transaction.transactionType == TransactionType.SYNC) {
            /* check the status of synchronous transaction */
            requestPaymentStatus(resourcePath!!)
        } else {
            /* wait fot the callback in the onNewIntent() */
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(transaction.redirectUrl)))
        }
    }

    override fun transactionFailed(transaction: Transaction, paymentError: PaymentError) {
        hideProgressBar()
       // Log.d("Dddddaaaaaa22222",transaction.transactionType.toString())
//        Log.d("Dddddaaaaaa22222",transaction.toString())
//        Log.d("Dddddaaaaaa22222",paymentError.errorMessage.toString())
        showErrorDialog(paymentError)
    }
    //endregion

    @ExperimentalCoroutinesApi
    private fun initViews() {
        viewDataBinding?.apply {
            holderEditText.setText(Constants.Config.CARD_HOLDER_NAME)
            numberEditText.setText(Constants.Config.CARD_NUMBER)
            expiryMonthEditText.setText(Constants.Config.CARD_EXPIRY_MONTH)
            expiryYearEditText.setText(Constants.Config.CARD_EXPIRY_YEAR)
            cvvEditText.setText(Constants.Config.CARD_CVV)
            progressBar = progressBarCustomUi

            buttonPayNow.setOnClickListener {
                showConfirmationDialog(Constants.Config.AMOUNT,
                   Constants.Config.CURRENCY
                )
//                if (providerBinder != null && checkFields()) {
//                    requestCheckoutId()
//                }
            }
        }

    }

    private fun checkFields(): Boolean {
        viewDataBinding?.apply {
            if (holderEditText.text.isEmpty() ||
                numberEditText.text.isEmpty() ||
                expiryMonthEditText.text.isEmpty() ||
                expiryYearEditText.text.isEmpty() ||
                cvvEditText.text.isEmpty()) {
                showAlertDialog(R.string.error_empty_fields)

                return false
            }
        }


        return true
    }

    private fun requestCheckoutInfo(checkoutId: String) {
        try {
            providerBinder!!.requestCheckoutInfo(checkoutId)
        } catch (e: PaymentException) {
            e.message?.let { showAlertDialog(it) }
        }
    }

    private fun pay(checkoutId: String) {
        try {
            val paymentParams = createPaymentParams(checkoutId)
            paymentParams.shopperResultUrl = getString(R.string.custom_ui_callback_scheme) + "://callback"
            val transaction = Transaction(paymentParams)

            providerBinder!!.submitTransaction(transaction)
        } catch (e: PaymentException) {
            Log.d("Dddddaaaaaa3333",e.error.toString())
            showErrorDialog(e.error)
        }
    }
    private lateinit var cardHolder: String
    private lateinit var cardNumber: String
    private lateinit var cardExpiryMonth: String
    private lateinit var cardExpiryYear: String
    private lateinit var cardCVV: String

    private fun createPaymentParams(checkoutId: String): CardPaymentParams {
        viewDataBinding?.apply {
             cardHolder = holderEditText.text.toString()
             cardNumber = numberEditText.text.toString()
             cardExpiryMonth = expiryMonthEditText.text.toString()
             cardExpiryYear = expiryYearEditText.text.toString()
             cardCVV = cvvEditText.text.toString()


        }
        return CardPaymentParams(
            checkoutId,
            Constants.Config.CARD_BRAND,
            cardNumber,
            cardHolder,
            cardExpiryMonth,
            "20$cardExpiryYear",
            cardCVV
        )
    }

    private fun showConfirmationDialog(amount: String, currency: String) {
        checkoutId=intent.getStringExtra("EXTRA_CHECKOUT_ID")?: checkoutId
     //   Log.d("Ddddd#######",checkoutId)
        AlertDialog.Builder(this)
                .setMessage(String.format(getString(R.string.message_payment_confirmation), amount, currency))
                .setPositiveButton(R.string.button_ok) { _, _ -> pay(checkoutId) }
                .setNegativeButton(R.string.button_cancel) { _, _ -> hideProgressBar()}
                .setCancelable(false)
                .show()
    }

    private fun showErrorDialog(message: String) {
        runOnUiThread { showAlertDialog(message) }
    }

    private fun showErrorDialog(paymentError: PaymentError) {
     //   Log.d("Dddddaaaaaa",paymentError.errorMessage.toString())
        runOnUiThread { showErrorDialog(paymentError.errorMessage) }
    }
}