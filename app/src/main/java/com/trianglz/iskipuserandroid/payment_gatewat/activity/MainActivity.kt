package com.trianglz.iskipuserandroid.payment_gatewat.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import androidx.appcompat.app.AppCompatActivity
import com.trianglz.iskipuserandroid.R
import com.oppwa.mobile.connect.provider.Connect
import com.trianglz.iskipuserandroid.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), OnClickListener {

    private var viewDataBinding: ActivityMainBinding?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
     //   setContentView(R.layout.activity_main)
        viewDataBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewDataBinding?.root)

        viewDataBinding?.apply {
            checkoutUi.setOnClickListener(this@MainActivity)
            paymentButton.setOnClickListener(this@MainActivity)
            customUi.setOnClickListener(this@MainActivity)

            versionNumber.text =
                String.format(getString(R.string.sdk_version_number), Connect.getVersion())
        }

    }

    override fun onClick(view: View?) {
        if (view != null) {
            when (view.id) {
                R.id.checkout_ui -> startActivity(Intent(this, CheckoutUIActivity::class.java))
                R.id.payment_button -> startActivity(Intent(this, PaymentButtonActivity::class.java))
                R.id.custom_ui -> startActivity(Intent(this, CustomUIActivity::class.java))
            }
        }
    }
}