package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.appsums.view.adapters.RecyclerCallback
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentSaveDetailBinding
import com.trianglz.iskipuserandroid.databinding.ItemSaveBinding
import com.trianglz.iskipuserandroid.databinding.ItemStoreNewBinding
import com.trianglz.iskipuserandroid.model.SavedListsItemM
import com.trianglz.iskipuserandroid.model.StoresItemM
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.base.CustomDialog


class SaveDetailFragment :
    BaseFragment<FragmentSaveDetailBinding>(FragmentSaveDetailBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<StoresItemM, ItemStoreNewBinding>? =
        null
    private var list: ArrayList<StoresItemM>? = null

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list = arguments?.getSerializable(BUNDLECONSTANTS.ID.name) as ArrayList<StoresItemM>?

        viewDataBinding?.apply {

            backBtn.setOnClickListener {
                popBack()
            }
            if (list != null) {
                setAdapter1()
            }
        }
    }

    private fun setAdapter1() {

        rvAdapProgress = RecyclerViewGenricAdapter<StoresItemM, ItemStoreNewBinding>(
            list,
            R.layout.item_store_new, object :
                RecyclerCallback<ItemStoreNewBinding, StoresItemM> {
                @SuppressLint("SetTextI18n")
                override fun bindData(
                    binding: ItemStoreNewBinding,
                    model: StoresItemM,
                    position: Int,
                    itemView: View
                ) {

                    binding?.apply {

                        imgBookmark.gone()
                        stateAndCategory.gone()
                        setImageWithUrl(model.logo.url, storeLogo, R.drawable.img_k_logo)
                        setImageWithUrl(model.logo.url, storeBanner, R.drawable.menu)
                        storeName.text = model.name
                        distance.text = model.distance_from_user

                        root.setOnClickListener {
                            val bundle = Bundle()
                            bundle.putInt(BUNDLECONSTANTS.ID.name, list!![position].id)
                            displayItAddStack(StoreDetailFragment(), bundle)
                        }
                    }

                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}