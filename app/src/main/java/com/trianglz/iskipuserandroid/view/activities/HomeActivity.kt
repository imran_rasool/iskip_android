package com.trianglz.iskipuserandroid.view.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.trianglz.iskipuserandroid.IskipUserApp
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.ActivityHomeBinding
import com.trianglz.iskipuserandroid.model.ParamModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.view.base.BaseActivity
import com.trianglz.iskipuserandroid.view.base.CustomDialog
import com.trianglz.iskipuserandroid.view.fragments.*


class HomeActivity : BaseActivity<ActivityHomeBinding>(ActivityHomeBinding::inflate) {


    var locationname: MutableLiveData<String>? = null

    override fun setContainerLayout(): Int {
        return R.id.container
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        if (SharedPrefClass().getPrefValue(
                this,
                GlobalConstants.IS_VERIFIED
            ).toString().toBoolean()
        ) {
            getCurrentLocationName()
            callNavFragment(HomeFragment())
        } else {
            callNavFragment(OnBoardingFragment())
        }

        setBottomNavigation()
        initObserver()

    }

    fun initObserver() {


//        viewDataBinding?.apply {
//
//            btnProfile.setOnClickListener {
//                root.closeDrawers()
//                displayItAddStack(CompleteProfileFragment(1))
//            }
//
//
//        }


    }


    private fun setBottomNavigation() {
        viewDataBinding?.apply {


            bottomNavigation.setOnNavigationItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.home -> {
                        callNavFragment(HomeFragment())
                        true
                    }
                    R.id.orders -> {

                        if (SharedPrefClass().getPrefValue(
                                IskipUserApp.instance,
                                GlobalConstants.ACCESS_TOKEN
                            ) == null
                        ) {
                            CustomDialog(this@HomeActivity).openLoginDialog(object :
                                CustomDialog.DialogListener {
                                override fun positiveBtn() {
                                    clearAllStack()
                                    displayItNoStack(LoginFragment())
                                }

                                override fun positiveBtn(param: String) {
                                }

                                override fun positiveBtn(paramModel: ParamModel) {

                                }
                            })
                            false
                        } else {
                            callNavFragment(OrderFragment())
                            true
                        }

                    }
                    R.id.updates -> {
                        if (SharedPrefClass().getPrefValue(
                                IskipUserApp.instance,
                                GlobalConstants.ACCESS_TOKEN
                            ) == null
                        ) {
                            CustomDialog(this@HomeActivity).openLoginDialog(object :
                                CustomDialog.DialogListener {
                                override fun positiveBtn() {
                                    clearAllStack()
                                    displayItNoStack(LoginFragment())
                                }

                                override fun positiveBtn(param: String) {
                                }

                                override fun positiveBtn(paramModel: ParamModel) {

                                }
                            })
                            false
                        } else {
                            callNavFragment(SavedFragment())
                            true
                        }

                    }
                    R.id.profile -> {
                        if (SharedPrefClass().getPrefValue(
                                IskipUserApp.instance,
                                GlobalConstants.ACCESS_TOKEN
                            ) == null
                        ) {
                            CustomDialog(this@HomeActivity).openLoginDialog(object :
                                CustomDialog.DialogListener {
                                override fun positiveBtn() {
                                    clearAllStack()
                                    displayItNoStack(LoginFragment())
                                }

                                override fun positiveBtn(param: String) {
                                }

                                override fun positiveBtn(paramModel: ParamModel) {

                                }
                            })
                            false
                        } else {
                            callNavFragment(ProfileFragment())
                            true
                        }

                    }
                    else -> false
                }
            }
        }
    }

    fun callNavFragment(fragment: Fragment) {
        displayItNoStack(fragment)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            //  var fragment = supportFragmentManager.findFragmentById(R.id.frameContainer)
            supportFragmentManager.popBackStack()
        } else {
            finishAffinity()

        }
    }

    fun setToolbarText(toolBarModel: ToolBarModel) {
        viewDataBinding?.apply {

            if (toolBarModel.bottomBar) {
                bottomNavigation.visibility = View.VISIBLE
            } else {
                bottomNavigation.visibility = View.GONE
            }
        }
    }

    fun logoutDialog() {
//        CustomDialog(this).openLogoutDialog(object :
//            CustomDialog.DialogListener {
//            override fun positiveBtn() {
//                logout()
//            }
//
//            override fun positiveBtn(paramModel: ParamModel) {
//
//            }
//
//        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        fragment!!.onActivityResult(requestCode, resultCode, data)

    }


}