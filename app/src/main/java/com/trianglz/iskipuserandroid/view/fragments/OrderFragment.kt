package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsums.view.adapters.RecyclerCallback
import com.google.android.material.tabs.TabLayout
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentOrdersBinding
import com.trianglz.iskipuserandroid.databinding.ItemOrderBinding
import com.trianglz.iskipuserandroid.databinding.ItemStoreNewBinding
import com.trianglz.iskipuserandroid.model.*
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.extensions.clickWithThrottle
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response


class OrderFragment() :
    BaseFragment<FragmentOrdersBinding>(FragmentOrdersBinding::inflate) {

    private var tabListenree: TabLayout.OnTabSelectedListener? = null
    private var rvAdapProgress: RecyclerViewGenricAdapter<ReservationItemM, ItemOrderBinding>? =
        null
    private var layoutmanager: LinearLayoutManager? = null
    private var isLoading = true
    private var isLastPage = false
    private var type = ""
    private var list = ArrayList<ReservationItemM>()
    private var pageNO = 0
    private var tabSelect = 0

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = true
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            layoutmanager = LinearLayoutManager(context)
            recyclerView.layoutManager = layoutmanager
            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText("All"))
                tabLayout.addTab(tabLayout.newTab().setText("Reservations"))
                tabLayout.addTab(tabLayout.newTab().setText("Waiting List"))
            }

            swipeRefresh.setOnRefreshListener {
                // refreshAction() // refresh your list contents somehow
                swipeRefresh.isRefreshing = false
            }

            tabListener()
            setAdapter1()
            callApi(tabLayout.selectedTabPosition)

        }
    }

    private fun callAPi1(reservation: ReservationListModel) {
        homeActivity?.let {
            ApiCall.reservationList(it, reservation, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        val response = mResponse?.body()
                        isLoading = response?.total_items ?: 0 >= response?.items ?: 0
                        isLastPage = !isLoading
                        viewDataBinding?.progressCircular?.hide()
                        response?.reservations?.let { it ->
                            list?.clear()
                            list?.addAll(it)
                            rvAdapProgress?.notifyDataSetChanged()
                            chkEmptyList()
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun tabListener() {
        if (tabListenree != null) {
            viewDataBinding?.tabLayout?.removeOnTabSelectedListener(tabListenree)
        }
        tabListenree = object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tabSelect = tab?.position ?: 0
                callApi(tabSelect)
                when (tabSelect) {
                    0 -> {
                        type = ""
                    }
                    1 -> {
                        type = "Reservation"
                    }
                    2 -> {
                        type = "Waitinglist"
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        }
        viewDataBinding?.apply {
            tabLayout.addOnTabSelectedListener(tabListenree)
        }

    }

    private fun callApi(selectTab: Int) {
        when (selectTab) {
            0 -> {
                callAPi1(
                    ReservationListModel(
                        locale = "en", items = 10, page = 1,
                        type = ""
                    )
                )
            }
            1 -> {
                callAPi1(
                    ReservationListModel(
                        locale = "en", items = 10, page = 1,
                        type = "Reservation"
                    )
                )

            }
            2 -> {
                callAPi1(
                    ReservationListModel(
                        locale = "en", items = 10, page = 1,
                        type = "Waitinglist"
                    )
                )
            }
        }


    }

    private fun chkEmptyList() {
        viewDataBinding?.apply {
            if (list.isEmpty()) {
                recyclerView.gone()
                tvNoDataFound.visible()
            } else {
                recyclerView.visible()
                tvNoDataFound.gone()
            }
        }

    }

    private fun setAdapter1() {

        rvAdapProgress = RecyclerViewGenricAdapter<ReservationItemM, ItemOrderBinding>(
            list,
            R.layout.item_order, object :
                RecyclerCallback<ItemOrderBinding, ReservationItemM> {
                @SuppressLint("SetTextI18n")
                override fun bindData(
                    binding: ItemOrderBinding,
                    model: ReservationItemM,
                    position: Int,
                    itemView: View
                ) {

                    binding.apply {

                        storeNameTv.text = model.status
                        serviceAndOrderNoTv.text =
                            model.service + " | " + "Reservation " + model.reservation_code
                        setImageWithUrl(model.store_image, storeLogoImage1)
                        if (model.reservation_date != null && model.reservation_time != null) {
                            val date = changeDateFormat1(model.reservation_date)
                            footerTv.text = "" + model.reservation_time + "|" + date
                        }

                        cardView.clickWithThrottle {
                            displayItAddStack(OrderDetailFragment(model))
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
            recyclerView.addOnScrollListener(paginationScroll())
        }
    }

    private fun paginationScroll(): RecyclerView.OnScrollListener {
        val recyclerViewOnScrollListener: RecyclerView.OnScrollListener =
            object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(
                    recyclerView: RecyclerView,
                    newState: Int
                ) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val visibleItemCount: Int = layoutmanager?.childCount ?: 0
                    val totalItemCount: Int = layoutmanager?.itemCount ?: 0
                    val firstVisibleItemPosition: Int =
                        layoutmanager?.findFirstVisibleItemPosition() ?: 0
                    if (isLoading && !isLastPage) {
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                            firstVisibleItemPosition >= 0 && totalItemCount >= 10
                        ) {
                            isLoading = false
                            pageNO += 1
                            viewDataBinding?.progressCircular?.show()
                            viewDataBinding?.progressCircular?.visibility = View.VISIBLE
                            callAPi1(
                                ReservationListModel(
                                    locale = "en", items = 10, page = pageNO,
                                    type = type
                                )
                            )

                        }
                    }
                }
            }
        return recyclerViewOnScrollListener
    }


}