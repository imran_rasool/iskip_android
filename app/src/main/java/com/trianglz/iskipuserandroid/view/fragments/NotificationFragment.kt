package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.view.adapters.RecyclerCallback
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentNotificationBinding
import com.trianglz.iskipuserandroid.databinding.ItemNotiBinding
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment


class NotificationFragment :
    BaseFragment<FragmentNotificationBinding>(FragmentNotificationBinding::inflate) {


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(context)

        }
        setAdapter1()
    }


    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("All")
        list.add("All")
        list.add("All")
        list.add("All")
        list.add("All")
        list.add("All")


        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemNotiBinding>(
            list,
            R.layout.item_noti, object :
                RecyclerCallback<ItemNotiBinding, String> {
                override fun bindData(
                    binding: ItemNotiBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binding.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}