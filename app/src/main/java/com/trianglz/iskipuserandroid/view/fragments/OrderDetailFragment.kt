package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.view.adapters.RecyclerCallback
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.DialogConfirmBinding
import com.trianglz.iskipuserandroid.databinding.FragmentOrderDetailsNewBinding
import com.trianglz.iskipuserandroid.databinding.FragmentStoreDetailsBinding
import com.trianglz.iskipuserandroid.databinding.ItemStoreNewBinding
import com.trianglz.iskipuserandroid.model.ReservationItemM
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.base.CustomDialog

class OrderDetailFragment(val model: ReservationItemM? = null) :
    BaseFragment<FragmentOrderDetailsNewBinding>(FragmentOrderDetailsNewBinding::inflate) {

    var cancelBtn = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = false

        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            if (model != null) {
                tvReservationNo.text = model.reservation_code
                val date = changeDateFormat1(model.reservation_date)
                tvReservationTime.text = "" + model.reservation_time + "|" + date
                tvGuestNo.text = "" + model.number_of_guests
                tvArea.text = model.seating_area
                tvLocation.text = model.zone
                tvNameService.text = model.service
                setImageWithUrl(model.qr_code, qrImage, R.drawable.ic_qrcode)

            }
            closeBtn.setOnClickListener {
                popBack()
            }

            imgRecipt.setOnClickListener {
                llDetail.gone()
                llReceipt.visible()
            }

            imgDetail.setOnClickListener {
                llDetail.visible()
                llReceipt.gone()
            }

            imgEdit.setOnClickListener {
                cancelBtn = !cancelBtn
                if (cancelBtn)
                    btnCancel.visible()
                else
                    btnCancel.gone()
            }

            btnCancel.setOnClickListener {
                CustomDialog(homeActivity!!).openCancelAppDialog()

            }


        }


    }


}