package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Base64OutputStream
import android.view.View
import android.widget.RadioButton
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentCompleteProfileBinding
import com.trianglz.iskipuserandroid.model.GenericModel1
import com.trianglz.iskipuserandroid.model.ProfileUpdateModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.CheckPermission
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.utils.TakePictureUtils
import com.trianglz.iskipuserandroid.utils.cropimage.CropImage
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.base.CustomDialog
import retrofit2.Response
import java.io.*


class CompleteProfileFragment(var from: Int) :
    BaseFragment<FragmentCompleteProfileBinding>(FragmentCompleteProfileBinding::inflate) {

    private var storeId: Int? = null
    private val tempImage = "tempImage"
    private var path: String? = null
    private var file: File? = null
    private var imgURL: String = ""


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        storeId = arguments?.getInt(BUNDLECONSTANTS.ID.name)

        viewDataBinding?.apply {

            tvDelAcc.setOnClickListener {
                CustomDialog(homeActivity!!).openDelAccountDialog()
            }

            imgProfile.setOnClickListener {
                checkPermission()
            }
            edtDOB.setOnClickListener {
                datePicker(edtDOB, min = false, max = true)
            }


            btnSubmit.setOnClickListener {

                val radioButtonID: Int = genderGroup.checkedRadioButtonId
                val radioButton: View = genderGroup.findViewById(radioButtonID)
                val idx: Int = genderGroup.indexOfChild(radioButton)
                val r: RadioButton = genderGroup.getChildAt(idx) as RadioButton
                var finalBase64 = ""
                finalBase64 = if (file != null) {
                    val base = imageFileToBase64(file!!)
                    val append = "data:image/png;base64,"
                    append + base
                } else {
                    Base64.encodeToString(imgURL.toByteArray(), Base64.DEFAULT)
                }
                if (isValidate()) {
                    callApi2(
                        ProfileUpdateModel(
                            getDeviceId(),
                            getDeviceId(),
                            "en",
                            finalBase64,
                            edtFullName.text.toString(),
                            edtEmail.text.toString(),
                            edtDOB.text.toString(),
                            r.text.toString().lowercase()
                        )
                    )
                }

            }

            callApi(69) //storeId

        }


    }

    private fun callApi(id: Int) {
        homeActivity?.let {
            ApiCall.customerDetail(it, id, object : ResponseListener<GenericModel1> {
                @SuppressLint("SetTextI18n")
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    if (mResponse.body() != null) {
                        val model = mResponse?.body()?.customer
                        if (model != null)
                            viewDataBinding?.apply {

                                SharedPrefClass().putObject(
                                    it,
                                    GlobalConstants.NAME,
                                    model.name
                                )

                                if (model.image != null) {
                                    SharedPrefClass().putObject(
                                        requireContext(), GlobalConstants.PIC, model.image
                                    )
                                }


                                setImageWithUrl(model.image, imgProfile, R.drawable.menu)
                                imgURL = model.image
                                edtFullName.setText("" + model.name)
                                edtCountryCode.setText("" + model.country_code)
                                edtMobileNumber.setText("" + model.phone_number)
                                edtEmail.setText("" + model.email)
                                if (model.date_of_birth != null) {
                                    val date = changeUTCDateFormat(model.date_of_birth)
                                    edtDOB.setText("" + date)
                                }
                                if (model.gender == "male") {
                                    maleRb.isChecked = true
                                    femaleRb.isChecked = false
                                } else {
                                    femaleRb.isChecked = true
                                    maleRb.isChecked = false

                                }
                            }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun callApi2(model: ProfileUpdateModel) {
        homeActivity?.let {
            ApiCall.updateProfile(it, model,
                object : ResponseListener<GenericModel1> {
                    @SuppressLint("SetTextI18n")
                    override fun onSuccess(mResponse: Response<GenericModel1>) {
                        if (mResponse.body() != null) {
                            if (mResponse.body()?.success == true) {
                                //callApi(69) //storeId
                                viewDataBinding?.apply {
                                    popBack()
                                    // setImageWithUri(file, viewDataBinding?.imgProfile)
                                }
                            } else {
                                showToast(mResponse.body()?.message)
                            }
                        }
                    }

                    override fun onError(msg: String) {

                    }
                })
        }
    }


    private fun checkPermission() {
        if (CheckPermission.checkIsMarshMallowVersion()) {
            if (CheckPermission.checkCameraStoragePermission(requireContext())) {
                showImageDialog(requireActivity(), tempImage, false)
            } else {
                CheckPermission.requestCameraStoragePermission(requireActivity())
            }
        } else {
            showImageDialog(requireActivity(), tempImage, false)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CheckPermission.REQUEST_CODE_CAMERA_STORAGE_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showImageDialog(requireActivity(), tempImage, false)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            TakePictureUtils.TAKE_PICTURE -> TakePictureUtils.startCropImage(
                context as Activity?,
                tempImage + ".jpg",
                500,
                500
            )
            TakePictureUtils.PICK_GALLERY -> {
                try {
                    val inputStream: InputStream? =
                        homeActivity!!.getContentResolver().openInputStream(
                            data!!.data!!
                        )
                    val fileOutputStream: FileOutputStream =
                        FileOutputStream(
                            File(
                                requireContext().getExternalFilesDir("temp"),
                                tempImage + ".jpg"
                            )
                        )
                    TakePictureUtils.copyStream(inputStream, fileOutputStream)
                    fileOutputStream.close()
                    inputStream?.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                TakePictureUtils.startCropImage(context as Activity?, tempImage + ".jpg", 500, 500)
            }
            TakePictureUtils.CROP_FROM_CAMERA -> {
                path = null
                if (data != null) {
                    path = data.getStringExtra(CropImage.IMAGE_PATH)
                    if (path != null) file = File(path)
                    //    viewModel.callApiFileToUrl(context, file)
                    if (path == null) {
                        return
                    }
                    setImageWithUri(file, viewDataBinding?.imgProfile)

//                    Glide.with(context!!)
//                        .load(file)
//                        .placeholder(R.drawable.dummy)
//                        .into<Target<Drawable>>(viewDataBinding)
                }
            }
        }
    }

    private fun imageFileToBase64(imageFile: File): String {

        return FileInputStream(imageFile).use { inputStream ->
            ByteArrayOutputStream().use { outputStream ->
                Base64OutputStream(outputStream, Base64.DEFAULT).use { base64FilterStream ->
                    inputStream.copyTo(base64FilterStream)
                    base64FilterStream.flush()
                    outputStream.toString()
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        viewDataBinding?.apply {
            return when {
                edtFullName.text.isNullOrEmpty() -> {
                    showToast(getString(R.string.enter_your_full_name))
                    false
                }
                edtMobileNumber.text.isNullOrEmpty() -> {
                    showToast(getString(R.string.please_enter_phone_number))
                    false
                }
                edtEmail.text.isNullOrEmpty() -> {
                    showToast(getString(R.string.enter_email_address))
                    false
                }
                else -> {
                    true
                }
            }
        }
        return true
    }

}