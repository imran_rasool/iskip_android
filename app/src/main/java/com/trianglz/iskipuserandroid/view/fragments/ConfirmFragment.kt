package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import com.trianglz.iskipuserandroid.databinding.FragmentConfirmationBinding
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.view.base.BaseFragment


class ConfirmFragment :
    BaseFragment<FragmentConfirmationBinding>(FragmentConfirmationBinding::inflate) {


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


            backBtn.setOnClickListener {
                popBack()
            }


            tvLink.paintFlags = tvLink.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        }
    }


}