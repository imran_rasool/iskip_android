package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsums.view.adapters.RecyclerCallback
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.trianglz.iskipuserandroid.IskipUserApp
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentStoresBinding
import com.trianglz.iskipuserandroid.databinding.FragmentStoresSearchBinding
import com.trianglz.iskipuserandroid.databinding.ItemStoreNewBinding
import com.trianglz.iskipuserandroid.model.*
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectGuestNo
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectSeatingArea
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectSlot
import retrofit2.Response
import java.lang.reflect.Type


class StoreSearchFragment :
    BaseFragment<FragmentStoresSearchBinding>(FragmentStoresSearchBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<StoreListModel, ItemStoreNewBinding>? =
        null
    var isPasswordVisibe = false
    var list = ArrayList<StoreListModel>()
    private var layoutmanager: LinearLayoutManager? = null
    var isLoading = true
    var isLastPage = false
    private var isLoginToken = false
    var pageNO = 0

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gson = Gson()
        val type: Type = object : TypeToken<ArrayList<ZonesModel>>() {}.type


        isLoginToken = SharedPrefClass().getPrefValue(
            IskipUserApp.instance,
            GlobalConstants.ACCESS_TOKEN
        ) != null

        viewDataBinding?.apply {

            layoutmanager = LinearLayoutManager(context)
            recyclerView.layoutManager = layoutmanager

            backBtn.setOnClickListener {
                popBack()
            }



            searchBtn.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    callApi()
                    return@OnEditorActionListener true
                }
                false
            })

            setAdapter1()
            callApi()

        }

    }


    fun callApi() {

        callAPi(
            StoreModel(
                lat, long, locale = "en",
                name = viewDataBinding?.searchBtn?.text.toString(), public_api = isLoginToken
            )
        )
    }

    fun setAdapter1() {
        rvAdapProgress = RecyclerViewGenricAdapter<StoreListModel, ItemStoreNewBinding>(
            list,
            R.layout.item_store_new, object :
                RecyclerCallback<ItemStoreNewBinding, StoreListModel> {
                override fun bindData(
                    binding: ItemStoreNewBinding,
                    model: StoreListModel,
                    position: Int,
                    itemView: View
                ) {
                    binding.apply {
                        storeName.text = model.name
                        stateAndCategory.text = model.category_name
                        setImageWithUrl(model.cover_photo, storeBanner, R.drawable.menu)
                        setImageWithUrl(model.logo, storeLogo)
                        distance.text = "" + distance(
                            model.latitude.toFloat(), model.longitude.toFloat(),
                            SharedPrefClass().getPrefValue(
                                homeActivity!!,
                                GlobalConstants.LATTITUDE
                            ).toString().toFloat(), SharedPrefClass().getPrefValue(
                                homeActivity!!,
                                GlobalConstants.LATTITUDE
                            ).toString().toFloat()
                        ) + " km away"

                        root.setOnClickListener {
                            selectSeatingArea = 0
                            selectGuestNo = 0
                            selectSlot = ""
                            val bundle = Bundle()
                            bundle.putInt(BUNDLECONSTANTS.ID.name, list[position].id)
                            displayItAddStack(StoreDetailFragment(), bundle)
                        }
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
            recyclerView.addOnScrollListener(paginationScroll())

        }
    }


    fun callAPi(storeModel: StoreModel) {
        homeActivity?.let {
            ApiCall.allStores(it, storeModel, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        var response = mResponse?.body()
                        isLoading = response?.total_items ?: 0 >= response?.items ?: 0
                        isLastPage = !isLoading
                        //  zonesList.clear()
                        response?.stores?.let { it ->
                            list?.clear()
                            list?.addAll(it)
                            rvAdapProgress?.notifyDataSetChanged()
                            chkEmptyList()
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun chkEmptyList() {
        viewDataBinding?.apply {
            if (list.isEmpty()) {
                recyclerView.gone()
                noResults.visible()
            } else {
                recyclerView.visible()
                noResults.gone()
            }
        }

    }

    fun callAPiZone(storeModel: ZoneStoreModel) {
        homeActivity?.let {
            ApiCall.storesId(it, storeModel, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        //  zonesList.clear()
                        mResponse.body()?.stores?.let { it ->
                            list.clear()
                            list.addAll(it)
                            rvAdapProgress?.notifyDataSetChanged()
                            chkEmptyList()
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun paginationScroll(): RecyclerView.OnScrollListener {
        val recyclerViewOnScrollListener: RecyclerView.OnScrollListener =
            object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(
                    recyclerView: RecyclerView,
                    newState: Int
                ) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val visibleItemCount: Int = layoutmanager?.childCount ?: 0
                    val totalItemCount: Int = layoutmanager?.itemCount ?: 0
                    val firstVisibleItemPosition: Int =
                        layoutmanager?.findFirstVisibleItemPosition() ?: 0
                    if (isLoading && !isLastPage) {
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= 10
                        ) {
                            isLoading = false
                            pageNO += 1
                            //      viewDataBinding?.progressCircular?.show()
                            //     viewDataBinding?.progressCircular?.visibility = View.VISIBLE
                            callAPi(
                                StoreModel(
                                    lat,
                                    long,
                                    locale = "en",
                                    page = pageNO,
                                    public_api = isLoginToken
                                )
                            )

                        }
                    }
                }
            }
        return recyclerViewOnScrollListener
    }

}