package com.trianglz.iskipuserandroid.view.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.trianglz.iskipuserandroid.IskipUserApp
import com.trianglz.iskipuserandroid.databinding.FragmentCheckoutPayBinding
import com.trianglz.iskipuserandroid.model.GenericModel
import com.trianglz.iskipuserandroid.model.ParamModel
import com.trianglz.iskipuserandroid.model.ReservationModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.payment_gatewat.activity.CustomUIActivity
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.base.CustomDialog
import retrofit2.Response
import java.lang.reflect.Type

class CheckoutPayFragment :
    BaseFragment<FragmentCheckoutPayBinding>(FragmentCheckoutPayBinding::inflate) {

    private var model: ReservationModel? = null


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gson = Gson()
        val type: Type = object : TypeToken<ReservationModel>() {}.type
        model =
            gson.fromJson(arguments?.getString(BUNDLECONSTANTS.OBJECT.name), type)

        viewDataBinding?.apply {
            backBtn1.setOnClickListener {
                popBack()
            }
            btnConfirm1.setOnClickListener {
                if (SharedPrefClass().getPrefValue(
                        IskipUserApp.instance,
                        GlobalConstants.ACCESS_TOKEN
                    )==null){
                    CustomDialog(homeActivity!!).openLoginDialog(object :
                        CustomDialog.DialogListener {
                        override fun positiveBtn() {
                            homeActivity?.clearAllStack()
                            displayItNoStack(LoginFragment())
                        }

                        override fun positiveBtn(param: String) {
                        }

                        override fun positiveBtn(paramModel: ParamModel) {

                        }
                    })
                }else{
                    model?.let { it1 -> callAPi(it1) }
                }

            }

            btnConfirm.setOnClickListener {
                btnConfirm1.isEnabled=true
                cardView.gone()
            }

            btnCancel.setOnClickListener {
              cardView.gone()
            }

            checkbox1.setOnClickListener {
                setCheckout(true, false, false, false)
            }
            checkbox2.setOnClickListener {
                setCheckout(false, true, false, false)
            }
            checkbox3.setOnClickListener {
                setCheckout(false, false, true, false)
            }
            checkbox4.setOnClickListener {
                setCheckout(false, false, false, true)
            }

        }
    }

    fun setCheckout(
        check1: Boolean,
        check2: Boolean,
        check3: Boolean,
        check4: Boolean
    ) {
        viewDataBinding?.apply {
            checkbox1.isChecked = check1
            checkbox2.isChecked = check2
            checkbox3.isChecked = check3
            checkbox4.isChecked = check4
        }
    }

    fun callAPi(body: ReservationModel) {
        homeActivity?.let {
            ApiCall.createReservation(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        var model = mResponse?.body()?.reservation
                        callAPi(model?.id ?: 0, model?.payment?.payment_option ?: "")
                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(id: Int, name: String) {
        homeActivity?.let {
            ApiCall.getPaymentCheckout(it, id, name, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {

//homeActivity?.clearAllStack()
                        //69F9E82DB24613F6AAF4F8BDC917FF54.uat01-vm-tx02
//                        Log.d("Ddddd","===="+mResponse?.body()?.checkout_id?:"gfgfgf")
                        val intent = Intent(homeActivity, CustomUIActivity::class.java)
                        intent.putExtra("EXTRA_CHECKOUT_ID",mResponse.body()?.checkout_id)
                      //  intent.putExtra("A",mResponse?.body()?.checkout_id)
//                        intent.putExtra("EXTRA_CHECKOUT_ID","3B6CAA8B1F9B8AB517DCED0FAA67C847.uat01-vm-tx04")
                        startActivity(intent)
                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}