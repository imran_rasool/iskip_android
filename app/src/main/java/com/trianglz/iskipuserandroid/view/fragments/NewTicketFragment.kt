package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.trianglz.iskipuserandroid.databinding.FragmentCraeteTicketBinding
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.view.base.BaseFragment


class NewTicketFragment :
    BaseFragment<FragmentCraeteTicketBinding>(FragmentCraeteTicketBinding::inflate) {


    override fun getToolBar(): ToolBarModel {
        val toolBarModel = ToolBarModel()

        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            submitButton.setOnClickListener {
              popBack()
            }
        }

    }


}