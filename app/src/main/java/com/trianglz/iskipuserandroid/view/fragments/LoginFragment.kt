package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentLoginBinding
import com.trianglz.iskipuserandroid.model.GenericModel
import com.trianglz.iskipuserandroid.model.LoginModel
import com.trianglz.iskipuserandroid.model.SignupModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response


class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    var isPasswordVisibe = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            backButton.setOnClickListener {
                homeActivity?.getCurrentLocationName()
                displayItNoStack(HomeFragment())
            }

            passwordEditText.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= passwordEditText.getRight() - passwordEditText.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe = !isPasswordVisibe

                        if (isPasswordVisibe) {
                            passwordEditText.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            passwordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_show,
                                0
                            );
                        } else {
                            passwordEditText.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            passwordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_hide,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })



            signInBtn.setOnClickListener {
                if (isValidate())
                    callAPi(
                        LoginModel(
                            getDeviceId(),
                            SharedPrefClass().getPrefValue(
                                requireContext(),
                                GlobalConstants.DEVICE_ID
                            ).toString(),
                            getString(R.string.code_) + phoneEditText.text.toString(),
                            "en",
                            passwordEditText.text.toString()
                        )
                    )
            }

            forgotPasswordButton.setOnClickListener {
                displayItAddStack(ForgotPasswordFragment())
            }

            registerButton.setOnClickListener {
                displayItAddStack(RegisterFragment())
            }

        }
    }

    fun callAPi(body: LoginModel) {
        homeActivity?.let {
            ApiCall.login(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        var model = mResponse?.body()?.customer

                        if (model?.auth_token != null)
                            SharedPrefClass().putObject(
                                it,
                                GlobalConstants.ACCESS_TOKEN,
                                model?.auth_token
                            )
                        if (model?.phone_verified != null)
                            SharedPrefClass().putObject(
                                it,
                                GlobalConstants.IS_VERIFIED,
                                model?.phone_verified
                            )
                        if (model?.name != null) {
                            SharedPrefClass().putObject(
                                it,
                                GlobalConstants.NAME,
                                model?.name
                            )
                        }
                        if (model?.image != null) {
                            SharedPrefClass().putObject(
                                it,
                                GlobalConstants.PIC,
                                model?.image
                            )

                        }

                        if (model?.phone_verified == true)
                            displayItAddStack(HomeFragment())
                        else {
                            displayItAddStack(OtpFragment(1))
                        }
                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (phoneEditText.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_phone_number))
                return false
            } else if (passwordEditText.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_password))
                return false
            } else {
                return true
            }
        }
        return true
    }


}