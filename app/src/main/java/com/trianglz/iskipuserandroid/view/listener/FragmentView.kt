package com.trianglz.iskipuserandroid.view.listener

import com.trianglz.iskipuserandroid.model.ToolBarModel


interface FragmentView {
    fun getToolBar(): ToolBarModel?
}