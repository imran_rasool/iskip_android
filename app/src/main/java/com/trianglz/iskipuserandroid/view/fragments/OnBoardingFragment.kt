package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentBoardingBinding
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.utils.extensions.clickWithThrottle
import com.trianglz.iskipuserandroid.view.adapters.OnboardingViewPagerAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment


class OnBoardingFragment :
    BaseFragment<FragmentBoardingBinding>(FragmentBoardingBinding::inflate) {

    var isPasswordVisibe = false
    var curPos = 1

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        initListeners()
//        initObservers()
//        makeStatusBarTextColorWhite()

    }

    private fun initAdapter() {
        viewDataBinding?.viewPager?.adapter = OnboardingViewPagerAdapter(homeActivity!!)

    }

    private fun initListeners() {
        viewDataBinding?.apply {


            nextButton.clickWithThrottle {
                curPos = curPos.plus(1) ?: 1
                if (curPos == 2) {
                    displayItNoStack(LoginFragment())
                 //   setIsTutorialViewed()
                } else {
                    viewPager.currentItem = curPos
                    setBoardingCardData(curPos)
                    changeIndicator(curPos)
               //     _currentPosition.postValue(position)
                }

             //   viewModel.setCurrentPosition(viewModel.currentPosition.value?.plus(1) ?: 1)
            }

            skipBtn.clickWithThrottle {
                displayItNoStack(LoginFragment())
              //  viewModel.setIsTutorialViewed()
            }

            viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    curPos = position
                    viewPager.currentItem = curPos
                    setBoardingCardData(curPos)
                    changeIndicator(curPos)
//                    viewModel.setCurrentPosition(position)
                }
            })
        }

    }


    private fun setBoardingCardData(position: Int) {

//        viewDataBinding?.onboradingMainText?.text = when (position) {
//            0 -> getString(R.string.reservation_onboarding)
//            else -> getString(R.string.waiting_list_onboarding)
//        }

    }

    private fun changeIndicator(position: Int) {
        viewDataBinding?.apply {
            when (position) {
                0 -> {
                    ballOne.setImageResource(R.drawable.selected_indicator)
                    ballTwo.setImageResource(R.drawable.unselected_indicator)
//                    ballThree.setImageResource(R.drawable.unselected_indicator)
                }
                1 -> {
                    ballOne.setImageResource(R.drawable.unselected_indicator)
                    ballTwo.setImageResource(R.drawable.selected_indicator)
//                    ballThree.setImageResource(R.drawable.unselected_indicator)
                }
            }
        }
    }


}