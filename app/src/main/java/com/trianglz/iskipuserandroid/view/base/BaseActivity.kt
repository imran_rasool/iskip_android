package com.trianglz.iskipuserandroid.view.base

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.trianglz.iskipuserandroid.IskipUserApp
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.utils.GPSTracker
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.view.activities.HomeActivity
import java.util.*

abstract class BaseActivity<T : ViewBinding>(val bindingFactory: (LayoutInflater) -> T) :
    AppCompatActivity() {

    var viewDataBinding: T? = null
    private var reqCode: Int = 0
    private var progressDialog: Dialog? = null
    private var mFragmentManager: FragmentManager? = null
    var _serverTime = ""
    var currentFragment: Fragment? = null
    var permCallback: PermCallback? = null


    abstract fun setContainerLayout(): Int

    fun getviewDataBinding(): T {
        return viewDataBinding!!
    }


    fun hideStatausBar() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }


    fun getCurPatientPackage(): Boolean {
        return packageName == "com.appsums.patient"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = bindingFactory(layoutInflater)
        setContentView(viewDataBinding?.root)
        initializeProgressDialog()
    }

    fun showSettingsDialog(mActivity: AppCompatActivity) {
        androidx.appcompat.app.AlertDialog.Builder(mActivity).apply {
            setTitle("Need Permissions")
            setCancelable(false)
            setMessage("This app needs permission to use this feature. You can grant them in app settings.")
            setPositiveButton("GOTO SETTINGS") { dialog, _ ->
                dialog.dismiss()
                openSettings(mActivity)
            }
            show()
        }
    }

    fun openSettings(activity: AppCompatActivity) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity.packageName, null)
        intent.data = uri
        activity.startActivityForResult(intent, 1010)
    }

    fun clearAllStack() {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun displayItAddStack(mFragment: Fragment, bundle: Bundle? = null) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        currentFragment = mFragment
        val fragmentTransaction = supportFragmentManager
            .beginTransaction()

        // if (isBack) {
        fragmentTransaction.addToBackStack(mFragment::class.java.canonicalName)
        //}

        fragmentTransaction.replace(
            setContainerLayout(),
            currentFragment!!,
            mFragment::class.java.canonicalName
        )
            .commitAllowingStateLoss()
    }


    fun displayItAddStack(mFragment: Fragment, bundle: Bundle? = null, conatinerId: Int?) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        currentFragment = mFragment
        val fragmentTransaction = supportFragmentManager
            .beginTransaction()

        // if (isBack) {
        fragmentTransaction.addToBackStack(mFragment::class.java.canonicalName)
        //}

        fragmentTransaction.replace(
            conatinerId!!,
            currentFragment!!,
            mFragment::class.java.canonicalName
        )
            .commitAllowingStateLoss()
    }

    fun popBack() {
        supportFragmentManager.popBackStack()
    }

    fun displayItNoStack(mFragment: Fragment, bundle: Bundle? = null) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        currentFragment = mFragment
        val fragmentTransaction = supportFragmentManager
            .beginTransaction()

        // if (isBack) {
        //  fragmentTransaction.addToBackStack(mFragment::class.java.canonicalName)
        //}

        fragmentTransaction.replace(
            setContainerLayout(),
            currentFragment!!,
            mFragment::class.java.canonicalName
        )
            .commitAllowingStateLoss()
    }


    fun getCurFragment(): Fragment {
        return currentFragment!!
    }

    public fun setImageWithUrl(
        uri: String?,
        imgProfile: ImageView?
    ) {
        Glide.with(this)
            .load(uri)
            .placeholder(R.drawable.img_k_logo)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imgProfile!!)
    }

    fun callHomeActivity() {
        finish()
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        val view = currentFocus
        if (view != null && (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_MOVE) && view is EditText && !view.javaClass.name.startsWith(
                "android.webkit."
            )
        ) {
            val scrcoords = IntArray(2)
            view.getLocationOnScreen(scrcoords)
            val x = ev.rawX + view.left - scrcoords[0]
            val y = ev.rawY + view.top - scrcoords[1]
            if (x < view.left || x > view.right || y < view.top || y > view.bottom) {

                // if (currentFragment !is ChatFragment)
                (Objects.requireNonNull(this.getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).hideSoftInputFromWindow(
                    this.window.decorView.applicationWindowToken, 0
                )
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    fun showToast(msg: String?) {
        try {
            if (!msg.isNullOrEmpty())
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
        }
    }

    fun showToast(msg: Int) {
        try {
            Toast.makeText(this, getString(msg), Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {

        }
    }


    private fun initializeProgressDialog() {
//        progressDialog = Dialog(this, R.style.transparent_dialog_borderless)
//        val binding = DataBindingUtil.inflate<ViewDataBinding>(
//            LayoutInflater.from(this),
//            R.layout.progress_dialog,
//            null,
//            false
//        )
//        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        progressDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        progressDialog!!.setContentView(binding.root)
//        mFragmentManager = supportFragmentManager
//        progressDialog!!.setCancelable(false)
    }

    /*
    * Method to start progress dialog*/
    fun startProgressDialog() {
        if (progressDialog != null && !progressDialog!!.isShowing) {
            try {
                progressDialog!!.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    /*
     * Method to stop progress dialog*/
    fun stopProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            try {
                progressDialog!!.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun isLogin(): Boolean {
        val userId =
            SharedPrefClass().getPrefValue(IskipUserApp.instance, GlobalConstants.USERID)
        return userId != null
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == 321) {
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        checkFragment()!!.onActivityResult(requestCode, resultCode, data)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
//                else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
//                    val status = Autocomplete.getStatusFromIntent(data)
//                    Log.e("HOME", "Stuck in error$status")
//                }
                else if (resultCode == Activity.RESULT_CANCELED) {
                    Log.e("HOME", "Intent Cancelled")
                }
            }
        }
    }

    private fun checkFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(setContainerLayout())
    }

    fun logout() {
        // showToast(getString(R.string.logout_session))
        val notificationToken =
            SharedPrefClass().getPrefValue(this, GlobalConstants.DEVICE_ID).toString()

        SharedPrefClass().clearAll(this)
        SharedPrefClass().putObject(this, GlobalConstants.DEVICE_ID, notificationToken)
        startActivity(
            Intent(
                this,
                HomeActivity::class.java
            ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
        finishAffinity()
    }

    interface PermCallback {
        fun permGranted(resultCode: Int)

        fun permDenied(resultCode: Int)
    }


    fun checkPermissions(
        perms: Array<String>,
        requestCode: Int,
        permCallback: PermCallback
    ): Boolean {
        this.permCallback = permCallback
        this.reqCode = requestCode
        val permsArray = ArrayList<String>()
        var hasPerms = true
        for (perm in perms) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    perm
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                permsArray.add(perm)
                hasPerms = false
            }
        }
        if (!hasPerms) {
            val permsString = arrayOfNulls<String>(permsArray.size)
            for (i in permsArray.indices) {
                permsString[i] = permsArray[i]
            }
            ActivityCompat.requestPermissions(this@BaseActivity, permsString, 99)
            return false
        } else
            return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var permGrantedBool = false
        when (requestCode) {
            99 -> {
                for (grantResult in grantResults) {
                    if (grantResult == PackageManager.PERMISSION_DENIED) {
                        showPermissionCustomDialog(
                            this,
                            getString(R.string.not_sufficient_permissions)
                        )
                        permGrantedBool = false
                        break
                    } else {
                        permGrantedBool = true
                    }
                }
                if (permCallback != null) {
                    if (permGrantedBool) {
                        permCallback!!.permGranted(reqCode)
                    } else {
                        permCallback!!.permDenied(reqCode)
                    }
                }
            }

            9001 -> {
                for (grantResult in grantResults) {
                    if (grantResult == PackageManager.PERMISSION_DENIED) {
                        showPermissionCustomDialog(
                            this,
                            getString(R.string.not_sufficient_permissions)
                        )
                        permGrantedBool = false
                        break
                    } else {
                        permGrantedBool = true
                    }
                }
                if (permCallback != null) {
                    if (permGrantedBool) {
                        permCallback!!.permGranted(reqCode)
                    } else {
                        permCallback!!.permDenied(reqCode)
                    }
                }
            }
        }
    }

    fun showPermissionCustomDialog(context: Context, message: String) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.custom_permission_alert)
        val doneBT = dialog.findViewById<Button>(R.id.doneBT)
        val descTV = dialog.findViewById<TextView>(R.id.customAlertMsgTV)
        descTV.text = message
        doneBT.setOnClickListener {
            dialog.dismiss()
            var intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            var uri = Uri.fromParts("package", getPackageName(), null)
            intent.setData(uri)
            startActivity(intent)

        }
        dialog.show()
    }


    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            val nwInfo = connectivityManager.activeNetworkInfo ?: return false
            return nwInfo.isConnected
        }
    }


    private var mDialogLoader: Dialog? = null

    fun showLoader() {
        hideLoader()
        mDialogLoader = setLoadingDialog(this);
    }

    fun hideLoader() {
        if (mDialogLoader != null && mDialogLoader!!.isShowing) {
            mDialogLoader?.cancel()
        }
    }

    fun setLoadingDialog(activity: AppCompatActivity): Dialog {

        val dialog = Dialog(activity)
        dialog.show()
        if (dialog.window != null) {
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        dialog.setContentView(R.layout.loader)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }


    fun getCurrentLocationName(): Boolean {
        var chk = false
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
//                        val locationManager =
//                            getSystemService(Context.LOCATION_SERVICE) as LocationManager
//
//                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//                            OnGPS();
//                        } else {
//                            getLocation(locationManager);
//                        }

                        var gps = GPSTracker(this@BaseActivity)
                        // Check if GPS enabled
                        //       GlobalScope.async(Dispatchers.IO) {
                        var latitude = gps.latitude
                        var longitude = gps.longitude

                        //   withContext(Dispatchers.Main) {

                        if (gps.canGetLocation()) {
                            chk = true
                            if (gps.location != null) {

                                SharedPrefClass().putObject(
                                    this@BaseActivity,
                                    GlobalConstants.LATTITUDE,
                                    latitude
                                )
                                SharedPrefClass().putObject(
                                    this@BaseActivity,
                                    GlobalConstants.LONGITUDE,
                                    longitude
                                )

                                SharedPrefClass().putObject(
                                    this@BaseActivity,
                                    GlobalConstants.LOCNAME,
                                    getLocationAddress(latitude, longitude)
                                )

                            } else {

                            }

                        } else {
                            // Can't get location.
                            // GPS or network is not enabled.
                            chk = false
                            gps.showSettingsAlert()

                        }
                        //    }

                        //     }
//                        getCurLatLong()
//                        getCurrentLatlng()
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog(this@BaseActivity)
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {
                    chk = false
                    token?.continuePermissionRequest()
                }
            }).check()
        return chk
    }

    fun getLocationAddress(latitude: Double, longitude: Double): String? {
        val gcd = Geocoder(this@BaseActivity, Locale.getDefault())
        try {
            val addresses: List<Address> = gcd.getFromLocation(
                latitude, longitude,
                10
            )


            return addresses[0].locality + " " + addresses[0].adminArea + " " + addresses[0].postalCode


//                                    location.text =
//                                        locationname ?: ((addresses[0].getAddressLine(0))
//                                            ?: "")


        } catch (e: Exception) {
            e.stackTrace
            return null
        }

    }

}