package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.widget.RadioButton
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentSignupNewBinding
import com.trianglz.iskipuserandroid.model.GenericModel
import com.trianglz.iskipuserandroid.model.SignupModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.utils.UtilsFunctions
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response


class RegisterFragment : BaseFragment<FragmentSignupNewBinding>(FragmentSignupNewBinding::inflate) {

    var isPasswordVisibe = false
    var isPasswordVisibe2 = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            backButton.setOnClickListener {
                homeActivity?.onBackPressed()
            }

            nextButton.setOnClickListener {

                val radioButtonID: Int = rbGrp.checkedRadioButtonId
                val radioButton: View = rbGrp.findViewById(radioButtonID)
                val idx: Int = rbGrp.indexOfChild(radioButton)
                val r: RadioButton = rbGrp.getChildAt(idx) as RadioButton


                if (isValidate())
                    callAPi(
                        SignupModel(
                            getDeviceId(),
                            SharedPrefClass().getPrefValue(
                                requireContext(),
                                GlobalConstants.DEVICE_ID
                            )
                                .toString(),
                            getString(R.string.code_) + phoneEditText.text.toString(),
                            etEmail.text.toString(), "en", r.text.toString().toLowerCase(),
                            etName.text.toString(), true,
                            passwordEditText.text.toString()
                        )
                    )


//                if (isValidate())

//                displayItAddStack(TermsAndConditionsFragment())
            }


            passwordEditText.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= passwordEditText.getRight() - passwordEditText.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe = !isPasswordVisibe

                        if (isPasswordVisibe) {
                            passwordEditText.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            passwordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_show,
                                0
                            );
                        } else {
                            passwordEditText.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            passwordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_hide,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })


            secondPasswordEditText.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= secondPasswordEditText.getRight() - secondPasswordEditText.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe2 = !isPasswordVisibe2

                        if (isPasswordVisibe2) {
                            secondPasswordEditText.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            secondPasswordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_show,
                                0
                            );
                        } else {
                            secondPasswordEditText.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            secondPasswordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_hide,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })


        }
    }

    fun callAPi(body: SignupModel) {
        homeActivity?.let {
            ApiCall.signUp(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.ACCESS_TOKEN,
                        mResponse.body()?.customer?.auth_token
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.IS_VERIFIED,
                        mResponse.body()?.customer?.phone_verified
                    )

                    //    mResponse.body()?.terms_and_conditions_ar="https://www.google.co.in/"

                    if (!(mResponse.body()?.terms_and_conditions_url.isNullOrEmpty())) {

                        displayItAddStack(
                            TermsAndConditionsFragment(
                                mResponse.body()?.terms_and_conditions_url ?: ""
                            )
                        )
                    }


                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etEmail.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_email_address))
                return false
            } else if (!UtilsFunctions.isValidEmail(etEmail.text.toString())) {
                showToast(getString(R.string.invalid_email))
                return false
            } else if (etName.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_your_full_name))
                return false
            } else if (passwordEditText.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_password))
                return false
            } else if (secondPasswordEditText.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_confirm_password))
                return false
            } else if (!passwordEditText.text.toString()
                    .equals(secondPasswordEditText.text.toString(), ignoreCase = false)
            ) {
                showToast(getString(R.string.pass_does_not_match))
                return false
            } else {
                return true
            }
        }
        return true
    }


}