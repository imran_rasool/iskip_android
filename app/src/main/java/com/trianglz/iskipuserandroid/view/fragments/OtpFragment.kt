package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import com.trianglz.iskipuserandroid.databinding.FragmentOtpNewBinding
import com.trianglz.iskipuserandroid.model.*
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response


class OtpFragment(var from: Int, var mobileNo: String = "") :
    BaseFragment<FragmentOtpNewBinding>(FragmentOtpNewBinding::inflate) {

    private var countDownTimer: CountDownTimer?=null
    var isPasswordVisibe = false
    var otp = ""

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


             countDownTimer = object : CountDownTimer(60000, 1000) {
                override fun onTick(milliseconds: Long) {
                    val minutes: Long = milliseconds / 1000 / 60

                    val seconds: Long = milliseconds / 1000 % 60
                    viewDataBinding?.tvTimer?.setText("$minutes:$seconds sec to receive code")
                    viewDataBinding?.tvResend?.isEnabled = false
//                    Log.d("Timeeee",
//                        milliseconds.toString() + " Milliseconds = "
//                                + minutes + " minutes and "
//                                + seconds + " seconds."
//                    )
                    // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000)
                }

                override fun onFinish() {
                    viewDataBinding?.tvResend?.isEnabled = true

                    //  mTextField.setText("done!")
                }
            }
            countDownTimer?.start()


            tvResend.setOnClickListener {
                if (from == 0) {
                    callAPi(ForgotPasswordModel(mobileNo, true))
                } else {
                    callAPi(true)
                }


            }

            otpView.setOtpCompletionListener {
                otp = it
            }

            signInBtn.setOnClickListener {
                if (otp.length == 4) {
                    if (from == 0) {
                        callAPi(
                            OTPResetPasswordModel(
                                SharedPrefClass().getPrefValue(
                                    requireContext(),
                                    GlobalConstants.ACCESS_TOKEN_PASSWORD
                                ).toString(), otp
                            )
                        )
                    } else {
                        callAPi(OtpModel(otp))
                    }
                } else
                    showToast("Please enter otp")

            }

        }
    }

    fun callAPi(body: ForgotPasswordModel) {
        homeActivity?.let {
            ApiCall.forgotPassword(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {

                        var model = mResponse?.body()
                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.ACCESS_TOKEN_PASSWORD,
                            model?.token
                        )
                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }


    fun callAPi(body: OtpModel) {
        homeActivity?.let {
            ApiCall.otp(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    var model = mResponse?.body()?.customer

                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.IS_VERIFIED,
                        model?.phone_verified
                    )

                    homeActivity?.clearAllStack()
                    displayItNoStack(HomeFragment())

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(body: OTPResetPasswordModel) {
        homeActivity?.let {
            ApiCall.otpResetPassword(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    displayItAddStack(ResetPasswordFragment())
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(body: Boolean) {
        homeActivity?.let {
            ApiCall.resendCode(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    countDownTimer?.cancel()
                    countDownTimer?.start()
                    showToast(mResponse.body()?.message)
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}