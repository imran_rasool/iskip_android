package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentResetPassNewBinding
import com.trianglz.iskipuserandroid.model.GenericModel
import com.trianglz.iskipuserandroid.model.ResetPasswordModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response


class ResetPasswordFragment :
    BaseFragment<FragmentResetPassNewBinding>(FragmentResetPassNewBinding::inflate) {

    var isPasswordVisibe = false
    var isPasswordVisibe2 = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            submitButton.setOnClickListener {
                callAPi(
                    ResetPasswordModel(
                        SharedPrefClass().getPrefValue(
                            requireContext(),
                            GlobalConstants.ACCESS_TOKEN_PASSWORD
                        ).toString(), passwordEditText.text.toString()
                    )
                )
            }



            passwordEditText.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= passwordEditText.getRight() - passwordEditText.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe = !isPasswordVisibe

                        if (isPasswordVisibe) {
                            passwordEditText.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            passwordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_show,
                                0
                            );
                        } else {
                            passwordEditText.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            passwordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_hide,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })


            secondPasswordEditText.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= secondPasswordEditText.getRight() - secondPasswordEditText.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe2 = !isPasswordVisibe2

                        if (isPasswordVisibe2) {
                            secondPasswordEditText.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            secondPasswordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_show,
                                0
                            );
                        } else {
                            secondPasswordEditText.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            secondPasswordEditText.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.img_lock,
                                0,
                                R.drawable.ic_icon_hide,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })

        }
    }


    fun callAPi(body: ResetPasswordModel) {
        homeActivity?.let {
            ApiCall.resetPassword(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        homeActivity?.clearAllStack()
                        displayItNoStack(LoginFragment())
                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}