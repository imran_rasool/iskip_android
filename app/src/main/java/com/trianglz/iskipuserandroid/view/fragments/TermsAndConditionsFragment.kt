package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.trianglz.iskipuserandroid.databinding.FragmentTermsAndConditionsBinding
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.view.base.BaseFragment


class TermsAndConditionsFragment(val url: String) :
    BaseFragment<FragmentTermsAndConditionsBinding>(FragmentTermsAndConditionsBinding::inflate) {

    var isPasswordVisibe = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            tvText.setText(url)


        //    webView.loadUrl("https://www.journaldev.com");


            nextButton.setOnClickListener {
                displayItAddStack(OtpFragment(1))

            }
        }

    }



}