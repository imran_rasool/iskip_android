package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.view.adapters.RecyclerCallback
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentMenuBinding
import com.trianglz.iskipuserandroid.databinding.ItemMenuBinding
import com.trianglz.iskipuserandroid.model.*
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response
import java.lang.reflect.Type


class MenuFragment :
    BaseFragment<FragmentMenuBinding>(FragmentMenuBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<ItemModel, ItemMenuBinding>?=null
    private var catList= ArrayList<String>()
    private var storeListModel: StoreListModel?=null
    private  var tabListenree: TabLayout.OnTabSelectedListener?=null
    val list = ArrayList<ItemModel>()
    val listTab = ArrayList<ItemModel>()

    override fun getToolBar(): ToolBarModel {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gson = Gson()

        val type: Type = object : TypeToken<StoreListModel>() {}.type
        storeListModel =
            gson.fromJson(arguments?.getString(BUNDLECONSTANTS.OBJECT.name), type)


        viewDataBinding?.apply {
            backBtn.setOnClickListener {
                popBack()
            }
            setImageWithUrl(storeListModel?.cover_photo, storeCoverImage, R.drawable.menu)
            recyclerView.layoutManager = LinearLayoutManager(context)
            tabListenr()

        }

        setAdapter1()
        callAPi()
        storeListModel?.id?.let { callAPi(it) }
    }

    fun tabListenr(){
        if (tabListenree!=null){
            viewDataBinding?.itemTabLayout?.tabLayout?.removeOnTabSelectedListener(tabListenree)
        }
        tabListenree= object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                var tabSelect = tab?.position ?: 0
                //   Log.d("TABBBBBBB",tabSelect.toString())
                //       callApi(tabSelect)
                if (tabSelect>0){
                    var title = catList[tabSelect-1]

                    list.clear()
                    for (i in 0 until listTab.size) {
                        if (listTab[i].category_name == title) {
                            list.add(listTab[i])
                        }
                    }
                    rvAdapProgress?.notifyDataSetChanged()
                }else{
                    list.clear()
                    list.addAll(listTab)
                    rvAdapProgress?.notifyDataSetChanged()
                }
                chkEmptyList()

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        }
        viewDataBinding?.apply {
            itemTabLayout.tabLayout.addOnTabSelectedListener(tabListenree)
        }

    }

    fun setAdapter1() {
         rvAdapProgress = RecyclerViewGenricAdapter<ItemModel, ItemMenuBinding>(
            list,
            R.layout.item_menu, object :
                RecyclerCallback<ItemMenuBinding, ItemModel> {
                override fun bindData(
                    binding: ItemMenuBinding,
                    model: ItemModel,
                    position: Int,
                    itemView: View
                ) {

                    binding.apply {

                        setImageWithUrl(model.image, menuItemImage,R.drawable.menu)
                        itemNameTv.text = model.name
                        tvDesc.text = model.description
                        discountPriceTv.setText(""+ model?.price + " SAR")

                        root.setOnClickListener {
                            val storeMenuInfoFragment=MenuDetailDialogFragment(list[position])
                            storeMenuInfoFragment.show(activity?.supportFragmentManager!!,"MenuDetailDialogFragment")
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

    fun chkEmptyList() {
        viewDataBinding?.apply {
            if (list.isEmpty()) {
                swipeRefresh.gone()
                tvNoDataFound.visible()
            } else {
                swipeRefresh.visible()
                tvNoDataFound.gone()
            }
        }
    }

    fun callAPi() {
        homeActivity?.let {
            ApiCall.allCategory(it, "en", object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        //  zonesList.clear()
                        mResponse.body()?.setting_options?.let { it ->
                             catList=it.options
                           viewDataBinding?.apply {
                               itemTabLayout.apply {
                                   tabLayout.removeAllTabs()
                                   if (tabLayout.tabCount<=0){
                                       tabLayout.addTab(tabLayout.newTab().setText("All"))
                                      repeat(catList.size){ it ->
                                          tabLayout.addTab(tabLayout.newTab().setText(catList[it]))
                                      }
                                   }
                               }
                           }
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(id: Int) {
        homeActivity?.let {
            ApiCall.menuStoreItems(it, id, object : ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    if (mResponse.body() != null) {
                       var model = mResponse?.body()?.menu_items
                        model?.let {
                            list.addAll(it)
                            listTab.addAll(it)
                            rvAdapProgress?.notifyDataSetChanged()
                            chkEmptyList()
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}