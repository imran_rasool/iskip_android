package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsums.view.adapters.RecyclerCallback
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.trianglz.iskipuserandroid.IskipUserApp
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.*
import com.trianglz.iskipuserandroid.model.*
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.utils.TakePictureUtils
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.base.CustomDialog
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectDate
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectGuestNo
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectSeatingArea
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectSlot
import retrofit2.Response
import java.lang.reflect.Type


class StoreFragment :
    BaseFragment<FragmentStoresBinding>(FragmentStoresBinding::inflate) {

    private var tabListenree: TabLayout.OnTabSelectedListener? = null
    private var rvAdapProgress: RecyclerViewGenricAdapter<StoreListModel, ItemStoreNewBinding>? =
        null
    private var rvAdapSaveFav: RecyclerViewGenricAdapter<SavedListsItemM, ItemBottomListBinding>? =
        null
    private var saveFavlist = ArrayList<SavedListsItemM>()
    var isPasswordVisibe = false
    var list = ArrayList<StoreListModel>()
    var zonesList = ArrayList<ZonesModel>()
    private var layoutmanager: LinearLayoutManager? = null
    var isLoading = true
    var isLastPage = false
    var dialog: Dialog? = null
    private var storeAction = ""
    private var storeID = 0
    private var isLogintoken = false

    companion object {
        var cateNmae = ""
        var listAlreadySelect = ArrayList<String>()
    }

    var pageNO = 0
    var tabSelect = 0

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gson = Gson()
        val type: Type = object : TypeToken<ArrayList<ZonesModel>>() {}.type

        isLogintoken = SharedPrefClass().getPrefValue(
            IskipUserApp.instance,
            GlobalConstants.ACCESS_TOKEN
        ) != null

        zonesList =
            gson.fromJson(arguments?.getString(BUNDLECONSTANTS.ZONELIST.name), type)

        viewDataBinding?.apply {

            layoutmanager = LinearLayoutManager(context)
            recyclerView.layoutManager = layoutmanager

            backBtn.setOnClickListener {
                popBack()
            }

            filterBtn.setOnClickListener {
                displayItAddStack(FilterFragment())
            }

            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText("All"))
                repeat(zonesList.size) {
                    tabLayout.addTab(tabLayout.newTab().setText(zonesList[it].title))
                }
            }

            searchBtn.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    callApi(tabLayout.selectedTabPosition)
                    return@OnEditorActionListener true
                }
                false
            })

            tabListenr()
            setAdapter1()
            callApi(tabLayout.selectedTabPosition)
            callApi2()
        }


        /*   setFragmentResultListener(
               requestKey
           ) { _, result ->
               result.getString(FILTER)?.let { note ->
                   *//*  cateNmae = note
                  callAPi(
                      StoreModel(
                          lat,
                          long,
                          locale = "en",
                          category_name = note,
                          name = viewDataBinding?.searchBtn?.text.toString()
                      )
                  )*//*
            }

        }*/
    }


    fun tabListenr() {
        if (tabListenree != null) {
            viewDataBinding?.tabLayout?.removeOnTabSelectedListener(tabListenree)
        }
        tabListenree = object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tabSelect = tab?.position ?: 0
                callApi(tabSelect)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        }
        viewDataBinding?.apply {
            tabLayout.addOnTabSelectedListener(tabListenree)
        }

    }


    fun callApi(selectTab: Int) {
        if (selectTab >= 1)
            callAPiZone(
                ZoneStoreModel(
                    zoneId = zonesList[selectTab - 1].id, category_name = cateNmae,
                    name = viewDataBinding?.searchBtn?.text.toString()
                )
            )
        else
            callAPi(
                StoreModel(
                    lat, long, locale = "en", category_name = cateNmae,
                    name = viewDataBinding?.searchBtn?.text.toString(), public_api = isLogintoken
                )
            )
    }

    fun setAdapter1() {
        rvAdapProgress = RecyclerViewGenricAdapter<StoreListModel, ItemStoreNewBinding>(
            list,
            R.layout.item_store_new, object :
                RecyclerCallback<ItemStoreNewBinding, StoreListModel> {
                override fun bindData(
                    binding: ItemStoreNewBinding,
                    model: StoreListModel,
                    position: Int,
                    itemView: View
                ) {
                    binding.apply {
                        storeName.text = model.name
                        stateAndCategory.text = model.category_name
                        setImageWithUrl(model.cover_photo, storeBanner, R.drawable.menu)
                        setImageWithUrl(model.logo, storeLogo)
                        if (model.is_store_saved) {
                            imgBookmark.setImageResource(R.drawable.ic_icon_save_to_list_pressed)
                        } else {
                            imgBookmark.setImageResource(R.drawable.ic_save_pressed)
                        }
                        distance.text = "" + distance(
                            model.latitude.toFloat(), model.longitude.toFloat(),
                            (SharedPrefClass().getPrefValue(
                                homeActivity!!,
                                GlobalConstants.LATTITUDE
                            ) ?: 0.0).toString().toFloat(), (SharedPrefClass().getPrefValue(
                                homeActivity!!,
                                GlobalConstants.LATTITUDE
                            ) ?: 0.0).toString().toFloat()
                        ) + " km away"

                        storeBanner.setOnClickListener {
                            selectSeatingArea = 0
                            selectGuestNo = 0
                            selectSlot = ""
                            selectDate = ""
                            val bundle = Bundle()
                            bundle.putInt(BUNDLECONSTANTS.ID.name, list[position].id)
                            displayItAddStack(StoreDetailFragment(), bundle)
                        }

                        imgBookmark.setOnClickListener {
                            if (model.is_store_saved) {
                                // storeAction = "remove"
                                //storeID = model.id
                                //bottomSheet(homeActivity!!)

                            } else {
                                storeAction = "add"
                                storeID = model.id
                                bottomSheet(homeActivity!!)
                            }

                        }
                    }
                }
            })

        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
            recyclerView.addOnScrollListener(paginationScroll())

        }
    }


    private fun bottomSheet(context: Activity) {
        val binding: DialogBottomFavBinding?
        val builder = AlertDialog.Builder(
            requireContext()
        )
        //layout inflate custom
        val dialogView: View =
            LayoutInflater.from(context).inflate(R.layout.dialog_bottom_fav, null)
        builder.setView(dialogView)
        binding = DataBindingUtil.bind(dialogView)
        dialog = builder.create()

        binding?.apply {

            setAdapter2(rvBottomList)
            imgClose.setOnClickListener(View.OnClickListener {
                dialog?.dismiss()
            })

            builder.setCancelable(false)
            dialog?.setCancelable(false)
            dialog?.show()
            dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window!!.setGravity(Gravity.BOTTOM)
        }

    }


    private fun callApi3(body: UpsertUpdateModel) {
        homeActivity?.let {
            ApiCall.upsertUpdate(it, body, object : ResponseListener<GenericModel> {
                @SuppressLint("SetTextI18n")
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    dialog?.dismiss()

                    if (mResponse.body()?.success == true) {
                        rvAdapProgress?.notifyDataSetChanged()
                    }
                }

                override fun onError(msg: String) {
                    dialog?.dismiss()

                }
            })
        }
    }

    private fun callApi2() {
        homeActivity?.let {
            ApiCall.savedListsApi(it, object : ResponseListener<GenericModel> {
                @SuppressLint("SetTextI18n")
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        val model = mResponse?.body()?.saved_lists
                        if (model != null) {
                            saveFavlist.clear()

                            saveFavlist.addAll(model)

                            rvAdapSaveFav?.notifyDataSetChanged()

                        }


                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun setAdapter2(rvBottomList: RecyclerView) {

        rvAdapSaveFav = RecyclerViewGenricAdapter<SavedListsItemM, ItemBottomListBinding>(
            saveFavlist,
            R.layout.item_bottom_list, object :
                RecyclerCallback<ItemBottomListBinding, SavedListsItemM> {
                override fun bindData(
                    binding: ItemBottomListBinding,
                    model: SavedListsItemM,
                    position: Int,
                    itemView: View
                ) {

                    binding?.apply {

                        tvTitle.text = model.title

                        llItem.setOnClickListener {
                            if (storeAction != "") {
                                callApi3(
                                    UpsertUpdateModel(
                                        model.id,
                                        model.title,
                                        storeID,
                                        storeAction
                                    )
                                )
                            }
                        }
                    }

                }
            })

        rvBottomList.adapter = rvAdapSaveFav

    }

    fun callAPi(storeModel: StoreModel) {
        homeActivity?.let {
            ApiCall.allStores(it, storeModel, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        var response = mResponse?.body()
                        isLoading = response?.total_items ?: 0 >= response?.items ?: 0
                        isLastPage = !isLoading
                        //  zonesList.clear()
                        response?.stores?.let { it ->
                            list?.clear()
                            list?.addAll(it)
                            rvAdapProgress?.notifyDataSetChanged()
                            chkEmptyList()
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun chkEmptyList() {
        viewDataBinding?.apply {
            if (list.isEmpty()) {
                recyclerView.gone()
                tvNoDataFound.visible()
            } else {
                recyclerView.visible()
                tvNoDataFound.gone()
            }
        }

    }

    fun callAPiZone(storeModel: ZoneStoreModel) {
        homeActivity?.let {
            ApiCall.storesId(it, storeModel, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        //  zonesList.clear()
                        mResponse.body()?.stores?.let { it ->
                            list.clear()
                            list.addAll(it)
                            rvAdapProgress?.notifyDataSetChanged()
                            chkEmptyList()
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun paginationScroll(): RecyclerView.OnScrollListener {
        val recyclerViewOnScrollListener: RecyclerView.OnScrollListener =
            object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(
                    recyclerView: RecyclerView,
                    newState: Int
                ) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val visibleItemCount: Int = layoutmanager?.childCount ?: 0
                    val totalItemCount: Int = layoutmanager?.itemCount ?: 0
                    val firstVisibleItemPosition: Int =
                        layoutmanager?.findFirstVisibleItemPosition() ?: 0
                    if (isLoading && !isLastPage) {
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= 10
                        ) {
                            isLoading = false
                            pageNO += 1
                            viewDataBinding?.progressCircular?.show()
                            viewDataBinding?.progressCircular?.visibility = View.VISIBLE
                            callAPi(
                                StoreModel(
                                    lat,
                                    long,
                                    locale = "en",
                                    page = pageNO,
                                    public_api = isLogintoken
                                )
                            )

                        }
                    }
                }
            }
        return recyclerViewOnScrollListener
    }

}