package com.trianglz.iskipuserandroid.view.fragments

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentMenuDetailsBinding
import com.trianglz.iskipuserandroid.model.ItemModel


class MenuDetailDialogFragment(val model: ItemModel) : DialogFragment() {

    private var dialog1: Dialog? = null
    private var binding: FragmentMenuDetailsBinding? = null
    var isPasswordVisibe = false

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        dialog1 = super.onCreateDialog(savedInstanceState) as Dialog
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 40)
        dialog1?.window!!.setBackgroundDrawable(inset)
        return dialog1 as Dialog
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuDetailsBinding.inflate(inflater, container, false)

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            setImageWithUrl(model.image, itemImage, R.drawable.menu)
            itemNameTv.text = model.name
            tvDesc.text = model.description
            discountPriceTv.setText(""+ model?.price + " SAR")
            tvCalory.setText(""+ model?.calories + " CAL")
        }

    }

    fun setImageWithUrl(
        uri: String?,
        imgProfile: ImageView?, placeholder:Int?=null
    ) {
        Glide.with(requireContext())
            .load(uri)
            .placeholder(placeholder ?: R.drawable.img_k_logo)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imgProfile!!)
    }

}