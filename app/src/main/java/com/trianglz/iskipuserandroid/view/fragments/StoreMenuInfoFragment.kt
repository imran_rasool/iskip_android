package com.trianglz.iskipuserandroid.view.fragments

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.view.adapters.RecyclerCallback
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentStoreDetailsMoreInfoBinding
import com.trianglz.iskipuserandroid.databinding.ItemStoreDetailsMoreInfoSocialIconBinding
import com.trianglz.iskipuserandroid.model.StoreListModel
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import java.util.*
import kotlin.collections.ArrayList


class StoreMenuInfoFragment(var model: StoreListModel?) : DialogFragment() {

    private var rvAdapProgress: RecyclerViewGenricAdapter<SocialModel, ItemStoreDetailsMoreInfoSocialIconBinding>? =
        null
    private var dialog1: Dialog? = null
    private var binding: FragmentStoreDetailsMoreInfoBinding? = null
    var isPasswordVisibe = false
    val list = ArrayList<SocialModel>()


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        dialog1 = super.onCreateDialog(savedInstanceState) as Dialog
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 40)
        dialog1?.window!!.setBackgroundDrawable(inset)
        return dialog1 as Dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStoreDetailsMoreInfoBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            recyclerView.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            setAdapter1()

            shareBtn.setOnClickListener {
                shareText(getString(R.string.app_name), model?.website_url ?: "", false)
            }

            if (model != null) {
                nameTv.text = model?.name
                categoryTv.text = model?.category_name
                descriptionTv.text = model?.description_en
                descriptionTv1.text = model?.longitude?.let {
                    model?.latitude?.let { it1 ->
                        getLocationAddress(
                            it1,
                            it
                        )
                    }
                }
                descriptionTv11.text = model?.website_url
                cancellationPolicy.text = model?.cancellation_policy

                list.clear()
                if (!model?.facebook_url.isNullOrEmpty()) {
                    list.add(SocialModel(model?.facebook_url!!, R.drawable.facebook, "F"))
                }
                if (!model?.instagram_url.isNullOrEmpty()) {
                    list.add(SocialModel(model?.instagram_url!!, R.drawable.ic_icon_instgram, "I"))
                }
                if (!model?.twitter_url.isNullOrEmpty()) {
                    list.add(SocialModel(model?.twitter_url!!, R.drawable.ic_icon_twitter_2, "T"))
                }
                rvAdapProgress?.notifyDataSetChanged()

                if (model?.opening_hours?.isNullOrEmpty() == false)
                    statusAndOpeningTimeTv.text =
                        "From " + model?.opening_hours?.get(0)?.from_time + " To " + model?.opening_hours?.get(
                            0
                        )?.to_time
            }
        }

    }

    data class SocialModel(var url: String, var img: Int, var type: String)

    fun setAdapter1() {
        rvAdapProgress =
            RecyclerViewGenricAdapter<SocialModel, ItemStoreDetailsMoreInfoSocialIconBinding>(
                list,
                R.layout.item_store_details_more_info_social_icon, object :
                    RecyclerCallback<ItemStoreDetailsMoreInfoSocialIconBinding, SocialModel> {
                    override fun bindData(
                        binding: ItemStoreDetailsMoreInfoSocialIconBinding,
                        model: SocialModel,
                        position: Int,
                        itemView: View
                    ) {
                        binding.apply {
                            imgSocialIcon.setImageDrawable(
                                ContextCompat.getDrawable(
                                    context!!,
                                    model.img
                                )
                            )

                            imgSocialIcon.setOnClickListener {
                                var link = model.url
                                // newFacebookIntent(requireActivity().packageManager,link)
                                if (!link.startsWith("http://") && !link.startsWith("https://"))
                                    link = "https://$link"
                                if (model.type == "I")
                                    instagramApp(link)
                                else if (model.type == "T")
                                    twitterLink(link)
                                else if (model.type == "F")
                                    newFacebookIntent(link)
                            }
                        }
                    }
                })
        binding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

    fun newFacebookIntent( url: String) {

        val uri = Uri.parse(url)
        val likeIng = Intent(Intent.ACTION_VIEW, uri)
        likeIng.setPackage("com.facebook.katana")
       // requireContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
        try {
            startActivity(likeIng)
        } catch (e: Exception) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(url)
                )
            )
        }
        //  return Intent(Intent.ACTION_VIEW, uri)
    }

    fun instagramApp(url: String) {
        val uri = Uri.parse(url)
        val likeIng = Intent(Intent.ACTION_VIEW, uri)
        likeIng.setPackage("com.instagram.android")
        try {
            startActivity(likeIng)
        } catch (e: Exception) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(url)
                )
            )
        }
    }

    fun twitterLink(url: String) {
        var intent: Intent? = null
        try {
            // get the Twitter app if possible
            requireContext().packageManager.getPackageInfo("com.twitter.android", 0)
            intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        } catch (e: java.lang.Exception) {
            // no Twitter app, revert to browser
            intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        }
        this.startActivity(intent)
    }


    fun getLocationAddress(latitude: Double, longitude: Double): String? {
        val gcd = Geocoder(requireContext(), Locale.getDefault())
        try {
            val addresses: List<Address> = gcd.getFromLocation(
                latitude, longitude,
                10
            )
            Log.d("Ddd", addresses.toString())


            return (addresses[0].locality ?: "") + " " + (addresses[0].adminArea
                ?: "") + " " + (addresses[0].postalCode ?: "")


//                                    location.text =
//                                        locationname ?: ((addresses[0].getAddressLine(0))
//                                            ?: "")


        } catch (e: Exception) {
            e.stackTrace
            return null
        }

    }


    fun shareText(subject: String = "", extraText: String = "", isShareApp: Boolean = true) {
        try {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(
                Intent.EXTRA_SUBJECT,
                if (isShareApp) requireContext().packageName else subject
            )
            val shareAppExtraText = resources.getString(R.string.try_app_now).plus("\n")
                .plus("https://play.google.com/store/apps/details?id=" + requireContext().packageName)
            intent.putExtra(Intent.EXTRA_TEXT, if (isShareApp) shareAppExtraText else extraText)
            startActivity(
                Intent.createChooser(
                    intent,
                    this.resources.getString(R.string.share_via)
                )
            )
        } catch (e: Exception) {
            //e.toString();
        }
    }



}