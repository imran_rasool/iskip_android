package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.view.adapters.RecyclerCallback
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.kizitonwose.calendarview.utils.next
import com.kizitonwose.calendarview.utils.previous
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.*
import com.trianglz.iskipuserandroid.model.*
import com.trianglz.iskipuserandroid.utils.extensions.disable
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.invisible
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.ArraySpinnerAdapter
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectDate
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectSlot
import com.trianglz.iskipuserandroid.view.fragments.StoreDetailFragment.Companion.selectSlotStart
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


class SelectDateTimeDialogFragment :
    BaseFragment<FragmentSelectDateTimeBinding>(FragmentSelectDateTimeBinding::inflate) {
    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemSelectTimeWidthBinding>? =
        null
    private var binding: FragmentSelectDateTimeBinding? = null
    var selectedDate: LocalDate? = null
    private val today = LocalDate.now()
    private var model: StoreListModel? = null
    var selectSeatingArea1= StoreDetailFragment.selectSeatingArea
    var selectGuestNo1= StoreDetailFragment.selectGuestNo
    val listSlots = ArrayList<String>()
    val daysOfWeek = arrayOf(
        DayOfWeek.SATURDAY,
        DayOfWeek.SUNDAY,
        DayOfWeek.MONDAY,
        DayOfWeek.TUESDAY,
        DayOfWeek.WEDNESDAY,
        DayOfWeek.THURSDAY,
        DayOfWeek.FRIDAY,
    )
    val daysOfWeekString = arrayOf(
        "SATURDAY",
        "SUNDAY",
        "MONDAY",
        "TUESDAY",
        "WEDNESDAY",
        "THURSDAY",
        "FRIDAY",
    )


    override fun getToolBar(): ToolBarModel {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectPosition = -1
        binding = viewDataBinding!!
        val gson = Gson()
        val type: Type = object : TypeToken<StoreListModel>() {}.type
        model =
            gson.fromJson(arguments?.getString(BUNDLECONSTANTS.OBJECT.name), type)


        viewDataBinding?.apply {

            closeBtn.setOnClickListener { popBack() }
            offerTv.setText(
                Html.fromHtml("This restaurant requires a minimum reservation charge of " + "<b><font color=#f27238>" + model?.min_charge + " " + model?.currency + "</font></b>" + " per person")
            )
            val layoutManager1 =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            recycler.layoutManager = layoutManager1

            nextBtn.setOnClickListener {
                val firstElementPosition =
                    layoutManager1.findFirstCompletelyVisibleItemPosition()
                val lastElementPosition =
                    layoutManager1.findLastCompletelyVisibleItemPosition()
                recycler.smoothScrollToPosition(lastElementPosition + 2);
            }

            previousBtn.setOnClickListener {
                val firstElementPosition =
                    layoutManager1.findFirstCompletelyVisibleItemPosition()
                val lastElementPosition =
                    layoutManager1.findLastCompletelyVisibleItemPosition()

                if (firstElementPosition > 1)
                    recycler.smoothScrollToPosition(firstElementPosition - 2);

            }

            submitButton.setOnClickListener {
                if (selectPosition<=listSlots.size){
                    selectSlot = listSlots[selectPosition]
                    selectSlotStart = listSlots[0]
                }


                selectDate = if (selectedDate==null){
                    today.toString()
                }else
                    selectedDate.toString()
                StoreDetailFragment.selectSeatingArea = selectSeatingArea1
                StoreDetailFragment.selectGuestNo = selectGuestNo1
                popBack()
            }


        }

        setupCalendar()
        setAdapter()
        setSpinner()
        model?.store_seating_areas?.let { setSpinner1(it) }
    }

    fun chkSlotsList(){
        viewDataBinding?.submitButton?.isEnabled=false
        viewDataBinding?.apply {
            if (listSlots.isNotEmpty()){
                llSlots.visible()
                tvNoSlots.gone()
            }else{
                llSlots.gone()
                tvNoSlots.visible()
            }
        }
    }

    fun setAdapter() {

      //  listSlots.addAll(model?.booking_times?.get(0)?.time_slots ?: listSlots)

        Collections.sort(listSlots, Comparator<String?> { o1, o2 ->
            try {
                return@Comparator SimpleDateFormat("hh:mm a").parse(o1)
                    .compareTo(SimpleDateFormat("hh:mm a").parse(o2))
            } catch (e: Exception) {
                return@Comparator 0
            }
        })
        val c = Calendar.getInstance()
        val currentDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
        val dateformat = SimpleDateFormat("yyyy-MM-dd hh:mm aa")
        val datetime = dateformat.parse(dateformat.format(c.time))

        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemSelectTimeWidthBinding>(
            listSlots,
            R.layout.item_select_time_width, object :
                RecyclerCallback<ItemSelectTimeWidthBinding, String> {
                override fun bindData(
                    binding: ItemSelectTimeWidthBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    var appenddatetime = currentDate + " " + model
                    var slotDateTime = dateformat.parse(appenddatetime)


                    binding.apply {
                        textView.text = model
                        if (selectPosition == position) {
                            root.background = context!!.getDrawable(R.drawable.curved_background_8)
                            root.backgroundTintList =
                                context!!.resources.getColorStateList(R.color.dusty_orange, null)
                        } else {
                            root.background = null
                        }

                        if (selectedDate == null || selectedDate?.isEqual(today) == true) {
                            if (datetime.after(slotDateTime)) {
                                Log.d("Ddddd", "after")

                                textView.isEnabled = false
                                textView.setTextColor(setColor(R.color.brownish_grey))
                            }
                            if (datetime.before(slotDateTime)) {
                                textView.isEnabled = true
                                Log.d("Ddddd", "before")
                                textView.setTextColor(setColor(R.color.black))

                            }

                            if (datetime.equals(slotDateTime)) {
                                textView.isEnabled = false
                                textView.setTextColor(setColor(R.color.brownish_grey))

                                Log.d("Ddddd", "equals")

                            }
                        } else {
                            textView.isEnabled = true
                            textView.setTextColor(setColor(R.color.black))
                        }



                        textView.setOnClickListener {
                            if (selectPosition == position) {
                                selectPosition = -1
                              //  selectSlot = ""
                                viewDataBinding?.submitButton?.isEnabled=false
                            } else {
                                selectPosition = position
//                                selectSlot = listSlots[position]
                                viewDataBinding?.submitButton?.isEnabled=true
                            }
                            rvAdapProgress?.notifyDataSetChanged()
                        }
                    }
                }
            })

        viewDataBinding?.apply {
            recycler.adapter = rvAdapProgress
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupCalendar() {

        class DayViewContainer(view: View) : ViewContainer(view) {
            // With ViewBinding
            val textView = ItemCalendarDayLayoutBinding.bind(view).calendarDayText

            // Will be set when this container is bound. See the dayBinder.
            lateinit var day: CalendarDay

            init {


                view.setOnClickListener {
                    if (day.date != selectedDate && day.date >= today) {
                        //  StoreDetailsFragment.time=null
                        val oldDate = selectedDate
                        //  pickSlotAdapterCallBack.onSelectedDate(day.date)
                        selectedDate = day.date
                  //      selectDate = day.date.toString()
                        selectPosition = -1
                        //selectSlot = ""
                        binding?.calendar?.notifyDateChanged(day.date)
                        oldDate?.let { binding?.calendar?.notifyDateChanged(oldDate) }
                        listSlots.clear()

                        for (i in 0 until (model?.booking_times?.size ?: 0)) {
                            if (day.date.dayOfWeek.toString()
                                    .equals(model?.booking_times?.get(i)?.day, ignoreCase = true)
                            ) {
                                for (j in 0 until (model?.opening_hours?.size?:0)){
                                    if (model?.opening_hours?.get(j)?.day.equals(model?.booking_times?.get(i)?.day, ignoreCase = true)){
                                        model?.booking_times?.get(i)?.time_slots?.let { it1 ->
                                            listSlots.addAll(
                                                it1
                                            )
                                        }
                                    }
                                }
                            }
                        }
                        rvAdapProgress?.notifyDataSetChanged()
                        chkSlotsList()

                        // StoreDetailsFragment.dateSelect=selectedDate

                    }
                }
            }
        }

        class MonthViewContainer(view: View) : ViewContainer(view) {
            val bind = ItemCalendarMonthHeaderLayoutBinding.bind(view)
            val textView = bind.headerTextView

            init {
                bind.previousBtn.setOnClickListener {
                    binding?.calendar?.findFirstVisibleMonth()?.let {
                        binding?.calendar?.smoothScrollToMonth(it.yearMonth.previous)
                    }
                }

                bind.nextBtn.setOnClickListener {
                    binding?.calendar?.findFirstVisibleMonth()?.let {
                        binding?.calendar?.smoothScrollToMonth(it.yearMonth.next)
                    }
                }
            }
        }

        binding?.calendar?.dayBinder = object : DayBinder<DayViewContainer> {
            // Called only when a new container is needed.
            override fun create(view: View) = DayViewContainer(view)

            // Called every time we need to reuse a container.
            override fun bind(container: DayViewContainer, day: CalendarDay) {


                container.day = day
                container.textView.text = day.date.dayOfMonth.toString()
                if (day.owner == DayOwner.THIS_MONTH) {
                    when {
                        day.date < today -> {
                            container.textView.alpha = 0.7F
                            container.textView.disable()
                        }
                        selectedDate == day.date -> {
                            container.textView.setTextColor(
                                context!!.resources.getColor(
                                    R.color.white,
                                    null
                                )
                            )
                            container.textView.setBackgroundResource(R.drawable.curved_background_8)
                            container.textView.background.setTint(
                                context!!.resources.getColor(
                                    R.color.dark_blue_grey,
                                    null
                                )
                            )
                        }
                        today == day.date -> {
                            container.textView.setTextColor(
                                context!!.resources.getColor(
                                    R.color.dark_blue_grey,
                                    null
                                )
                            )
                            container.textView.setBackgroundResource(R.drawable.circle_background)
                            container.textView.background.setTint(
                                context!!.resources.getColor(
                                    R.color.pale_grey,
                                    null
                                )
                            )
                            listSlots.clear()

                            for (i in 0 until (model?.booking_times?.size ?: 0)) {
                                if (day.date.dayOfWeek.toString()
                                        .equals(model?.booking_times?.get(i)?.day, ignoreCase = true)
                                ) {
                                    for (j in 0 until (model?.opening_hours?.size?:0)){
                                        if (model?.opening_hours?.get(j)?.day.equals(model?.booking_times?.get(i)?.day, ignoreCase = true)){
                                            model?.booking_times?.get(i)?.time_slots?.let { it1 ->
                                                listSlots.addAll(
                                                    it1
                                                )
                                            }
                                        }
                                    }
                                }
                            }
                            rvAdapProgress?.notifyDataSetChanged()
                            chkSlotsList()
                        }
                        else -> {
                            container.textView.setTextColor(
                                context!!.resources.getColor(
                                    R.color.dark_blue_grey,
                                    null
                                )
                            )
                            container.textView.background = null
                        }
                    }
                } else {
                    container.textView.invisible()
                }
            }
        }


        binding?.calendar?.monthHeaderBinder =
            object : MonthHeaderFooterBinder<MonthViewContainer> {
                override fun create(view: View) = MonthViewContainer(view)
                override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                    container.textView.text = "${
                        DateTimeFormatter.ofPattern("MMM").format(month.yearMonth)
                            .toUpperCase(Locale.getDefault())
                    } ${month.year}"

                    Log.d("Dddd=====", container.textView.text.toString())

//                        if (seatingAreas.isNullOrEmpty() == false && StoreDetailsFragment.dateSelect != null) {
//                            selectedDate = StoreDetailsFragment.dateSelect
//                            pickSlotAdapterCallBack.onSelectedDate(StoreDetailsFragment.dateSelect!!)
//                        }
                }
            }
        val currentMonth = YearMonth.now()
        val lastMonth = currentMonth.plusMonths(10)

        val firstDayOfWeek = daysOfWeek.first()
        binding?.calendar?.setup(currentMonth, lastMonth, firstDayOfWeek)
        binding?.calendar?.scrollToMonth(currentMonth)


//            if (StoreDetailsFragment.dateSelect != null) {
//                selectedDate = StoreDetailsFragment.dateSelect
//                pickSlotAdapterCallBack.onSelectedDate(StoreDetailsFragment.dateSelect!!)
//            }

    }


    fun setSpinner() {
        val yrList = ArrayList<String>()

        for (i in 1 until 21) {
            yrList.add("" + i + "")
        }

        val arrayAdapter = ArraySpinnerAdapter(
            requireActivity(),
            yrList,
            object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, String> {
                override fun setDataSpinner(
                    binder: SpinnerItemBinding?,
                    position: Int,
                    model: String
                ) {

                    binder?.textSpinner?.text = model.toString()

                }

            }
        )

        viewDataBinding?.apply {

            guestsNumEditText.setOnClickListener {
                spinnerGuest.performClick()
            }
            spinnerGuest.adapter = arrayAdapter
            spinnerGuest.setSelection(StoreDetailFragment.selectGuestNo)
            spinnerGuest.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    guestsNumEditText.setText(yrList[p2].toString())
                    selectGuestNo1 = p2
                  //  StoreDetailFragment.selectGuestNo = p2
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }

    fun setSpinner1(list: ArrayList<SeatingAreaModel>) {


        val arrayAdapter = ArraySpinnerAdapter(
            requireActivity(),
            list,
            object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, SeatingAreaModel> {
                override fun setDataSpinner(
                    binder: SpinnerItemBinding?,
                    position: Int,
                    model: SeatingAreaModel
                ) {

                    binder?.textSpinner?.text = model.name.toString()

                }

            }
        )

        viewDataBinding?.apply {

            seatingAreaEditText.setOnClickListener {
                spinner.performClick()
            }
            spinner.adapter = arrayAdapter
            spinner.setSelection(StoreDetailFragment.selectSeatingArea)

            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    seatingAreaEditText.setText(list[p2].name.toString())
                   selectSeatingArea1 = p2
                  //  StoreDetailFragment.selectSeatingArea = p2
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }

}