package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import com.appsums.view.adapters.RecyclerCallback
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentFilterStoresBinding
import com.trianglz.iskipuserandroid.databinding.ItemFilterTagBinding
import com.trianglz.iskipuserandroid.model.GenericModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.GlobalConstants.requestKey
import com.trianglz.iskipuserandroid.utils.extensions.clickWithThrottle
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.utils.setFragmentResult
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.fragments.StoreFragment.Companion.cateNmae
import com.trianglz.iskipuserandroid.view.fragments.StoreFragment.Companion.listAlreadySelect
import retrofit2.Response

class FilterFragment :
    BaseFragment<FragmentFilterStoresBinding>(FragmentFilterStoresBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemFilterTagBinding>? = null
    var isPasswordVisibe = false
    var list = ArrayList<String>()
    var listSelect = ArrayList<Boolean>()

    override fun getToolBar(): ToolBarModel {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectPosition = -1
        viewDataBinding?.apply {
            val layoutManager = FlexboxLayoutManager(context)
            layoutManager.flexDirection = FlexDirection.ROW
            layoutManager.justifyContent = JustifyContent.CENTER
            layoutManager.alignItems = AlignItems.CENTER
            recyclerView.layoutManager = layoutManager
            applyButton.setOnClickListener {
                var append = ""
                listAlreadySelect.clear()

                for (i in 0 until listSelect.size) {
                    if (append.isNullOrEmpty() && listSelect[i]) {
                        append = list[i]
                        listAlreadySelect.add(list[i])
                    } else if (!append.isNullOrEmpty() && listSelect[i]) {
                        append = append +","+ list[i]
                        listAlreadySelect.add(list[i])
                    }
                }
                cateNmae=append
               /* setFragmentResult(
                    requestKey,
                    bundleOf(GlobalConstants.FILTER to append)
                )*/
                popBack()
            }
            resetButton.setOnClickListener {
          /*      setFragmentResult(
                    requestKey,
                    bundleOf(GlobalConstants.FILTER to "")
                )*/
                cateNmae=""
                popBack()
            }
            closeButton.setOnClickListener {
                popBack()
            }
        }
        setAdapter1()
        callAPi()
    }

    fun setAdapter1() {
        rvAdapProgress = RecyclerViewGenricAdapter<String, ItemFilterTagBinding>(
            list,
            R.layout.item_filter_tag, object :
                RecyclerCallback<ItemFilterTagBinding, String> {
                override fun bindData(
                    binding: ItemFilterTagBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {
                    binding.apply {
                        filteredText.text = model

                        if (listSelect[position] || listAlreadySelect.contains(model)) {
                            deleteButton.visible()
                            root.backgroundTintList=context!!.resources.getColorStateList(R.color.dusty_orange,null)
                        } else {
                            deleteButton.gone()
                            root.backgroundTintList=context!!.resources.getColorStateList(R.color.pale_grey,null)
                        }

                        deleteButton.setOnClickListener {
                            listSelect.set(position, !listSelect.get(position))
                            listAlreadySelect.remove(list[position])
                            rvAdapProgress?.notifyDataSetChanged()
                        }

                        root.clickWithThrottle {
                            listSelect.set(position, !listSelect.get(position))
                            listAlreadySelect.remove(list[position])
                            rvAdapProgress?.notifyDataSetChanged()
                        }
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

    fun callAPi() {
        homeActivity?.let {
            ApiCall.allCategory(it, "en", object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        //  zonesList.clear()
                        mResponse.body()?.setting_options?.let { it ->
                            listSelect.clear()
                            list.clear()
                            list.addAll(it.options)
                            rvAdapProgress?.notifyDataSetChanged()
                            repeat(list.size) {
                                listSelect.add(listAlreadySelect.contains(list[it]))
                            }
                            Log.d("Dddd", it.toString())
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}