package com.trianglz.iskipuserandroid.view.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.trianglz.iskipuserandroid.view.fragments.OnBoardingitemFragment

/**
 * Created by Indu Bala on 17/12/21.
 */

class OnboardingViewPagerAdapter(fragment: FragmentActivity) : FragmentStateAdapter(fragment) {
    override fun getItemCount() = 2
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> OnBoardingitemFragment().apply {
                arguments = Bundle().apply { putInt(OnBoardingitemFragment.POSITION, 0) } }
            1 -> OnBoardingitemFragment().apply {
                arguments = Bundle().apply { putInt(OnBoardingitemFragment.POSITION, 1) } }
            else -> throw IndexOutOfBoundsException()
        }
    }
}