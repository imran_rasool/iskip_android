package com.trianglz.iskipuserandroid.view.base

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RoundRectShape
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.Html
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.TakePictureDialogBinding
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.utils.TakePictureUtils
import com.trianglz.iskipuserandroid.utils.extensions.StringWrapper
import com.trianglz.iskipuserandroid.view.activities.HomeActivity
import com.trianglz.iskipuserandroid.view.listener.FragmentView
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


enum class RegexPatterns(private val pattern: Regex) {
    DIGITS_ONLY(Regex("^\\d+$")),
    EIGHT_OR_MORE(Regex(".{8,}")),
    FOUR_OR_MORE(Regex(".{4,}")),
    EIGHT_OR_MORE_ONE_UPPER_LOWER_NUMBER(Regex("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).*")),
    ;

    fun matches(string: String): Boolean {
        return pattern.matches(string)
    }
}

abstract class BaseFragment<T : ViewBinding>(val bindingFactory: (LayoutInflater) -> T) :
    Fragment(),
    FragmentView {

    private var timedialog: Dialog? = null
    var baseActivity: BaseActivity<ViewBinding>? = null
    var homeActivity: HomeActivity? = null
    var viewDataBinding: T? = null
    var mRootView: View? = null
    val passwordPattern = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$"
    val patternPassword = Pattern.compile(passwordPattern)
    val format = SimpleDateFormat("dd MMMM yyyy", Locale("en"))
    val format3 = SimpleDateFormat("MMMM dd,yyyy", Locale("en"))
    val AUTOCOMPLETE_REQUEST_CODE = 102

    var lat = 21.493964
    var long = 39.493964
    //var lat = 0.0
    //var long = 0.0

    var selectPosition = 0

    enum class BUNDLECONSTANTS {
        TITLE, WEBURL, ZONELIST, ID, OBJECT
    }

    fun getLattitude(): Float {
        return (SharedPrefClass().getPrefValue(
            homeActivity!!,
            GlobalConstants.LATTITUDE
        ) ?: 0.0).toString().toFloat()

    }

    fun getLongitude(): Float {
        return (SharedPrefClass().getPrefValue(
            homeActivity!!,
            GlobalConstants.LONGITUDE
        ) ?: 0.0).toString().toFloat()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        lat=  (SharedPrefClass().getPrefValue(
//            requireContext(),
//            GlobalConstants.LATTITUDE
//        ) ?: 28.536288450553798).toString().toDouble()
//        long= (SharedPrefClass().getPrefValue(
//            requireContext(),
//            GlobalConstants.LONGITUDE
//        ) ?: 77.38301406357039) .toString().toDouble()
        if (viewDataBinding == null) {
            viewDataBinding = bindingFactory(layoutInflater)
            return requireNotNull(viewDataBinding).root
        }
        return requireNotNull(viewDataBinding).root
    }


    fun underlineText(textview: TextView) {
        textview.paintFlags = textview.paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }

    fun stringToFloat(value: String?): Float {
        return value?.toFloat() ?: 0f
    }

    fun stringToInt(value: String?): Int {
        return value?.toInt() ?: 0
    }

    override fun onStart() {
        super.onStart()
        homeActivity?.setToolbarText(getToolBar() ?: ToolBarModel())
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        homeActivity = context as HomeActivity
        baseActivity = context as BaseActivity<ViewBinding>
    }

    fun clrNotification() {
        var nMgr =
            requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nMgr.cancelAll()
    }

    fun setHtmlText(text: String): Spanned? {
        return Html.fromHtml(text)
    }

    fun setImgTint(img: ImageView, color: Int) {
        ImageViewCompat.setImageTintList(
            img,
            ColorStateList.valueOf(setColor(color))
        )
    }

    fun setColor(colorId: Int): Int {
        return ContextCompat.getColor(requireContext(), colorId)
    }

    fun setDrawable(id: Int): Drawable? {
        return ContextCompat.getDrawable(requireContext(), id)
    }

    fun popBack() {
        homeActivity?.supportFragmentManager?.popBackStack()
    }

    fun getColor(colorId: Int): Int {
        return ContextCompat.getColor(requireActivity(), colorId)
    }

    fun getStringText(id: Int): String {
        return getString(id)
    }

    fun showToast(msg: String?) {
        homeActivity?.showToast(msg)
    }

    fun showToast(msg: Int) {
        homeActivity?.showToast(msg)
    }


    fun createShapeDrawable() {
        var r = 8
        var shape =
            ShapeDrawable(RoundRectShape(floatArrayOf(0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f), null, null))
        shape.paint.color = Color.RED
        //   view.setBackground(shape);
    }

    fun disableEMO(editText: EditText) {
        val EMOJI_FILTER = InputFilter { source, start, end, dest, dstart, dend ->
            for (index in start until end) {
                val type = Character.getType(source.get(index))
                if (type == Character.SURROGATE.toInt() || source == "☺") {
                    return@InputFilter ""
                }
            }
            null
        }
        editText.filters = arrayOf(EMOJI_FILTER)
    }

    fun disableEmojiLen(editText: EditText, len: Int) {
        val SPACE_FILTER = InputFilter { source, start, end, dest, dstart, dend ->
            for (index in start until end) {
                val type = Character.getType(source[index])
                if (type == Character.SURROGATE.toInt() || source == "☺") {
                    return@InputFilter ""
                }
            }
            null
        }
        editText.filters = arrayOf(SPACE_FILTER, LengthFilter(len))
    }

    private fun openSettings(activity: Activity) {

        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity.packageName, null)
        intent.data = uri
        activity.startActivityForResult(intent, 101)
    }

    fun displayItAddStack(mFragment: Fragment, bundle: Bundle? = null) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        homeActivity?.displayItAddStack(mFragment)
    }

    fun displayItNoStack(mFragment: Fragment, bundle: Bundle? = null) {
        if (bundle != null) {
            mFragment.arguments = bundle
        }
        homeActivity?.displayItNoStack(mFragment)
    }


    fun getDynamicText(id: Int, dynamicText: String?): String {
        return String.format(
            getString(id), dynamicText
        )
    }


    fun strikeText(content2: String, start: Int, end: Int): SpannableString {
        val spannableString2 = SpannableString(content2)
        spannableString2.setSpan(StrikethroughSpan(), start, end, 0)
        return spannableString2
    }

    fun strikeText(content2: String): SpannableString {
        val content = String.format(
            "", content2
        )
        val spannableString2 = SpannableString(content)
        spannableString2.setSpan(StrikethroughSpan(), 0, content.length, 0)
        return spannableString2
    }


    fun getRealPathFromURI(contentURI: Uri): String? {
        val result: String?
        val cursor = requireActivity().contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    fun getCount(textItemCount: AppCompatTextView): Int {
        var count: Int =
            textItemCount.text.trim().toString().toInt() + 1
        return count
    }

    fun getCountMinus(textItemCount: AppCompatTextView): Int {
        var count: Int =
            textItemCount.text.trim().toString().toInt() - 1
        return if (count >= 0)
            count
        else 1
    }


    fun isLogin(): Boolean {
        return homeActivity?.isLogin() ?: false
    }


    fun changeDateFormat(startDate: String?): String {
        try {
            // "2021-03-11 18:53:38"
            var date = startDate
            var spf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val newDate: Date = spf.parse(date)
            spf = SimpleDateFormat("dd-MM-yyyy hh:mm a")
            return spf.format(newDate)
        } catch (e: Exception) {
            return startDate!!
        }
    }

    fun changeDateFormat1(startDate: String?): String {
        return try {
            // "2021-03-11"
            var spf = SimpleDateFormat("yyyy-MM-dd")
            val newDate: Date = spf.parse(startDate)
            spf = SimpleDateFormat("dd-MMM-yyyy")
            spf.format(newDate)
        } catch (e: Exception) {
            startDate!!
        }
    }

    fun changeUTCDateFormat(utcDate: String): String {
        return try {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
            val date = dateFormat.parse(utcDate)
            val formatter =
                SimpleDateFormat("yyyy-MM-dd") //If you need time just put specific format for time like ‘HH:mm:ss’
            return formatter.format(date)
        } catch (e: Exception) {
            utcDate
        }
    }

    fun datePicker(eText: EditText, min: Boolean = false, max: Boolean = false) {
        val cldr = Calendar.getInstance()
        val day = cldr[Calendar.DAY_OF_MONTH]
        val month = cldr[Calendar.MONTH]
        val year = cldr[Calendar.YEAR]
        // date picker dialog
        // date picker dialog
        var picker = DatePickerDialog(
            requireContext(),
            { view, year, monthOfYear, dayOfMonth ->
                eText.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
            },
            year,
            month,
            day
        )

        if (min)
            picker.datePicker.minDate = System.currentTimeMillis() - 1000
        if (max)
            picker.datePicker.maxDate = System.currentTimeMillis() - 1000



        picker.show()
    }


    open fun showImageDialog(context: Activity, tempImage: String?, isDocument: Boolean) {
        val takePictureDailogBinding: TakePictureDialogBinding?
        val builder = AlertDialog.Builder(
            requireContext()
        )
        //layout inflate custom
        val dialogView: View =
            LayoutInflater.from(context).inflate(R.layout.take_picture_dialog, null)
        builder.setView(dialogView)
        takePictureDailogBinding = DataBindingUtil.bind(dialogView)
        val imagedialog = builder.create()
        takePictureDailogBinding?.apply {
            takePictureDailogBinding.tvCamera.setOnClickListener(View.OnClickListener {
                imagedialog.dismiss()
                TakePictureUtils.takePicture(context, tempImage)
            })
            takePictureDailogBinding.tvGallery.setOnClickListener(View.OnClickListener {
                imagedialog.dismiss()
                TakePictureUtils.openGallery(context)
            })
            takePictureDailogBinding.tvCancel.setOnClickListener(View.OnClickListener { imagedialog.dismiss() })
            takePictureDailogBinding.tvDocuments.setOnClickListener(View.OnClickListener {
                imagedialog.dismiss()
                TakePictureUtils.openPdf(context)
            })
            if (isDocument) {
                takePictureDailogBinding.tvCamera.setVisibility(View.GONE)
                takePictureDailogBinding.tvGallery.setVisibility(View.GONE)
                takePictureDailogBinding.view.setVisibility(View.GONE)
                takePictureDailogBinding.isdocView.setVisibility(View.GONE)
                takePictureDailogBinding.tvDocuments.setVisibility(View.VISIBLE)
                takePictureDailogBinding.tvCancel.setVisibility(View.VISIBLE)
            } else {
                takePictureDailogBinding.tvCamera.setVisibility(View.VISIBLE)
                takePictureDailogBinding.tvGallery.setVisibility(View.VISIBLE)
                takePictureDailogBinding.view.setVisibility(View.VISIBLE)
                takePictureDailogBinding.isdocView.setVisibility(View.GONE)
                takePictureDailogBinding.tvDocuments.setVisibility(View.GONE)
                takePictureDailogBinding.tvCancel.setVisibility(View.VISIBLE)
            }
            builder.setCancelable(false)
            imagedialog.setCancelable(false)
            imagedialog.show()
            imagedialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            imagedialog.window!!.setGravity(Gravity.BOTTOM)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("Ddd", "Ddsdssd")
    }


    public fun setImageWithUri(
        uri: Uri?,
        imgProfile: ImageView?
    ) {
        Glide.with(baseActivity!!)
            .load(uri)
            .placeholder(R.drawable.img_k_logo)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imgProfile!!)
    }

    public fun setImageWithUrl(
        uri: String?,
        imgProfile: ImageView?, placeholder: Int? = null
    ) {
        Glide.with(baseActivity!!)
            .load(uri)
            .placeholder(placeholder ?: R.drawable.img_k_logo)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imgProfile!!)
    }

    public fun setImageWithUri(
        uri: File?,
        imgProfile: ImageView?
    ) {
        Glide.with(baseActivity!!)
            .load(uri)
            .placeholder(R.drawable.img_k_logo)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imgProfile!!)
    }

    fun getCurPatientPackage(): Boolean {
        return activity?.packageName == "com.appsums.patient"
    }

    fun isOnline(context: Context): Boolean {
        return homeActivity?.isOnline(context) ?: false
    }

    fun getDeviceId(): String {
        val android_id = Settings.Secure.getString(
            getContext()?.getContentResolver(),
            Settings.Secure.ANDROID_ID
        )
        return android_id ?: ""
    }


    fun isPasswordValid(editText: EditText): Boolean {

        when {
            editText.text.isNullOrEmpty() -> {
                showToast(
                    getString(
                        R.string.cant_be_empty,
                        getString(R.string.password)
                    )
                )
                return false
            }
            !RegexPatterns.EIGHT_OR_MORE.matches(editText.text.toString()) -> {

                showToast(
                    getString(
                        R.string.too_short_less_than,
                        getString(R.string.password),
                        8
                    )
                )
                return false
            }
            !RegexPatterns.EIGHT_OR_MORE_ONE_UPPER_LOWER_NUMBER.matches(editText.text.toString()) -> {
                StringWrapper {
                    getString(
                        R.string.password_include_number_upper_case,
                        getString(R.string.password)
                    )
                }
                return false
            }
            //  else -> true
        }
        return true
    }

    fun distance(lat_a: Float, lng_a: Float, lat_b: Float, lng_b: Float): Int {
        val earthRadius = 3958.75
        val latDiff = Math.toRadians((lat_b - lat_a).toDouble())
        val lngDiff = Math.toRadians((lng_b - lng_a).toDouble())
        val a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
                Math.cos(Math.toRadians(lat_a.toDouble())) * Math.cos(Math.toRadians(lat_b.toDouble())) *
                Math.sin(lngDiff / 2) * Math.sin(lngDiff / 2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        val distance = earthRadius * c
        val meterConversion = 1609
        return (distance * meterConversion.toFloat()).toInt() / 1000
    }

}
