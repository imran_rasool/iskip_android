package com.trianglz.iskipuserandroid.view.fragments

import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.trianglz.iskipuserandroid.databinding.FragmentCheckoutBinding
import com.trianglz.iskipuserandroid.model.ReservationModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import java.lang.reflect.Type


class CheckoutFragment :
    BaseFragment<FragmentCheckoutBinding>(FragmentCheckoutBinding::inflate) {

    private var model: ReservationModel? = null

    override fun getToolBar(): ToolBarModel {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gson = Gson()
        val type: Type = object : TypeToken<ReservationModel>() {}.type
        model =
            gson.fromJson(arguments?.getString(BUNDLECONSTANTS.OBJECT.name), type)

        viewDataBinding?.apply {

            tvAmt.setText(""+((model?.number_of_guests ?: "0").toInt() * (model?.original_price ?: 0))+"")
            tvPricePerson.setText(""+(model?.original_price ?: 0))
            dateTv.setText(model?.reservation_date)
            tvGuestNo.setText(model?.number_of_guests)
            tvSeating.setText(model?.seating_area)

            btnNext.setOnClickListener {
                val gson = Gson()
                val json = gson.toJson(model)
                val bundle = Bundle()
                bundle.putString(BUNDLECONSTANTS.OBJECT.name, json)
                displayItAddStack(CheckoutPayFragment(),bundle)
            }




        }
    }


}