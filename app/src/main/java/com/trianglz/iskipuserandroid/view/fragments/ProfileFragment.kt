package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentProfileBinding
import com.trianglz.iskipuserandroid.model.GenericModel
import com.trianglz.iskipuserandroid.model.LoginModel
import com.trianglz.iskipuserandroid.model.LogoutModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response


class ProfileFragment :
    BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    var isPasswordVisibe = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = true
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            userLayout.userName.text =
                SharedPrefClass().getPrefValue(
                    requireContext(),
                    GlobalConstants.NAME
                ).toString()

            setImageWithUrl(
                SharedPrefClass().getPrefValue(requireContext(), GlobalConstants.PIC).toString(),
                userLayout.userImage, R.drawable.menu
            )

            btnProfile.setOnClickListener {
                displayItAddStack(CompleteProfileFragment(1))
            }

            termsConditionBtn.setOnClickListener {

            }

            logoutBtn.setOnClickListener {
                callAPi(LogoutModel(getDeviceId()))
            }

            llChnagePass.setOnClickListener {
                displayItAddStack(ChangePasswordFragment())
            }

            llNoti.setOnClickListener {
                displayItAddStack(NotificationFragment())
            }

            llsupport.setOnClickListener {
                displayItAddStack(TicketsFragment())
            }

        }
    }

    fun callAPi(body: LogoutModel) {
        homeActivity?.let {
            ApiCall.logout(it, body.device_id, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    homeActivity?.logout()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}