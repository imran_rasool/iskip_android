package com.trianglz.iskipuserandroid.view.base

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.*
import com.trianglz.iskipuserandroid.model.ParamModel
import com.trianglz.iskipuserandroid.view.activities.HomeActivity


class CustomDialog(val mContainer: HomeActivity) {

    var dialog: Dialog? = null

    private fun initDialog(layout: View): Dialog {
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
        dialog = CustomDialog(mContainer, R.style.mytheme)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(layout)
        return dialog!!
    }

    class CustomDialog(context: Context, themeId: Int) : Dialog(context, themeId) {
        override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
            currentFocus?.let {
                val imm: InputMethodManager = context.applicationContext.getSystemService(
                    Context.INPUT_METHOD_SERVICE
                ) as (InputMethodManager)
                imm.hideSoftInputFromWindow(it.windowToken, 0)
            }
            return super.dispatchTouchEvent(ev)
        }
    }


    fun openCancelAppDialog(

    ) {
        val binding = DataBindingUtil
            .inflate<DialogConfirmBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_confirm,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        binding.apply {
            btnCancel.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    fun openAlertDialog(
        message: String

    ) {
        val binding = DataBindingUtil
            .inflate<DialogAlertBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_alert,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        binding.apply {
            tvMSG.text = message
            tvOk.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    fun openDelAccountDialog(

    ) {
        val binding = DataBindingUtil
            .inflate<DialogDelAccBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_del_acc,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        binding.apply {
            btnCancel.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    fun openFavoriteDialog(
    ) {
        val binding = DataBindingUtil
            .inflate<DialogFavoriteBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_favorite,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        binding.apply {
            btnCancel.setOnClickListener {
                dialog.dismiss()
            }
            btnConfirm.setOnClickListener {
                dialog.dismiss()
            }
        }
    }


    fun openLoginDialog(
        dialogListener: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogDelAccBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_del_acc,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        binding.apply {
            header.text = "Please Login First"
            btnConfirm.text = "Ok"
            btnCancel.setOnClickListener {
                dialog.dismiss()
            }

            btnConfirm.setOnClickListener {
                dialog.dismiss()
                dialogListener.positiveBtn()

            }
        }
    }

    fun openAddttlAppDialog(dialogListener: DialogListener) {
        val binding = DataBindingUtil
            .inflate<DialogAddTtlBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_add_ttl,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        binding.apply {
            btnConfirm.setOnClickListener {
                dialog.dismiss()
            }

            btnConfirm.setOnClickListener {
                if (rb1.isChecked) {
                    dialog.dismiss()
                    dialogListener.positiveBtn()
                } else {
                    dialog.dismiss()
                    dialogListener.positiveBtn(ParamModel())
                }
            }

        }
    }

    fun openNewttlAppDialog(dialogListener: DialogListener) {
        val binding = DataBindingUtil
            .inflate<DialogNewTtlBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_new_ttl,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        binding.apply {
            btnCreate.setOnClickListener {
                dialog.dismiss()
                val title = edtTitle.text.toString()
                dialogListener.positiveBtn(title)
            }
        }
    }


    interface DialogListener {
        fun positiveBtn()
        fun positiveBtn(param: String)
        fun positiveBtn(paramModel: ParamModel)
    }


}