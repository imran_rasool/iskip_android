package com.trianglz.iskipuserandroid.view.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.AdapterView
import com.google.gson.Gson
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentStoreDetailsBinding
import com.trianglz.iskipuserandroid.databinding.SpinnerItemBinding
import com.trianglz.iskipuserandroid.model.*
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.ArraySpinnerAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response
import java.time.LocalDate

class StoreDetailFragment :
    BaseFragment<FragmentStoreDetailsBinding>(FragmentStoreDetailsBinding::inflate) {

    private var model: StoreListModel?=null
    private var storeId: Int? = null
    val yrList = ArrayList<String>()
    companion object{
        var selectSeatingArea=0
        var selectGuestNo=0
        var selectSlot=""
        var selectSlotStart=""
        var selectDate=""
    }

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        storeId = arguments?.getInt(BUNDLECONSTANTS.ID.name)

        if (yrList.isEmpty())
        for (i in 1 until 21) {
            yrList.add("" + i + "")
        }
        viewDataBinding?.apply {

            serviceNameTv.setText("Reserve a table "+selectDate)

            backBtn.setOnClickListener {
                popBack()
            }

            nextButton.isEnabled = !selectDate.isNullOrEmpty() && !selectSlot.isNullOrEmpty()

            selectTimeBtn.setOnClickListener {
                val gson = Gson()
                val json = gson.toJson(model)
                val bundle = Bundle()
                bundle.putString(BUNDLECONSTANTS.OBJECT.name, json)
                displayItAddStack(SelectDateTimeDialogFragment(), bundle)
            }

            locationLayout.setOnClickListener {
                try {
                    val intent = Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(model?.location!!))
//                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
//                }
                } catch (e: Exception) {

                }

            }
            waitingList.setOnCheckedChangeListener { buttonView, isChecked ->
                nextButton.isEnabled=isChecked
            }

            nextButton.setOnClickListener {
                if (waitingList.isChecked){
                        homeActivity?.clearAllStack()
                        displayItAddStack(ConfirmFragment())
                }else{
                   var reservationModel= ReservationModel(69, model?.services?.get(0)!!, selectDate,
                       selectSlot,seatingAreaEditText.text.toString(),guestsNumEditText.text.toString(),
                       "online","visa_master",original_price = (model?.min_charge?:0).toInt())
                    val gson = Gson()
                    val json = gson.toJson(reservationModel)
                    val bundle = Bundle()
                    bundle.putString(BUNDLECONSTANTS.OBJECT.name, json)
                    displayItAddStack(CheckoutFragment(),bundle)
                }
                   }

            ratingLayout.setOnClickListener {
                val gson = Gson()
                val json = gson.toJson(model)
                val bundle = Bundle()
                bundle.putString(BUNDLECONSTANTS.OBJECT.name, json)
                displayItAddStack(MenuFragment(),bundle)
            }

            moreInfoBtn.setOnClickListener {
                val storeMenuInfoFragment = StoreMenuInfoFragment(model)
                storeMenuInfoFragment.show(activity?.supportFragmentManager!!, "Info")
                // displayItAddStack(StoreMenuInfoFragment())
            }
            storeId?.let { callAPi(69) }
        }


    }

    fun callAPi(id: Int) {
        homeActivity?.let {
            ApiCall.storesDetail(it, id, object : ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    if (mResponse.body() != null) {
                         model = mResponse?.body()?.store
                        if (model != null)
                            viewDataBinding?.apply {

                                for (i in model?.services ?: ArrayList()) {

                                    if (i == "Both") {
                                        cardView.visible()
                                        cardWaiting.visible()
                                        selectTimeBtn.visible()
                                        timeSlotTwoTv.textView.visible()
                                        if (!model?.opening_hours.isNullOrEmpty()) {
                                            if (selectSlot.isNotEmpty()){
                                                timeSlotOneTv.textView.setText(selectSlotStart)
                                                timeSlotTwoTv.textView.setText(selectSlot)
                                                timeSlotTwoTv.textView.backgroundTintList=context!!.resources.getColorStateList(R.color.dusty_orange,null)
                                            }else{
                                                timeSlotOneTv.textView.setText(model?.opening_hours?.get(0)?.from_time)
                                                timeSlotTwoTv.textView.setText(model?.opening_hours?.get(0)?.to_time)
                                                 val today = LocalDate.now()

                                                for (j in 0 until (model?.opening_hours?.size ?: 0)) {
                                                    if (today.dayOfWeek.toString().toLowerCase()
                                                            .equals(model?.opening_hours?.get(j)?.day?.toLowerCase())
                                                    ) {
                                                        timeSlotOneTv.textView.setText(model?.opening_hours?.get(j)?.from_time)
                                                        timeSlotTwoTv.textView.setText(model?.opening_hours?.get(j)?.to_time)
                                                    }
                                                }

                                                timeSlotTwoTv.textView.backgroundTintList=context!!.resources.getColorStateList(R.color.dark_blue_grey,null)
                                            }
                                        }
                                    } else if (i == "Waitinglist") {
                                        cardWaiting.visible()
                                        cardView.gone()
                                        timeSlotTwoTv.root.gone()
                                        selectTimeBtn.gone()
                                        timeSlotOneTv.textView.text = getString(R.string.not_available)
                                    } else if (i == "Reservation") {
                                        cardWaiting.gone()
                                        selectTimeBtn.visible()
                                        cardView.visible()
                                        timeSlotTwoTv.textView.visible()
                                        if (!model?.opening_hours.isNullOrEmpty()) {
                                            //timeSlotOneTv.textView.setText(model?.opening_hours?.get(0)?.from_time)
                                            if (selectSlot.isNotEmpty()){
                                                timeSlotOneTv.textView.setText(selectSlotStart)
                                                timeSlotTwoTv.textView.setText(selectSlot)
                                                timeSlotTwoTv.textView.backgroundTintList=context!!.resources.getColorStateList(R.color.dusty_orange,null)
                                            }else{
                                                timeSlotOneTv.textView.setText(model?.opening_hours?.get(0)?.from_time)
                                                timeSlotTwoTv.textView.setText(model?.opening_hours?.get(0)?.to_time)
                                                val today = LocalDate.now()

                                                for (j in 0 until (model?.opening_hours?.size ?: 0)) {
                                                    if (today.dayOfWeek.toString().toLowerCase()
                                                            .equals(model?.opening_hours?.get(j)?.day?.toLowerCase())
                                                    ) {
                                                        timeSlotOneTv.textView.setText(model?.opening_hours?.get(j)?.from_time)
                                                        timeSlotTwoTv.textView.setText(model?.opening_hours?.get(j)?.to_time)
                                                    }
                                                }

                                                timeSlotTwoTv.textView.backgroundTintList=context!!.resources.getColorStateList(R.color.dark_blue_grey,null)
                                            }
                                        }
                                    }
                                }

                                setImageWithUrl(
                                    model?.cover_photo,
                                    storeCoverImage,
                                    R.drawable.menu
                                )
                                setImageWithUrl(model?.logo, storeLogoImage)
                                storeNameTv.text = model?.name


                                offerTv.setText(
                                    Html.fromHtml("This restaurant requires a minimum reservation charge of " + "<b><font color=#f27238>" + model?.min_charge + " " + model?.currency + "</font></b>" + " per person")
                                );

                                model?.store_seating_areas?.let { it1 -> setSpinner1(it1) }

                                setSpinner111()

                            }


                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


    fun setSpinner111() {


        val arrayAdapter = ArraySpinnerAdapter(
            requireActivity(),
            yrList,
            object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, String> {
                override fun setDataSpinner(
                    binder: SpinnerItemBinding?,
                    position: Int,
                    model: String
                ) {

                    binder?.textSpinner?.text = model.toString()

                }

            }
        )

        viewDataBinding?.apply {

            guestsNumEditText.setOnClickListener {
                spinnerGuest.performClick()
            }
            spinnerGuest.adapter = arrayAdapter
            spinnerGuest.setSelection(StoreDetailFragment.selectGuestNo)
            spinnerGuest.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    guestsNumEditText.setText(yrList[p2].toString())
                    selectGuestNo=p2
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }




    fun setSpinner1(list: ArrayList<SeatingAreaModel>) {


        val arrayAdapter = ArraySpinnerAdapter(
            requireActivity(),
            list,
            object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, SeatingAreaModel> {
                override fun setDataSpinner(
                    binder: SpinnerItemBinding?,
                    position: Int,
                    model: SeatingAreaModel
                ) {

                    binder?.textSpinner?.text = model.name.toString()

                }

            }
        )

        viewDataBinding?.apply {

            seatingAreaEditText.setOnClickListener {
                spinner.performClick()
            }
            spinner.adapter = arrayAdapter
            spinner.setSelection(StoreDetailFragment.selectSeatingArea)

            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    seatingAreaEditText.setText(list[p2].name.toString())
                    selectSeatingArea=p2
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }


}