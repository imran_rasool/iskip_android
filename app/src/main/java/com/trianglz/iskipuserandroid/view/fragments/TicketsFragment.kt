package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.appsums.view.adapters.RecyclerCallback
import com.google.gson.Gson
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentTicketListBinding
import com.trianglz.iskipuserandroid.databinding.ItemTicketBinding
import com.trianglz.iskipuserandroid.model.GenericModel
import com.trianglz.iskipuserandroid.model.LatLngModel
import com.trianglz.iskipuserandroid.model.SupportTicketsModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.px
import com.trianglz.iskipuserandroid.utils.extensions.screenWidth
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList


class TicketsFragment :
    BaseFragment<FragmentTicketListBinding>(FragmentTicketListBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<SupportTicketsModel, ItemTicketBinding>? =
        null
    val list = ArrayList<SupportTicketsModel>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(context)

            imgBack.setOnClickListener {
                popBack()
            }

            llNewTicket.setOnClickListener {
                displayItAddStack(NewTicketFragment())
            }

        }
        setAdapter1()
        callAPi()
    }


    fun setAdapter1() {


        rvAdapProgress = RecyclerViewGenricAdapter<SupportTicketsModel, ItemTicketBinding>(
            list,
            R.layout.item_ticket, object :
                RecyclerCallback<ItemTicketBinding, SupportTicketsModel> {
                override fun bindData(
                    binding: ItemTicketBinding,
                    model: SupportTicketsModel,
                    position: Int,
                    itemView: View
                ) {

                    binding.apply {
                        tvTicket.setText(model.ticket_number)
                        tvTime.setText(getTimeAgo(model.created_at))

                        tvViewMore.setOnClickListener {
                            displayItAddStack(TicketStatusFragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    private fun currentDate(): Date {
        val calendar = Calendar.getInstance()
        return calendar.time
    }

    fun getTimeAgo(date1: String): String? {

        //   var date = date1
        var spf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        val date: Date = spf.parse(date1)

        var SECOND_MILLIS = 1000;
        var MINUTE_MILLIS = 60 * SECOND_MILLIS;
        var HOUR_MILLIS = 60 * MINUTE_MILLIS;
        var DAY_MILLIS = 24 * HOUR_MILLIS;

        var time: Long = date.time
        if (time < 1000000000000L) {
            time *= 1000
        }
        val now: Long = currentDate().time
        if (time > now || time <= 0) {
            return "in the future"
        }
        val diff = now - time
        return if (diff < MINUTE_MILLIS) {
            "moments ago"
        } else if (diff < 2 * MINUTE_MILLIS) {
            "a minute ago"
        } else if (diff < 60 * MINUTE_MILLIS) {
            (diff / MINUTE_MILLIS).toString() + " minutes ago"
        } else if (diff < 2 * HOUR_MILLIS) {
            "an hour ago"
        } else if (diff < 24 * HOUR_MILLIS) {
            (diff / HOUR_MILLIS).toString() + " hours ago"
        } else if (diff < 48 * HOUR_MILLIS) {
            "yesterday"
        } else {
            (diff / DAY_MILLIS).toString() + " days ago"
        }
    }

    fun callAPi() {
        homeActivity?.let {
            ApiCall.supportTicketsList(it, "en", object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        list.clear()
                        list.addAll(mResponse.body()?.support_tickets ?: list)
                        rvAdapProgress?.notifyDataSetChanged()

                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}