package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentUserHomeBinding
import com.trianglz.iskipuserandroid.databinding.ItemZonesContainerBinding
import com.trianglz.iskipuserandroid.model.GenericModel
import com.trianglz.iskipuserandroid.model.LatLngModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.model.ZonesModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.px
import com.trianglz.iskipuserandroid.utils.extensions.screenWidth
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.fragments.StoreFragment.Companion.listAlreadySelect
import retrofit2.Response


class HomeFragment() :
    BaseFragment<FragmentUserHomeBinding>(FragmentUserHomeBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<String, ItemZonesContainerBinding>? = null
    var isPasswordVisibe = false
    var zonesList = ArrayList<ZonesModel>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = true
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            searchBtn.setOnClickListener {
                displayItAddStack(StoreSearchFragment())
            }
        }

        if (zonesList.isEmpty())
        callAPi(LatLngModel(lat, long))
    }


    fun callAPi(latLngModel: LatLngModel) {
        homeActivity?.let {
            ApiCall.zones(it, latLngModel, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        zonesList.clear()
                        mResponse?.body()?.zones?.let { it1 -> zonesList.addAll(it1) }


                        viewDataBinding?.apply {
                            if (zonesList.isEmpty()) {
                                llCrou.gone()
                                tvNoDataFound.visible()
                            } else {
                                llCrou.visible()
                                tvNoDataFound.gone()
                            }

                            // setup carousel
                            carousel.size = zonesList.size
                            carousel.spacing = 24.px
                            carousel.hideIndicator(true)
                            // handle on bind carousel item
                            carousel.setCarouselViewListener { view, position ->
                                view.updateLayoutParams<RecyclerView.LayoutParams> {
                                    width = context?.screenWidth?.times(0.75)?.toInt()!!
                                }
                                // declare views
                                val name = view.findViewById<TextView>(R.id.name)
                                val description = view.findViewById<TextView>(R.id.description)
                                val entryFees = view.findViewById<TextView>(R.id.entry_fees)
                                val logo = view.findViewById<ImageView>(R.id.logo)
                                setImageWithUrl(zonesList[position].logo.url, logo)
                                // get zone model
                                val zone = zonesList[position]
                                // bind data
                                name.text = zone.title
//                            description.text = zone.getDescriptionString(context)
//                            entryFees.text = zone.getEntryFeesString(context)
//                            logo.load(zone.logo) {
//                                defaultDrawable(R.drawable.img_placeholder)
//                            }
//                            // set click listeners
                                view?.setOnClickListener {
                                    listAlreadySelect.clear()
                                    val gson = Gson()
                                    val json = gson.toJson(zonesList)

                                    val bundle=Bundle()
                                    bundle.putString(BUNDLECONSTANTS.ZONELIST.name,json)
                                    displayItAddStack(StoreFragment(),bundle)
                                }
                            }
                            carousel.show()
                            // setup indicator
                            val recycler =
                                carousel.findViewById<RecyclerView>(R.id.carouselRecyclerView)
                            indicator.attachToRecyclerView(recycler)
                            try {
                                PagerSnapHelper().attachToRecyclerView(recycler)
                            } catch (e: Exception) {

                            }


                        }
                        //   rvAdapProgress?.notifyDataSetChanged()


                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}