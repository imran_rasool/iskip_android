package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentForgtPassNewBinding
import com.trianglz.iskipuserandroid.model.ForgotPasswordModel
import com.trianglz.iskipuserandroid.model.GenericModel
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.GlobalConstants
import com.trianglz.iskipuserandroid.utils.SharedPrefClass
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import retrofit2.Response


class ForgotPasswordFragment :
    BaseFragment<FragmentForgtPassNewBinding>(FragmentForgtPassNewBinding::inflate) {

    var isPasswordVisibe = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            nextButton.setOnClickListener {
                callAPi(ForgotPasswordModel(getString(R.string.code_) + phoneEditText.text.toString(), true))
            }

        }
    }

    fun callAPi(body: ForgotPasswordModel) {
        homeActivity?.let {
            ApiCall.forgotPassword(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                      //  displayItAddStack(OtpFragment(0))
                        var model=mResponse?.body()
                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.ACCESS_TOKEN_PASSWORD,
                            model?.token
                        )
                        displayItAddStack(OtpFragment(0,body.phone_number))
//                        SharedPrefClass().putObject(
//                            it,
//                            GlobalConstants.IS_VERIFIED,
//                            model?.phone_verified
//                        )
//                        if (model?.phone_verified==true)
//                            displayItAddStack(HomeFragment())
//                        else{
//                            displayItAddStack(OtpFragment(1))
//                        }
                    }

                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}