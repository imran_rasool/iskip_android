package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentBoardingBinding
import com.trianglz.iskipuserandroid.databinding.FragmentForgtPassNewBinding
import com.trianglz.iskipuserandroid.databinding.FragmentOnBoardingBinding
import com.trianglz.iskipuserandroid.model.ToolBarModel
import com.trianglz.iskipuserandroid.view.base.BaseFragment


class OnBoardingitemFragment :
    BaseFragment<FragmentOnBoardingBinding>(FragmentOnBoardingBinding::inflate) {

    companion object {
        const val POSITION = "pos"
    }


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleOnboardingImages()
    }

    private fun handleOnboardingImages(){
        viewDataBinding?.apply {
            when(arguments?.getInt(POSITION)){
                0->{
                    onboradingImg.setImageResource(R.drawable.slide_01)
                    backgroundView.setBackgroundResource(R.drawable.ic_onboarding_bg_01)
                }
                1->{
                    onboradingImg.setImageResource(R.drawable.slide_02)
                    backgroundView.setBackgroundResource(R.drawable.ic_onboarding_bg_02)
                }
            }
        }

    }

}