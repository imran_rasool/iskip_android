package com.trianglz.iskipuserandroid.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.appsums.view.adapters.RecyclerCallback
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.FragmentSavedBinding
import com.trianglz.iskipuserandroid.databinding.ItemSaveBinding
import com.trianglz.iskipuserandroid.databinding.ItemZonesContainerBinding
import com.trianglz.iskipuserandroid.model.*
import com.trianglz.iskipuserandroid.network.ApiCall
import com.trianglz.iskipuserandroid.network.ResponseListener
import com.trianglz.iskipuserandroid.utils.extensions.gone
import com.trianglz.iskipuserandroid.utils.extensions.visible
import com.trianglz.iskipuserandroid.view.adapters.RecyclerViewGenricAdapter
import com.trianglz.iskipuserandroid.view.base.BaseFragment
import com.trianglz.iskipuserandroid.view.base.CustomDialog
import retrofit2.Response


class SavedFragment() :
    BaseFragment<FragmentSavedBinding>(FragmentSavedBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<SavedListsItemM, ItemSaveBinding>? =
        null
    private var list = ArrayList<SavedListsItemM>()
    private var listUpsert = ArrayList<SavedListsItemM>()
    private var filterUpsert = ArrayList<String>()


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        toolBarModel.bottomBar = true
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = GridLayoutManager(context, 1)

            llNewListCraete.setOnClickListener {
                CustomDialog(homeActivity!!).openAddttlAppDialog(object :
                    CustomDialog.DialogListener {
                    override fun positiveBtn() {
                        llnewList.gone()
                        llNewListTop.visible()
                        swipeRefresh.visible()

                        callApi2(UpsertModel("Favorite Restaurants"))
                    }

                    override fun positiveBtn(param: String) {

                    }

                    override fun positiveBtn(paramModel: ParamModel) {
                        callAddTll()
                    }

                })
            }

            llNewListTop.setOnClickListener {
                callAddTll()
            }

        }

        setAdapter1()
        callApi()

    }

    private fun callApi() {
        homeActivity?.let {
            ApiCall.savedListsApi(it, object : ResponseListener<GenericModel> {
                @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body() != null) {
                        val model = mResponse?.body()?.saved_lists
                        if (model != null) {
                            list.clear()
                            filterUpsert.clear()

                            list.addAll(model)
                            val list1 = list.sortByDescending { it.id }
                            rvAdapProgress?.notifyDataSetChanged()

                            for (i in list.indices) {
                                filterUpsert.add(list[i].title)
                            }

                            viewDataBinding?.apply {
                                if (list.size > 0) {

                                    llnewList.gone()
                                    llNewListTop.visible()
                                    swipeRefresh.visible()

                                } else {
                                    llnewList.gone()
                                    llNewListTop.visible()
                                    swipeRefresh.visible()
                                }

                            }

                        }


                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun callAddTll() {
        var flag: Boolean = true
        viewDataBinding?.apply {
            CustomDialog(homeActivity!!).openNewttlAppDialog(object : CustomDialog.DialogListener {
                override fun positiveBtn() {
                }

                override fun positiveBtn(param: String) {

                    for (i in filterUpsert.indices) {
                        if (param.lowercase() == filterUpsert[i].lowercase()) {
                            flag = false
                        }
                    }

                    if (flag) {
                        callApi2(UpsertModel(param))
                    } else {
                        CustomDialog(homeActivity!!).openFavoriteDialog()
                    }

                }

                override fun positiveBtn(paramModel: ParamModel) {

                }

            })
        }
    }

    private fun callApi2(body: UpsertModel) {
        homeActivity?.let {
            ApiCall.upsert(it, body, object : ResponseListener<GenericModel> {
                @SuppressLint("SetTextI18n")
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (mResponse.body()?.success == true) {
                        val model = mResponse?.body()?.saved_list
                        if (model != null) {
                            listUpsert.clear()
                            listUpsert.addAll(model)

                            callApi()
                        }
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun setAdapter1() {

        rvAdapProgress = RecyclerViewGenricAdapter<SavedListsItemM, ItemSaveBinding>(
            list,
            R.layout.item_save, object :
                RecyclerCallback<ItemSaveBinding, SavedListsItemM> {
                @SuppressLint("SetTextI18n")
                override fun bindData(
                    binding: ItemSaveBinding,
                    model: SavedListsItemM,
                    position: Int,
                    itemView: View
                ) {

                    binding?.apply {


                        storeTitle.text = model.title
                        if (model.stores != null && model.stores.size > 0) {
                            tvCount.text = "" + model.stores.size + " Restaurant(s)"
                        } else {
                            tvCount.gone()
                        }
                        llImg.setOnClickListener {
                            if (model.stores.size > 0) {

                                val bundle = Bundle()
                                bundle.putSerializable(
                                    BUNDLECONSTANTS.ID.name,
                                    model.stores
                                )
                                displayItAddStack(SaveDetailFragment(), bundle)

                            } else {
                                CustomDialog(homeActivity!!).openAlertDialog("There is no Folder in this Restaurant")
                            }
                        }
                    }

                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}