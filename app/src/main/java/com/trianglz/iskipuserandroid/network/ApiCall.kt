package com.trianglz.iskipuserandroid.network


import com.trianglz.iskipuserandroid.model.*
import com.trianglz.iskipuserandroid.view.activities.HomeActivity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.File

/**
 * Created by Indu Bala on 27/11/21.
 */
object ApiCall {


    fun logout(
        homeActivity: HomeActivity, model: String, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .logout(model)
            )
        }
    }

    fun login(
        homeActivity: HomeActivity, model: LoginModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .login(model)
            )
        }
    }

    fun createReservation(
        homeActivity: HomeActivity,
        model: ReservationModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .createReservation(model)
            )
        }
    }

    fun zones(
        homeActivity: HomeActivity,
        latLngModel: LatLngModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .zones(latLngModel.lat, latLngModel.long)
            )
        }
    }

    fun supportTicketsList(
        homeActivity: HomeActivity, locale: String, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .supportTicketsList(locale)
            )
        }
    }

    fun allStores(
        homeActivity: HomeActivity, storeModel: StoreModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            var hashMap = HashMap<String, String>()
            if (storeModel.category_name.isNotEmpty())
                hashMap.put("category_name", storeModel.category_name)
            if (storeModel.name.isNotEmpty())
                hashMap.put("name", storeModel.name)
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .allStores(storeModel.lat!!, storeModel.long!!, hashMap)
                //storeModel.locale,storeModel.category_name,storeModel.name,storeModel.items,storeModel.page)
            )
        }
    }

    fun storesId(
        homeActivity: HomeActivity, storeModel: ZoneStoreModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            var hashMap = HashMap<String, String>()
            if (storeModel.category_name.isNotEmpty())
                hashMap.put("category_name", storeModel.category_name)
            if (storeModel.name.isNotEmpty())
                hashMap.put("name", storeModel.name)

            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .storesZoneId(storeModel.zoneId!!, hashMap)
                //storeModel.locale,storeModel.category_name,storeModel.name,storeModel.items,storeModel.page)
            )
        }
    }

    fun storesDetail(
        homeActivity: HomeActivity, id: Int, callback: ResponseListener<GenericModel1>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()

            val mApiService = ApiService<GenericModel1>()
            mApiService.get(
                object : ApiResponse<GenericModel1> {
                    override fun onResponse(mResponse: Response<GenericModel1>) {
                        checkCode1(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel1>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .storesDetail(id)
                //storeModel.locale,storeModel.category_name,storeModel.name,storeModel.items,storeModel.page)
            )
        }
    }

    fun menuStoreItems(
        homeActivity: HomeActivity, id: Int, callback: ResponseListener<GenericModel1>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()

            val mApiService = ApiService<GenericModel1>()
            mApiService.get(
                object : ApiResponse<GenericModel1> {
                    override fun onResponse(mResponse: Response<GenericModel1>) {
                        checkCode1(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel1>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .menuStoreItems(id)
                //storeModel.locale,storeModel.category_name,storeModel.name,storeModel.items,storeModel.page)
            )
        }
    }

    fun getPaymentCheckout(
        homeActivity: HomeActivity, id: Int, name: String, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()

            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .getPaymentCheckout(id, name)
                //storeModel.locale,storeModel.category_name,storeModel.name,storeModel.items,storeModel.page)
            )
        }
    }


    fun allCategory(
        homeActivity: HomeActivity, locale: String, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .allCategory(locale)
            )
        }
    }

    fun signUp(
        homeActivity: HomeActivity, model: SignupModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .register(model)
            )
        }
    }

    fun forgotPassword(
        homeActivity: HomeActivity,
        model: ForgotPasswordModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .forgotPassword(model)
            )
        }
    }

    fun resetPassword(
        homeActivity: HomeActivity,
        model: ResetPasswordModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .resetPassword(model)
            )
        }
    }

    fun resendCode(
        homeActivity: HomeActivity, model: Boolean, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .resendCode(model)
            )
        }
    }

    fun otp(
        homeActivity: HomeActivity, model: OtpModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .verifyPhnNo(model)
            )
        }
    }


    fun otpResetPassword(
        homeActivity: HomeActivity,
        model: OTPResetPasswordModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .verifyOtpPassword(model)
            )
        }
    }


    fun reservationList(
        homeActivity: HomeActivity,
        reservation: ReservationListModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .reservationList(
                        reservation.locale,
                        reservation.items,
                        reservation.page,
                        reservation.type
                    )
            )
        }
    }

    fun customerDetail(
        homeActivity: HomeActivity, id: Int, callback: ResponseListener<GenericModel1>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()

            val mApiService = ApiService<GenericModel1>()
            mApiService.get(
                object : ApiResponse<GenericModel1> {
                    override fun onResponse(mResponse: Response<GenericModel1>) {
                        checkCode1(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel1>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .customerDetail(id)
                //storeModel.locale,storeModel.category_name,storeModel.name,storeModel.items,storeModel.page)
            )
        }
    }

    fun updateProfile(
        homeActivity: HomeActivity,
        body: ProfileUpdateModel,
        callback: ResponseListener<GenericModel1>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()

            val mApiService = ApiService<GenericModel1>()
            mApiService.get(
                object : ApiResponse<GenericModel1> {
                    override fun onResponse(mResponse: Response<GenericModel1>) {
                        checkCode1(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel1>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .updateProfile(body)
            )
        }
    }


    fun savedListsApi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()

            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .savedLists()
            )
        }
    }

    fun upsert(
        homeActivity: HomeActivity, body: UpsertModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()

            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .savedListsUpsert(body)
            )
        }
    }

    fun upsertUpdate(
        homeActivity: HomeActivity, body: UpsertUpdateModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()

            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .upsertUpdate(body)
            )
        }
    }


    fun deleteSaveList(
        homeActivity: HomeActivity, id: Int, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()

            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .deleteSaveLists(id)
            )
        }
    }


    fun checkCode(
        mResponse: Response<GenericModel>,
        callback: ResponseListener<GenericModel>,
        homeActivity: HomeActivity
    ) {
        if (mResponse.body()?.success == true) {
            homeActivity.showToast(mResponse.body()?.message)
            callback.onSuccess(mResponse)
        } else {
            if (mResponse.code() == 401) {
                try {
                    val jObjError = JSONObject(mResponse.errorBody()?.string())
                    homeActivity.showToast(jObjError.getString("message"))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                //   homeActivity.logout()
            } else {
                try {
                    val jObjError = JSONObject(mResponse.errorBody()?.string())
                    homeActivity.showToast(jObjError.getString("message"))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            //      callback.onError(true)
//            if (jObjError.has("result")) {
//                var jsonObject = jObjError.getJSONObject("result")
//                if (jsonObject.has("isVerified"))
//                    if (jsonObject.getBoolean("isVerified")) {
//                        callback.onError(true)
//                    } else {
//                        if (jsonObject.has("_id"))
//                            SharedPrefClass().putObject(
//                                homeActivity,
//                                GlobalConstants.USERID,
//                                jsonObject.getString("_id")
//                            )
//                        callback.onError(false)
//                    }
//
//            }

        }

    }

    fun checkCode1(
        mResponse: Response<GenericModel1>,
        callback: ResponseListener<GenericModel1>,
        homeActivity: HomeActivity
    ) {
        if (mResponse.body()?.success == true) {
            homeActivity.showToast(mResponse.body()?.message)
            callback.onSuccess(mResponse)
        } else {
            try {
                val jObjError = JSONObject(mResponse.errorBody()?.string())
                homeActivity.showToast(jObjError.getString("message"))
            } catch (e: Exception) {
                e.printStackTrace()
            }

            //      callback.onError(true)
//            if (jObjError.has("result")) {
//                var jsonObject = jObjError.getJSONObject("result")
//                if (jsonObject.has("isVerified"))
//                    if (jsonObject.getBoolean("isVerified")) {
//                        callback.onError(true)
//                    } else {
//                        if (jsonObject.has("_id"))
//                            SharedPrefClass().putObject(
//                                homeActivity,
//                                GlobalConstants.USERID,
//                                jsonObject.getString("_id")
//                            )
//                        callback.onError(false)
//                    }
//
//            }

        }

    }


}

interface ResponseListener<T> {
    fun onSuccess(mResponse: Response<T>)
    fun onError(msg: String)
    fun onError(msg: Boolean) {
    }
}

