package com.trianglz.iskipuserandroid.network

import com.trianglz.iskipuserandroid.model.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface API {

    companion object {
        const val subUrl = "v1/customer_app/customers/"
        const val subUrl1 = "v1/customer_app/"
    }

    @POST(subUrl + "classic_registration")
    fun register(
        @Body body: SignupModel
    ): Call<GenericModel>

    @PATCH(subUrl + "forgot_password")
    fun forgotPassword(
        @Body body: ForgotPasswordModel
    ): Call<GenericModel>

    @PATCH(subUrl + "reset_password")
    fun resetPassword(
        @Body body: ResetPasswordModel
    ): Call<GenericModel>


    @POST(subUrl1 + "sessions")
    fun login(
        @Body body: LoginModel
    ): Call<GenericModel>

    @POST(subUrl1 + "reservations")
    fun createReservation(
        @Body body: ReservationModel
    ): Call<GenericModel>

    @GET(subUrl1 + "zones")
    fun zones(
        @Query("lat") lat: Double,
        @Query("long") long: Double
    ): Call<GenericModel>

    @GET(subUrl1 + "support_tickets")
    fun supportTicketsList(
        @Query("locale") locale: String
    ): Call<GenericModel>

    @GET(subUrl1 + "stores")
    fun allStores(
        @Query("lat") lat: Double,
        @Query("long") long: Double,
        @QueryMap hashMap: HashMap<String, String>
    ): Call<GenericModel>

    @GET(subUrl1 + "stores/{zone_id}/list_stores")
    fun storesZoneId(
        @Path("zone_id") zone_id: Int, @QueryMap hashMap: HashMap<String, String>
    ): Call<GenericModel>

    @GET(subUrl1 + "stores/{id}")
    fun storesDetail(
        @Path("id") id: Int
    ): Call<GenericModel1>

    @GET(subUrl1 + "stores/{id}/store_items")
    fun menuStoreItems(
        @Path("id") id: Int
    ): Call<GenericModel1>

    @GET(subUrl1 + "reservations/{id}/request_checkout_id")
    fun getPaymentCheckout(
        @Path("id") id: Int, @Query("payment_option") payment_option: String
    ): Call<GenericModel>

    @GET(subUrl1 + "stores/all_category")
    fun allCategory(
        @Query("locale") locale: String
    ): Call<GenericModel>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = subUrl1 + "sessions", hasBody = true)
    fun logout(
        @Field("device_id") device_id: String
    ): Call<GenericModel>


    @PATCH(subUrl + "verify_phone_number")
    fun verifyPhnNo(
        @Body body: OtpModel
    ): Call<GenericModel>

    @PATCH(subUrl + "verify_otp")
    fun verifyOtpPassword(
        @Body body: OTPResetPasswordModel
    ): Call<GenericModel>

    @GET(subUrl + "resend_phone_verificaiton_code")
    fun resendCode(@Query("no_sms") model: Boolean): Call<GenericModel>

    @GET(subUrl1 + "reservations")
    fun reservationList(
        @Query("locale") locale: String, @Query("items") items: Int,
        @Query("page") page: Int, @Query("type") type: String
    ): Call<GenericModel>

    @GET(subUrl1 + "customers/{id}")
    fun customerDetail(
        @Path("id") id: Int
    ): Call<GenericModel1>

    @PATCH(subUrl + "update_profile")
    fun updateProfile(
        @Body body: ProfileUpdateModel
    ): Call<GenericModel1>

    @GET(subUrl1 + "saved_lists")
    fun savedLists(): Call<GenericModel>

    @POST(subUrl1 + "saved_lists/upsert")
    fun savedListsUpsert(
        @Body body: UpsertModel
    ): Call<GenericModel>

    @GET(subUrl1 + "saved_lists/{id}")
    fun deleteSaveLists(
        @Path("id") id: Int
    ): Call<GenericModel>


    @POST(subUrl1 + "saved_lists/upsert")
    fun upsertUpdate(
        @Body body: UpsertUpdateModel
    ): Call<GenericModel>

}