package com.trianglz.iskipuserandroid

import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho
import com.google.firebase.auth.PhoneAuthProvider
import timber.log.Timber

class IskipUserApp :  Application() {
    private val TAG = IskipUserApp::class.java.name

    companion object {
        @get:Synchronized
        lateinit var instance: IskipUserApp

        var resendToken: PhoneAuthProvider.ForceResendingToken?=null
    }

    override fun onCreate() {
        super.onCreate()

        instance = this
        initTimber()
        initStetho()
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}