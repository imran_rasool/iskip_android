package com.trianglz.iskipuserandroid.utils.extensions


import com.trianglz.iskipuserandroid.utils.enum.SimpleDateFormatEnum
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import java.text.ParseException
import java.util.*
import java.util.concurrent.TimeUnit.*
import kotlin.math.roundToInt
import kotlin.runCatching as catching

fun String?.shortenName(): String{
    val name = this ?: return ""
    val subNameArray = name.split("\\s+".toRegex(), 2)
    val shortName = StringBuilder()
    for (i in subNameArray.indices) {
        val currentName = subNameArray[i]
        val firstLetter = currentName.toCharArray()[0]
        shortName.append(firstLetter.toUpperCase())
    }
    return shortName.toString()
}
fun String?.getFirstAlphabet(default: String = "S") = this?.trim()
    ?.firstOrNull { "[A-z]".toRegex().matches(it.toString()) }?.toUpperCase()
    ?: default

/**
 * Make sure the string you call this function on is a number with a fraction
 */
fun String?.removeFollowingZeroes() = this?.toDoubleOrNull()?.roundToInt()?.toString() ?: ""
fun String?.lowerCaseWithCapitalFirst() = this?.toLowerCase(Locale.ROOT)?.capitalize(Locale.ROOT) ?: ""
//region Dates and time
fun String?.daysHoursMins(): String
{
    val dateTime = DateTime(this)
    val now = DateTime()
    val timeElapsed = now.millis - dateTime.millis
    val days = MILLISECONDS.toDays(timeElapsed)
    val hours = MILLISECONDS.toHours(timeElapsed) - DAYS.toHours(days)
    val minutes = MILLISECONDS.toMinutes(timeElapsed) - HOURS.toMinutes(hours) - DAYS.toMinutes(days)
    val timeBuilder= StringBuilder()
    if (days > 0) timeBuilder.append("${days}d")
    timeBuilder.append(" ")
    if (hours > 0) timeBuilder.append("${hours}hr")
    timeBuilder.append(" ")
    if (minutes > 0) timeBuilder.append("${minutes}m")
    return timeBuilder.toString()
}

fun String?.formatDate(
    inputFormat: SimpleDateFormatEnum = SimpleDateFormatEnum.DATE_INPUT,
    returnFormat: SimpleDateFormatEnum,
): String? {
    var date: Date? = null
    try {
        date = this.let {
            if (it != null) {
                val formatter = inputFormat.simpleDateFormat
                val dateUTC = formatter.parse(it)
                val x = formatter.format(dateUTC!!)
                formatter.parse(x)
            } else null
        }
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return if (date == null) {
        catching {
            val inputDate = DateTime(this, DateTimeZone.forOffsetHours(3))
            val outFormat = DateTimeFormat.forPattern(returnFormat.simpleDateFormat.toPattern())
            outFormat.print(inputDate)
        }.getOrElse { "" }
    }
    else {
        date.let { returnFormat.simpleDateFormat.format(it) }
    }
}
fun Date?.format(
    returnFormat: SimpleDateFormatEnum
): String {
    val date: Date? = this
    var result: String = ""
    try {
        date?.let {
            result = returnFormat.simpleDateFormat.format(it)
        }
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return result
}
fun String?.toDate(
    inputFormat: SimpleDateFormatEnum,
): Date? {
    var date: Date? = null
    try {
        val string = this ?: ""
        val formatter = inputFormat.simpleDateFormat
        val dateUTC = formatter.parse(string)
        date = dateUTC
    } catch (e : Exception) {

    }
    return date
}

val String?.dayMonthYearHour : String
    get() {
        return formatDate(returnFormat = SimpleDateFormatEnum.DAY_MONTH_YEAR_HOUR) ?: ""
    }
val String.dayMonthYear : String
    get() {
        return formatDate(returnFormat = SimpleDateFormatEnum.DAY_MONTH_YEAR3) ?: ""
    }

//endregion

/**
 * Easy way to convert String to [com.trianglz.iskipuserandroid.commons.presentation.model.StringWrapper]
 */
fun String?.wrap() = StringWrapper(this ?: "")