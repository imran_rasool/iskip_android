package com.trianglz.iskipuserandroid.utils.notificationbadge

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.databinding.NotificationBadgeLayoutBinding
import com.trianglz.iskipuserandroid.utils.notificationbadge.NotificationBadge

class SimpleNotificationBadge: NotificationBadge {
    private lateinit var binding: NotificationBadgeLayoutBinding

    constructor(context: Context) : super(context) {
        initSimpleNotificationBadge()
    }
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initSimpleNotificationBadge(attrs)
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    ) {
        initSimpleNotificationBadge(attrs)
    }

    //region Private API
    private fun initSimpleNotificationBadge(attrs: AttributeSet? = null) {
        // inflate layout
        inflateLayout()
    }

    private fun inflateLayout() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.notification_badge_layout
        , this, true)
    }
    //endregion

}