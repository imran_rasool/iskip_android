package com.trianglz.iskipuserandroid.utils.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Point
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.use
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import kotlin.reflect.KProperty1

//region Toast
fun Context.toast(@StringRes resId: Int, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, getString(resId), length).show()
}

fun Context.toast(string: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, string, length).show()
}
//endregion

//region Activity navigation
inline fun <reified T : AppCompatActivity> Context.toActivity(
    vararg params: Pair<KProperty1<out Any?, Any?>, Any?>
) {
    val extras = params.map { it.first.name to it.second }.toTypedArray()
    val intent = Intent(this, T::class.java)
    intent.putExtras(bundleOf(*extras))
    startActivity(intent)
    (this as AppCompatActivity).finish()
}
//endregion

//region Activity navigation without finish() & with FLAG_ACTIVITY_NEW_TASK
inline fun <reified T : AppCompatActivity> Context.toActivityAsNewTask(extras: Bundle.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    intent.putExtras(Bundle().apply(extras))
    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
    startActivity(intent)
}
//endregion

//region Measurements
inline val Context.screenWidth: Int
    get() = Point().also {
        (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getSize(
            it
        )
    }.x
inline val Context.screenHeight: Int
    get() = Point().also {
        (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getSize(
            it
        )
    }.y

fun Context.getStatusBarHeight(): Int {
    var result = 0
    val context = this
    val resourceId =
        context.resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = context.resources.getDimensionPixelSize(resourceId)
    }
    return result
}

//endregion

//region Life Cycle
fun Context.lifeCycleOwner(): LifecycleOwner? {
    return if (this is Fragment) {
        viewLifecycleOwner
    } else {
        (this as? LifecycleOwner)
    }
}
//endregion

//region Resources
/**
 * For example the string is R.string.submit we can pass submit as a string we get the same
 * string as if we were using string res Integer
 */
fun Context.getStringByIdName(idName: String?): String {
    val res: Resources = resources
    return res.getString(res.getIdentifier(idName, "string", packageName))
}
//endregion

//region theme color

@ColorInt
@SuppressLint("Recycle")
fun Context.themeColor(
    @AttrRes themeAttrId: Int
): Int {
    return obtainStyledAttributes(
        intArrayOf(themeAttrId)
    ).use {
        it.getColor(0, Color.MAGENTA)
    }
}
//endregion