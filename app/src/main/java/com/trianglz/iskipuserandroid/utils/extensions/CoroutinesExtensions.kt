package com.trianglz.iskipuserandroid.utils.extensions

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
* Switches the thread to [Dispatchers.Main] without retyping withContext
*
* @param block code executed
* @see launch extension function
*/
suspend inline fun <R> main(crossinline block: () -> R): R {
    return withContext(Dispatchers.Main){
        block()
    }
}
/**
 * Switches the thread to [Dispatchers.Default] without retyping withContext
 *
 * @param block code executed
 * @see launch extension function
 */
suspend inline fun <R> default(crossinline block: () -> R): R {
    return withContext(Dispatchers.Default){
        block()
    }
}
/**
 * Switches the thread to [Dispatchers.IO] without retyping withContext
 *
 * @param block code executed
 * @see launch extension function
 */
suspend fun <R> io(block: suspend () -> R): R {
    return withContext(Dispatchers.IO){
        block()
    }
}