package com.trianglz.iskipuserandroid.utils.extensions

import android.content.Context
import android.content.res.Resources
import androidx.core.content.ContextCompat
import com.trianglz.iskipuserandroid.R
import java.util.concurrent.TimeUnit

//region Time
fun Int.minsToHourMins(): String {
    val hours = TimeUnit.MINUTES.toHours(this.toLong())
    val minutes = (this - TimeUnit.HOURS.toMinutes(hours))
    return "$hours h $minutes min"
}
fun Long.millisToMinSec(): String {
    val minutes = TimeUnit.MILLISECONDS.toMinutes(this)
    val seconds = (TimeUnit.MILLISECONDS.toSeconds(this) - TimeUnit.MINUTES.toSeconds(minutes))
    return "$minutes min $seconds sec"
}

fun Long.millisToHrMinSec(): String {
    val hours = TimeUnit.MILLISECONDS.toHours(this)
    val minutes = TimeUnit.MILLISECONDS.toMinutes(this) - TimeUnit.HOURS.toMinutes(hours)
    val seconds = (TimeUnit.MILLISECONDS.toSeconds(this) - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minutes))
    return "$hours : $minutes : $seconds"
}
//endregion

//region Measurements
val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

val Int.dpAsPixels: Int 
    get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()
//endregion

//region UI
fun Int.getColorInt(context: Context): Int {
    return ContextCompat.getColor(context, this)
}
//endregion

//region Calculations
val Long.positiveOrZero: Long
    get() {
        if (this < 0) return 0
        return this
    }
val Int.positiveOrZero: Int
    get() {
        if (this < 0) return 0
        return this
    }
//endregion

// region Time
fun Int.to12HourFormat(context: Context): Pair<Int, String> {
    return if (this <= 12) {
        Pair(this, context.getString(R.string.am))
    } else {
        Pair((this - 12), context.getString(R.string.pm))
    }
}

fun Int.twoDigitFormat(): String {
    return String.format("%02d", this)
}
// endregion Time

// region currency

fun Double.getPriceWithCurrency(context: Context, currency: String = context.getString(R.string.saudi_currency)): String {
    return " ${context.getString(R.string.price_with_currency, this.toString(), currency)}"
}
// endregion currency

// region Decimals
fun Double.reduceDecimalNumbers(number: Int=2): Double{
    return this.toString().replace(
        "(?<=\\.\\d{${ if(number< 1) 1 else number}})\\d+".toRegex(), "")
        .toDoubleOrNull() ?: 0.0
}
// endregion

