package com.trianglz.iskipuserandroid.utils

import com.trianglz.iskipuserandroid.IskipUserApp
import com.trianglz.iskipuserandroid.R
import android.provider.Settings.Secure





object GlobalConstants {



    @JvmStatic
    val BASE_URL = "http://3.19.69.119/api/"

    @JvmStatic
    val BASE_URL_IMG = "http://3.19.69.119/public/"

    @JvmStatic
    val SHARED_PREF = IskipUserApp.instance.resources.getString(R.string.shared_pref)

    @JvmStatic
    val ACCESS_TOKEN_PASSWORD = "access_token_passwordf"
    @JvmStatic
    val ACCESS_TOKEN = "access_token"

  @JvmStatic
    val IS_VERIFIED = "IS_VERIFIED"

    @JvmStatic
    var DEVICE_ID = "device_id"

    var UNIQUE_DEVICE_ID = "unique_device_id"

    @JvmStatic
    var Device_Type = "Android"

    @JvmStatic
    var APP_VERSION = "1.0"

    @JvmStatic
    val USERID = "user_id"


    @JvmStatic
    val NAME = "NAME"


    @JvmStatic
    val EMAIL = "EMAIL"


    @JvmStatic
    val PIC = "PIC"


    @JvmStatic
    val LATTITUDE = "LATTITUDE"



    @JvmStatic
    val LONGITUDE = "LONGITUDE"

    val FILTER = "FILTER"

    val requestKey = "requestKey"



    @JvmStatic
    val LOCNAME = "LOCNAME"





    const val EMAIL_PATTERN =
        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
}




