package com.trianglz.iskipuserandroid.commons.presentation.avatarplaceholder

sealed class LettersNumberMode(val styleableId: Int) {
    object One: LettersNumberMode(1)
    object Two: LettersNumberMode(2)

    companion object {

        fun getModeByStyleableId(styleableId: Int): LettersNumberMode {
            return LettersNumberMode::class.nestedClasses
                .find { (it.objectInstance as? LettersNumberMode)
                    ?.styleableId == styleableId }
                ?.objectInstance as? LettersNumberMode
                ?: One
        }
    }
}
