package com.trianglz.iskipuserandroid.utils.extensions

import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Context
import android.content.res.Resources
import android.graphics.Rect
import android.graphics.Typeface
import android.icu.util.Calendar
import android.os.Parcelable
import android.os.SystemClock
import android.text.InputFilter
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.TouchDelegate
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.bold
import androidx.core.view.ViewCompat
import androidx.core.view.get
import androidx.core.view.updatePadding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.load
import coil.request.ImageRequest
import coil.request.ImageResult
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.utils.notificationbadge.NotificationBadge

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch



//region Pagination
/**
 * Add scrolling listener which then triggers the [paginationSource] to load the next page
 *
 * @param paginationSource
 * @param getPositionOffset needed if there is something above the paginated list like in
 *  [com.trianglz.iskipstore.commons.presentation.pagination.adapter.CustomTopPaginationAdapter]
 *  if value of offset is -1 no pagination will be triggered
 */
fun RecyclerView.addPaginationTrigger(onCheckPosition: ((Int) -> Unit),
                                      getPositionOffset: (() -> Int) = { 0 }) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            getPositionOffset().takeUnless { it < 0 } ?: return
            onCheckPosition(layoutManager.findLastVisibleItemPosition().run { minus(getPositionOffset()) })
        }
    })
}

//fun RecyclerView.addPaginationTrigger(paginationSource: BasePaginationSource<*>,
//                                      getPositionOffset: (() -> Int) = { 0 }) {
//    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//            super.onScrolled(recyclerView, dx, dy)
//            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
//            getPositionOffset().takeUnless { it < 0 } ?: return
//            paginationSource.checkPosition(layoutManager.findLastVisibleItemPosition().run { minus(getPositionOffset()) })
//        }
//    })
//}
//endregion

//region Measurements
inline val View.screenWidth: Int
    get() = context!!.screenWidth
fun View.expand(duration: Int) {
    val v = this
    v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    val targetHeight = v.measuredHeight

    // Older versions of android (pre API 21) cancel animations for views with a height of 0.
    v.layoutParams.height = 1
    v.visibility = View.VISIBLE
    val a: Animation = object : Animation() {
        override fun applyTransformation(
                interpolatedTime: Float,
                t: Transformation
        ) {
            v.layoutParams.height =
                if (interpolatedTime == 1f) ViewGroup.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
            v.requestLayout()
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }
    if (duration == -1) {
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toLong()
    } else {
        a.duration = duration.toLong()
    }
    v.startAnimation(a)

}
//endregion

//region Clicking
/**
 * Prevent multiple fast clicks
 *
 * @param throttleTime
 * @param action
 */
fun View.clickWithThrottle(throttleTime: Long = 600L, action: () -> Unit) {
    this.setOnClickListener(object : View.OnClickListener {
        private var lastClickTime: Long = 0

        override fun onClick(v: View) {
            if (SystemClock.elapsedRealtime() - lastClickTime < throttleTime) return
            else action()

            lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}
fun View.increaseHitArea() {
    val parent =
        parent as View // button: the view you want to enlarge hit area

    parent.post {
        val rect = Rect()
        getHitRect(rect)
        rect.top -= 100 // increase top hit area
        rect.left -= 100 // increase left hit area
        rect.bottom += 100 // increase bottom hit area
        rect.right += 100 // increase right hit area
        parent.touchDelegate = TouchDelegate(rect, this)
    }
}
@SuppressLint("ClickableViewAccessibility")
fun View.hideKeyboardOnTouch() {
    setOnTouchListener { view, motionEvent ->
        hideKeyboard()
        true
    }
}
//endregion

//region Enabled disabled
fun View.disable() {
    isEnabled = false
}
fun View.enable() {
    isEnabled = true
}
//endregion

//region Visibility
fun View.invisible() {
    visibility = View.INVISIBLE
}
fun View.makeInvisibleIf(invisible: Boolean) {
    if (!invisible) {
        visible()
    } else {
        invisible()
    }
}
fun View.makeGoneIf(gone: Boolean) {
    if (!gone) {
        visible()
    } else {
        gone()
    }
}

/**
 * @param gone if visible is false we make the view gone or invisible
 */
fun View.makeVisibleIf(visible: Boolean, gone: Boolean = true) {
    if (visible) {
        visible()
    } else {
        if (gone) gone() else invisible()
    }
}

fun Set<View>.visible() = forEach { it.visible() }
fun Set<View>.gone() = forEach { it.gone() }

fun View.visible() {
    visibility = View.VISIBLE
}
fun View.gone() {
    visibility = View.GONE
}
//endregion

//region Buttons

fun Button.clickIfEnabled() {
    if (isEnabled) callOnClick()
}

fun ImageButton.clickIfEnabled() {
    if (isEnabled) callOnClick()
}

fun Button.setButtonTextBold(){
    this.typeface = Typeface.DEFAULT_BOLD
}
//endregion

//region Keyboard
fun View.hideKeyboard() {
    val inputMethodManager: InputMethodManager? =
        context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
    inputMethodManager?.hideSoftInputFromWindow(windowToken, 0)
}

fun View.showKeyboard() {
    val inputMethodManager: InputMethodManager? =
        context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
    inputMethodManager?.showSoftInput(this, 1)
}
//endregion

//region View holders
/**
 * Used for views inside [LifecycleOwner]s to launch a coroutine and reduce code
 * If the view doesn't belong to a [LifecycleOwner] nothing will happen
 * The default from [Dispatchers] here is [Dispatchers.Main] and then it can be changed easily
 * @see main
 * @see default
 * @see io
 * @param block code to be executed as it is from a [CoroutineScope]
 */
fun View.launch(block: suspend CoroutineScope.() -> Unit) {
    (context as? LifecycleOwner)?.let { owner ->
        owner.lifecycleScope.launch(Dispatchers.Main){
            block()
        }
    }
}

val RecyclerView.ViewHolder.context: Context
    get() = itemView.context

val RecyclerView.ViewHolder.resources : Resources
    get() = itemView.resources
//endregion

////region Status bar
//fun View.setStatusBarPadding(){
//    this.updatePadding(top = context.getStatusBarHeight())
//}
fun View.shiftContentForStatusBar() {
    val rootView = this
    ViewCompat.setOnApplyWindowInsetsListener(rootView) { _, insets ->
        val menuLayoutParams = rootView.layoutParams as ViewGroup.MarginLayoutParams
        menuLayoutParams.setMargins(0, insets.systemWindowInsetTop, 0, 0)
        rootView.layoutParams = menuLayoutParams
        insets.consumeSystemWindowInsets()
    }
}
//endregion

// region Image Loading
fun ImageView.fadeCoil(string: String) {
    load(string) {
        crossfade(true)
    }
}

fun ImageView.loadImageWithPlaceHolder(uri: String, placeHolder: Int ){
    this.load(uri) {
        error(placeHolder)
        placeholder(placeHolder)
    }
}

fun ImageView.loadImageWithCallBacks(uri: String,
                                     placeHolder: Int,
                                     loadFailed: () -> Unit,
                                     loadSucceed: ()-> Unit){
    this.load(uri) {
        placeholder(placeHolder)
        listener(
                onError = { imageRequest: ImageRequest, throwable: Throwable ->
                    loadFailed()
                },
                onSuccess = { imageRequest: ImageRequest, metadata: ImageResult.Metadata ->
                    loadSucceed()
                }
        )
    }
}
// endregion

//region Animations
inline fun View.fadeIn(
        crossinline onEnd: (() -> Unit) = {},
        crossinline onStart: (() -> Unit) = {},
) {
    animate()
        .alpha(1f)
        .setDuration(400L)
        .withStartAction { onStart() }
        .withEndAction { onEnd() }
        .start()
}
inline fun View.fadeOut(
        invisible: Boolean = false,
        crossinline onEnd: (() -> Unit) = {},
        crossinline onStart: (() -> Unit) = {},
) {
    animate()
        .alpha(0f)
        .setDuration(400L)
        .withEndAction { onEnd() }
        .withStartAction { onStart() }
        .start()
}
fun View.scaleIn() {
    if (setOf(scaleX, scaleY).any { it > 0 }) return
    animate().scaleX(1f).scaleY(1f).setDuration(400L).withStartAction { visible() }.start()
}
fun View.scaleOut() {
    if (setOf(scaleX, scaleY).any { it < 1 }) return
    animate().scaleX(0f).scaleY(0f).setDuration(400L).withEndAction { gone() }.start()
}
fun ShimmerFrameLayout.setupShimmer() {
    val builder = Shimmer.AlphaHighlightBuilder()
    builder.setAutoStart(true)
        .setDirection(Shimmer.Direction.LEFT_TO_RIGHT)
        .setShape(Shimmer.Shape.LINEAR)
        .setBaseAlpha(0.8f)
        .setHighlightAlpha(0f)
    val colorBuilder = Shimmer.ColorHighlightBuilder().copyFrom(builder.build())
        .setHighlightColor(R.color.black_70.getColorInt(context))
        .setBaseColor(R.color.white_four.getColorInt(context))
    setShimmer(colorBuilder.build())
}

fun View.popAnimation(isShow: Boolean) {
    if (visibility == VISIBLE && isShow) return
    if ((visibility == GONE || visibility == INVISIBLE) && !isShow) return
    val anim = AnimationUtils.loadAnimation(context, if (isShow)  R.anim.pop_in else R.anim.pop_out)
    makeVisibleIf(isShow, true)
    startAnimation(anim)
}

fun View.popAnimationWithListener(isShow: Boolean) {
    if (visibility == VISIBLE && isShow) return
    if ((visibility == GONE || visibility == INVISIBLE) && !isShow) return
    val anim = AnimationUtils.loadAnimation(context, if (isShow)  R.anim.pop_in else R.anim.pop_out)
    anim.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {}

        override fun onAnimationEnd(animation: Animation?) {
            if (!isShow) gone()
        }

        override fun onAnimationStart(animation: Animation?) {
            if (isShow) visible()
        }
    })
    startAnimation(anim)

}
//endregion

//region Recycler view helpers
/**
 * Warning! Do not use this function if layout manager is not [LinearLayoutManager]
 */
fun RecyclerView.firstVisibleItem() = (layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
/**
 * Warning! Do not use this function if layout manager is not [LinearLayoutManager]
 */
fun RecyclerView.lastVisibleItem() = (layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
inline fun RecyclerView.doOnScrollState(crossinline onChanged: (RecyclerView, Int) -> Unit) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            onChanged(recyclerView, newState)
        }
    })
}
fun RecyclerView.getState() = layoutManager?.onSaveInstanceState()
fun RecyclerView.setState(state: Parcelable) = layoutManager?.onRestoreInstanceState(state)
fun RecyclerView.smoothSnapToPosition(position: Int, startFromPos: Int = 20, snapMode: Int = LinearSmoothScroller.SNAP_TO_START) {
    val smoothScroller = object : LinearSmoothScroller(this.context) {
        override fun getVerticalSnapPreference(): Int = snapMode
        override fun getHorizontalSnapPreference(): Int = snapMode
    }
    smoothScroller.targetPosition = position
    layoutManager?.let { manager ->
        (manager as? LinearLayoutManager)?.let { linearManager ->
            // if the list is long and we need to scroll up fast
            // recycler is scrolled instantly to [startFromPos] and then smooth to top (pos 0)
            if (linearManager.findLastVisibleItemPosition() >= startFromPos) {
                linearManager.scrollToPosition(startFromPos)
            }
        }
        manager.startSmoothScroll(smoothScroller)
    }
}

fun RecyclerView.setScrollListener( onScrolled: (recyclerView: RecyclerView, dx: Int, dy: Int) -> Unit){
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            onScrolled.invoke(recyclerView,dx,dy)
        }

    })
}

fun RecyclerView.scrollToPositionWithOffset(position: Int, offset: Int = this.paddingTop){
    (this.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(position, offset)
}
inline fun RecyclerView.scrollAndWait(toPos: Int, crossinline onDone: () -> Unit) {
    val manager = layoutManager as? LinearLayoutManager
    val firstItemInitial = manager?.findFirstCompletelyVisibleItemPosition() ?: 0
    val lastItemInitial = manager?.findLastCompletelyVisibleItemPosition() ?: 0
    if (toPos in firstItemInitial..lastItemInitial) {
        onDone()
        return
    }
    val listener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            manager?.let { lManager ->
                val firstItem = lManager.findFirstCompletelyVisibleItemPosition()
                val lastItem = lManager.findLastCompletelyVisibleItemPosition()
                if (newState == RecyclerView.SCROLL_STATE_IDLE && toPos in firstItem..lastItem) {
                    removeOnScrollListener(this)
                    onDone()
                } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    removeOnScrollListener(this)
                }
            }
        }
    }
    smoothSnapToPosition(toPos)
    addOnScrollListener(listener)
}
//endregion

//region Edit texts
inline fun EditText.onDone(crossinline onDone: (() -> Unit)) {
    setOnEditorActionListener { v, actionId, event ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onDone()
        }
        false
    }
}

inline fun EditText.onNext(crossinline onNext: (() -> Unit)) {
    setOnEditorActionListener { v, actionId, event ->
        if (actionId == EditorInfo.IME_ACTION_NEXT) {
            onNext()
        }
        false
    }
}


fun EditText.setFont(font: Int){
    this.typeface = ResourcesCompat.getFont(this.context, font)
}

fun EditText.requestFocusOpenKeyboard(){
    this.requestFocus()
    this.postDelayed({
        val imm: InputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(this, 0)
    }, 200)

}


fun EditText.setMaxLength(length: Int) {
    val filterArray = arrayOfNulls<InputFilter>(1)
    filterArray[0] = InputFilter.LengthFilter(length)
    this.filters = filterArray
}

fun EditText.placeCursorAtEndOfText(){
    this.setSelection(this.length())
}
//endregion

//region Bottom Navigation
/**
 * Used for Showing or hide badge notification on [BottomNavigationItemView]
 * We use [NotificationBadge] as parent class if any one want to make his custom layout he should extend or use it
 * to ensure that last item added to view is extends from [NotificationBadge]
 * we check if badge appear not to show another one because we need only one on the top to be removed easily to hide it
 * @param isShow to show or hide badge
 * @param index index of [BottomNavigationItemView] to do action on it
 * @param layoutId for any custom layout badge to show it by default we send  [R.layout.item_badge]
 */
fun BottomNavigationView.showHideBadge(isShow: Boolean, index: Int = 4, layoutId: Int = R.layout.item_badge) {

    val bottomNavigationMenuView = (getChildAt(0) as? BottomNavigationMenuView)?.getChildAt(index) as? BottomNavigationItemView
    val notificationBadge = LayoutInflater.from(context).inflate(layoutId,
        bottomNavigationMenuView, false) as? NotificationBadge

    val size = bottomNavigationMenuView?.childCount
    size?.let {
        if (isShow) {
            if (bottomNavigationMenuView[size - 1] !is NotificationBadge) {
                bottomNavigationMenuView.addView(notificationBadge, size)
            }
        } else {

            if (bottomNavigationMenuView[size - 1] is NotificationBadge) {
                bottomNavigationMenuView.removeViewAt(size - 1)
            }
        }

    }
}
//endregion

//region textView

fun TextView.withBoldTextAtTheBeginning(boldText: String, regularText: String){
   this.text= SpannableStringBuilder()
            .bold { append("${boldText} ") }
            .append(regularText)
}

fun TextView.unWrap(wrapper: StringWrapper) {
    text = wrapper.getString(context)
}
//endregion
//region TabLayout
/**
 * Used for bindTabs in [TabLayout] if items more than 3 it make tablayout scrollable and
 * if less than or equal 3 it will be Fixed in screen
 * make first item selected by default
 * @param tabsItems Array of items names in Int in strings to easy support locale
 */
@JvmName("bindTabsResList")
fun TabLayout.bindTabs(tabsItems: List<String>) {
    tabMode = if (tabsItems.size > 3) {
        TabLayout.MODE_SCROLLABLE
    } else {
        TabLayout.MODE_FIXED
    }

    for (i in tabsItems.indices) {
        addTab(newTab().setText(tabsItems[i]).setTag(tabsItems[i]), i == 0)
    }
}


fun TabLayout.setMargin(left: Int, top: Int, right: Int, bottom: Int) {
    for (i in 0 until tabCount) {
        val tab = (getChildAt(0) as ViewGroup).getChildAt(i)
        val p = tab.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(60, 0, 20, 0)
        tab.requestLayout()
    }
}

/**
 * Gets the selected tab and call the [lambda] with the tag casted to the type [T]
 *
 * @param T type of the tag object
 */
inline fun <reified T> TabLayout.doWithSelectedTabTag(lambda: (T) -> Unit) {
    kotlin.runCatching { getTabAt(selectedTabPosition) }.getOrNull()?.let { tab ->
        (tab.tag as? T)?.let { currentTag ->
            lambda(currentTag)
        }
    }
}


//region coloring
fun View.setViewTintColor(@ColorRes color: Int) {
    this.background.setTintList(ContextCompat.getColorStateList(context, color))
}

fun Button.setButtonTextColor(@ColorRes color: Int) {
    this.setTextColor(context.resources.getColor(color, null))
}

fun Button.setBackgroundTintColor(@ColorRes color: Int){
    this.backgroundTintList = context.resources.getColorStateList(color,null);
}

fun TextView.setTextViewTextColor(@ColorRes color: Int) {
    this.setTextColor(context.resources.getColor(color, null))
}

fun View.addRippleEffect(ripple: Int){
    foreground = ContextCompat.getDrawable(context, ripple)
}
//endregion

//region Time
fun TextView.showTimePickerDialog(title: String = context.getString(R.string.select_time), is24HourView: Boolean = false) {
    val currentTime: Calendar = Calendar.getInstance()
    val hour: Int = currentTime.get(Calendar.HOUR_OF_DAY)
    val minute: Int = currentTime.get(Calendar.MINUTE)
    val timePicker = TimePickerDialog(context, { _, selectedHour, selectedMinute ->
        text = if (!is24HourView) {
            val (selectedFormatedHour, timeFormat) = selectedHour.to12HourFormat(context)
            "${selectedFormatedHour.twoDigitFormat()}:${selectedMinute.twoDigitFormat()} $timeFormat"
        } else {
            "${selectedHour.twoDigitFormat()}:${selectedMinute.twoDigitFormat()}"
        }
    }, hour, minute, is24HourView
    )
    timePicker.setTitle(title)
    timePicker.show()
}
//endregion Time
