package com.trianglz.iskipuserandroid.utils.avatarplaceholder

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import coil.load
import com.trianglz.iskipuserandroid.R
import com.trianglz.iskipuserandroid.commons.presentation.avatarplaceholder.LettersNumberMode
import com.trianglz.iskipuserandroid.databinding.AvatarPlaceHolderLayoutBinding
import com.trianglz.iskipuserandroid.utils.extensions.invisible
import com.trianglz.iskipuserandroid.utils.extensions.visible
import java.util.*


class AvatarPlaceholder : ConstraintLayout {

    //region Members
    private lateinit var binding: AvatarPlaceHolderLayoutBinding
    private var lettersNumberMode: LettersNumberMode? = null
    //endregion


    //region Constructor
    constructor(context: Context) : super(context) {
        initAvatar()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initAvatar(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initAvatar(attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        initAvatar(attrs)
    }
    //endregion

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//
//        val width = measuredWidth
//        val height = measuredHeight
//        val squareLen = Math.min(width, height)
//
//        super.onMeasure(
//            MeasureSpec.makeMeasureSpec(squareLen, MeasureSpec.EXACTLY),
//            MeasureSpec.makeMeasureSpec(squareLen, MeasureSpec.EXACTLY)
//        )

//        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//        val requestedWidth = MeasureSpec.getSize(widthMeasureSpec)
//        val requestedWidthMode = MeasureSpec.getMode(widthMeasureSpec)
//
//        val requestedHeight = MeasureSpec.getSize(heightMeasureSpec)
//        val requestedHeightMode = MeasureSpec.getMode(heightMeasureSpec)
//
//        val desiredWidth: Int = //TODO("Define your desired width")
//        val desiredHeight: Int = //TODO("Define your desired height")
//
//        val width = when (requestedWidthMode) {
//            MeasureSpec.EXACTLY -> requestedWidth
//            MeasureSpec.UNSPECIFIED -> desiredWidth
//            else -> Math.min(requestedWidth, desiredWidth)
//        }
//
//        val height = when (requestedHeightMode) {
//            MeasureSpec.EXACTLY -> requestedHeight
//            MeasureSpec.UNSPECIFIED -> desiredHeight
//            else -> Math.min(requestedHeight, desiredHeight)
//        }
//
//        setMeasuredDimension(width, height)
    }


    //region Private API
    private fun initAvatar(attrs: AttributeSet? = null) {
        // get attrs
        context.theme.obtainStyledAttributes(
            attrs, R.styleable.AvatarPlaceholder, 0, 0
        ).apply {
            // get input mode
            lettersNumberMode = when (val selectedInputModeId =
                getInteger(R.styleable.AvatarPlaceholder_lettersNumber, 1)) {
                else -> LettersNumberMode.getModeByStyleableId(selectedInputModeId)
            }
            // inflate layout
            inflateLayout()

        }
    }

    private fun inflateLayout() {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.avatar_place_holder_layout, this, true
        )

    }

    private fun handleTextAvatar(name: String) {
        binding.textView.visible()
        val text = when (lettersNumberMode) {
            LettersNumberMode.One -> name.firstOrNull()?.toUpperCase()
            LettersNumberMode.Two -> {
                if (name.isEmpty()) return
                val arr = name.split(" ")
                if (arr.size > 1) {
                    "${arr.first().first()}${arr.last().first()}".toUpperCase(Locale.ROOT)
                } else {
                    name.firstOrNull()?.toUpperCase()
                }
            }
            else -> {
                ""
            }
        }
        binding.textView.text = text.toString()
    }
    //endregion

    //region Private API
    fun load(url: String?, name: String) {
        binding.imageView.clipToOutline = true
        binding.imageView.load(url) {
            crossfade(true)
            listener(
                onError = { _, _ ->
                    binding.imageView.invisible()
                    handleTextAvatar(name)
                },
                onSuccess = { _, _ ->
                    binding.imageView.visible()
                }
            )
        }
    }


    //region Private API
    fun loadImage(url: String?) {
        binding.imageView.clipToOutline = true
        binding.imageView.load(url) {
            crossfade(true)
            listener(
                onError = { _, _ ->
                    binding.imageView.visible()
                    binding.imageView.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.img_k_logo))
                },
                onSuccess = { _, _ ->
                    binding.imageView.visible()
                }
            )
        }
    }

    //endregion
}