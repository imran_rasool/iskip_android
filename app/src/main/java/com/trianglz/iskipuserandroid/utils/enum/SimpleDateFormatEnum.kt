package com.trianglz.iskipuserandroid.utils.enum

import java.text.SimpleDateFormat
import java.util.*

enum class SimpleDateFormatEnum(val simpleDateFormat: SimpleDateFormat) {
    DATE(SimpleDateFormat("hh:mm a | dd MMM yyy", Locale.ENGLISH)),
    DATE_INPUT(SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)),
    DATE_DAY(SimpleDateFormat("EEE, MMM d HH:mm:ss a", Locale.ENGLISH)),
    DATE_DAY2(SimpleDateFormat("EEE d MMM,yyyy",Locale.getDefault())),
    FULL_DATE(SimpleDateFormat("EEE MMM d,yyyy | hh:mm aa",Locale.getDefault())),
    DAY_MONTH(SimpleDateFormat("d MMM", Locale.ENGLISH)),
    DAY_MONTH_YEAR2(SimpleDateFormat("d MMMM, yyyy", Locale.ENGLISH)),
    DAY_MONTH_YEAR(SimpleDateFormat("dd MMM, yyy", Locale.ENGLISH)),
    DAY_MONTH_YEAR3(SimpleDateFormat("dd MMM yyy", Locale.getDefault())),
    DAY_MONTH_YEAR4(SimpleDateFormat("yyyy-MM-dd",  Locale.ENGLISH)),
    CHAT_DATE(SimpleDateFormat("d/M/yyyy h:mm a", Locale.ENGLISH)),
    DAY_MONTH_YEAR_HOUR(SimpleDateFormat("dd MMM, yyy | HH:mm a", Locale.getDefault())),
    YEAR_MONTH_DAY(SimpleDateFormat("yyyyMMdd", Locale.ENGLISH)),
    MONTH_YEAR(SimpleDateFormat("MMM yyyy", Locale.ENGLISH)),
    DAY_MONTH_HOUR(SimpleDateFormat("dd MMM | hh:mm a", Locale.ENGLISH)),
    HOUR_MINUTE(SimpleDateFormat("hh:mm a", Locale.ENGLISH)),
    HOUR_MINUTE_24(SimpleDateFormat("HH:mm", Locale.getDefault())),
    HOUR_MINUTE_24_ENG(SimpleDateFormat("HH:mm", Locale.ENGLISH)),
    HOUR_MINUTE_AM_PM(SimpleDateFormat("hh:mm aa", Locale.getDefault())),

}